# PhD Thesis

To download the latest version as PDF go [here](https://gitlab.cern.ch/mguth/phd-thesis/-/jobs/artifacts/master/raw/main.pdf?job=run_latex)

To view the latest version as PDF go [here](https://gitlab.cern.ch/mguth/phd-thesis/-/jobs/artifacts/master/file/main.pdf?job=run_latex)
