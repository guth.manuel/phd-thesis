\pdfbookmark[1]{Machine Learning}{machine-learning}
\chapter{Machine Learning}\label{chap:ML}

The collection of large datasets requires sophisticated techniques to analyse those.
In this context, \ac{ml} is one of the fastest-growing fields in computer science allowing the program to learn patterns in a multi-dimensional phase space.\\
Especially high energy physics is well suited to apply \ac{ml} techniques with its broad range of possible applications and its vast amount of labelled data (\acs{mc} simulation). Already at detector level \ac{ml}  comes into play by deploying neural networks on FPGAs for the trigger~\cite{FPGA-trigger} or to spot machine failures~\cite{WIELGOSZ2018166}. In the reconstruction step, new \ac{ml} techniques are developed for charged particle tracking, in particular to cope for the increasing luminosity during RUN~III of the \acs{lhc} and the \acs{hllhc}~\cite{tracking-hashing,Tsaris_2018}. \ac{ml} is not only able to outperform conventional algorithms but is also able to mimic algorithms which cannot be replaced by \ac{ml} per se. One example for this case is the detector simulation with \textsc{Geant}4~\cite{Agostinelli:2002hh,ATLAS-Geant4} which is one of the most CPU consuming tasks within ATLAS. As pointed out in Section~\ref{sec:hadroncoll:geant}, Generative Adversarial Networks (GANs) and Variational Auto-Encoders (VAEs) are studied to improve the fast calorimeter simulation~\cite{GANs:ATLAS}. Apart from the object reconstruction, also the object identification is improved using neural networks for instance in the $\tau$-identification~\cite{ATL-PHYS-PUB-2019-033} or in \btag algorithms~\cite{ATL-PHYS-PUB-2020-014}, which will be the main application discussed in this thesis. In physics analyses, sophisticated \ac{ml} methods help to reconstruct and discriminate signal processes~\cite{CMS-PAS-HIG-18-030,PhysRevLett.121.241803}. 
% Often analyses are dealing with negative event weights which are difficult to handle using \ac{ml}. However, there are also techniques to scope for that e.g. neural rebalancing of negative event weights~\cite{Nachman:2020fff}.

% Madminer~\cite{Brehmer:2019xox}

\section{General Introduction}
\acl{ml} is a very broad umbrella term covering all kinds of algorithms which are not per se optimised for a specific task but are flexible enough to adapt to different problem sets by tuning (training) their parameter set.\\
\ac{ml} requires besides the model itself also preparation and follow-up processing steps. In which extent they are necessary always depends on the available data, the model and its later application. Figure~\ref{fig:ML_circle} shows such an example workflow (the single steps are explained in more detail in the dedicated sections e.g. sec.~\ref{sec:ftag:dl1:preprocessing}).
\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{material/ML-circle.pdf}
    \caption{\acl{ml} workflow showing additional steps besides machine learning training itself.}
    \label{fig:ML_circle}
\end{figure}


Generally, two types of machine learning are distinguished: \textit{Supervised learning} requiring fully labelled training data and \textit{Unsupervised learning} not requiring any labelled data. There are also intermediate approaches called \textit{Semi-Supervised learning}. In the context of this thesis supervised approaches are used based on \acp{nn} and \acfp{bdt}.\\
    %  \textit{Reinforcement learning}
    

In the following, a statistical parametric (\ac{ml}) model is denoted as  $P_\text{model}(\Vec{x}_i;\theta)$ parametrised with the parameters $\theta$ while $P_\text{data}$ is the true but unknown distribution. A data set of length $N$ is given as $\Vec{X}=(\Vec{x}_1,\Vec{x}_2,...,\Vec{x}_N)$, in which each data point $i$ has a feature set $\Vec{x}_i=(x_i^1,x_i^2,...,x_i^M)$ with $M$ features and true labels $y_i$ in case of supervised learning.

\subsection{Dataset Handling}
It is important to ensure an unbiased training process. For this purpose, at least three orthogonal datasets are needed as indicated in Figure~\ref{fig:ml:datahandling}. The training sample is utilised for the actual algorithm training. The validation set is typically used to choose between different models and to optimise the model further such as hyperparameter optimisation. While for the training itself mostly a loss function is used (see sec.~\ref{sec:ml:loss}) to find the best parameter set, on the validation set, the performance measures dedicated for the problem set are evaluated (e.g. signal over background ratio) to fine-tune the model choice. The testing sample is only used to evaluate the final performance and is not involved in the training process.
In the case of samples with low statistics, one can use cross-validation or also called $k$-folding~\cite{ref1} where pairs of training and test-/validation sets are partitioned into $k$ subsets.\\
\begin{figure}[h]
    \centering
    \includegraphics[width=0.75\textwidth]{methodology/machine-learning/dataset-handling}
    \caption{Sketch of the dataset handling for \ac{ml} training, validating and testing a model.}
    \label{fig:ml:datahandling}
\end{figure}

Typically, in particle physics the event number\footnote{The event number is a unique integer number associated to each event not correlated with any physical observable.} variable is used to split the dataset into the training and testing set. The advantage is that at every point it is clear which events were used for the training.

\section{Performance Evaluation}
\label{sec:performance-ml}
Even though every \ac{ml} application is different, the model performance is the decisive measure in the end. Depending on the task, different metrics are used to judge the performance. In the following, the most common approaches are discussed.


\subsection{Maximum Likelihood}\label{sec:ml:lielihood}
The \ac{mle} is an approach to optimise parameters $\theta$ of a probability distribution using a likelihood function. A likelihood function $\mathscr{L}(\theta;\Vec{X})$, also simply called \textit{likelihood}, is a joint probability distribution of a finite data distribution $\Vec{X}$ %=\{\Vec{x}|\Vec{x}\sim P_\text{data}(\Vec{x})\}$ 
depending on parameters only. % with $P_\text{data}$ the true but unknown distribution. % and a function of the unknown parameters only. 
It provides a compatibility measure of the statistical, parametric model $P_\text{model}(\Vec{x};\theta)$ to data for given values of the unknown parameters
\begin{equation}
    \mathscr{L}(\theta;\Vec{X})=\prod_{\Vec{x}\in \Vec{X}}P_\text{model}(\Vec{x};\theta).
\end{equation}
The optimum is the maximised likelihood. It is more convenient to minimise the negative log-likelihood

\begin{equation}
    \theta^*=\arg\underset{\theta}{\min}(-\ln\mathscr{L}(\theta;\Vec{X}))=\arg\underset{\theta}{\min}\left(-\sum_{\Vec{x}\in \Vec{X}}\ln P_\text{model}(\Vec{x};\theta)\right),
\end{equation}
since the product of several terms much smaller than one, is numerically unstable to compute.
Even though the solution can be found analytically in some cases, it is mostly computed numerically.


\subsection{Multi-Classification Likelihood Discriminant}
A multi-classification model typically has $C$ outputs, one for each class $c$ associating a score for being compatible to that specific class. The multi-classification  allows a more detailed interpretation of the results than just a binary classification. Nonetheless, it is often useful to have one single discriminating variable.\\
% In order to compare the performance of different architectures, samples, preprocessing strategies etc., it is necessary to define a quantity to compare with. 
Generally, the best discriminant for a neural network with a multiple-class output is given by a monotonically decreasing function combining all output nodes with a signal over background ratio, as an example shown for three classes which will be used later in this thesis
\begin{equation}
\frac{p_\text{signal}}{k_1 \cdot p_\text{bkg1} + (1-k_1) \cdot p_{\text{bkg}2}},
\end{equation}
with $p_\text{signal}$, $p_\text{bkg1,2}$  the \ac{nn} output node corresponding to the signal class and the two background classes, respectively.
Hereby, \(k_1\) is considered to be an effective parameter defined between 0 and 1 which allows tuning the relative performance/rejection of background 1.
% \improvement{rephrase this, make it more clear what k1 is}  
This is only valid if the values of the output nodes sum up to one.
% is realised using the softmax activation function which also ensures that the outputs can be interpreted as probabilities.
By taking the logarithm of this function it is still monotonically decreasing and results in the following discriminant 
\begin{equation}
\mathcal{D} = \log\left(\frac{p_\text{signal}}{k_1 \cdot p_\text{bkg1} + (1-k_1) \cdot p_{\text{bkg}2}}\right).
\label{eq:performance-multiclass}
\end{equation}
This likelihood discriminant has the advantage that it is tuneable and one can give the emphasis to better discriminate a specific background class.\\
% This discriminant will be used for the neural network performance evaluation with neural networks having a 3 class output.
By adding more output classes it is also necessary to add per additional class another effective parameter \(k_i\) satisfying the relation 
\begin{equation}
    \sum_i k_i=1.
\end{equation}



\subsection{Loss Function}\label{sec:ml:loss}
% \newcommand{\loss}{\ensuremath{\mathcal{J}}\xspace}
A loss function \loss, also called \textit{cost function}, quantifies the deviation of a model with respect to its true values and is minimised during the model training. For supervised learning, loss functions are typically functions of the target labels. The choice of the optimal loss function requires a profound understanding of the problem. Different choices of the loss function could yield other optimal solutions.\\
Generally, the combined loss of a dataset $\Vec{X}$ can be calculated via the average of the individual losses of the single data points $\Vec{x}_i$
\begin{equation}
\loss(\theta;\Vec{X}) =\frac{1}{N}\sum_{i=1}^N\loss(y_i,P_\text{model}(\Vec{x}_i;\theta)).    
\end{equation}
When dealing with regression problems, the most frequently used loss function is the mean square error (MSE)
\begin{equation}
\loss_\text{MSE}(\theta;\Vec{X})=\frac{1}{N}\sum_{i=1}^N (y_i-P_\text{model}(x_i;\theta))^2.
\end{equation}
In the case of binary classification, the negative loglikelihood of a Bernoulli distribution is used, the so-called \textit{binary cross-entropy}
\begin{equation}
\loss_\text{binary-cross-entropy}(\theta;\Vec{X})=-\frac{1}{N}\sum_{i=1}^N y_i \cdot \log(P_\text{model}(\Vec{x}_i;\theta)) + (1-y_i) \cdot\log(1-P_\text{model}(\Vec{x}_i;\theta)),
\end{equation}
which can be extended to multi-classification with $C$ classes
\begin{equation}
\loss_\text{categorical-cross-entropy}(\theta;\Vec{X})=-\frac{1}{N}\sum_{i=1}^N\sum_{c=1}^C \mathcal{Y}_c\log P_\text{model}(c|\Vec{x}_i;\theta).
\end{equation}
The probability of the data point being of class $c$ is given by $P_\text{model}(c|\Vec{x}_i;\theta)$ and $\mathcal{Y}_c$ is a binary indicator specifying whether the class $c$ matches the true class of $\Vec{x}_i$.\\
Often, loss functions are also customised for specific tasks. Another loss is the exponential loss
% The double sum is over the observations `i`, whose number is `N`, and the categories `c`, whose number is `C`. The term `1_{y_i \in C_c}` is the indicator function of the `i`th observation belonging to the `c`th category. The `p_{model}[y_i \in C_c]` is the probability predicted by the model for the `i`th observation to belong to the `c`th category. When there are more than two categories, the neural network outputs a vector of `C` probabilities, each giving the probability that the network input should be classified as belonging to the respective category. When the number of categories is just two, the neural network outputs a single probability `\hat{y}_i`, with the other one being `1` minus the output. This is why the binary cross entropy looks a bit different from categorical cross entropy, despite being a special case of it.
%-------------------------------------------------------------------------------
\begin{equation}
\loss_\text{exponential}(\theta;\Vec{X})=\frac{1}{N}\sum_{i=1}^N e^{-y_i P_\text{model}(x_i;\theta)},\label{eq:ml:exploss}
\end{equation}
which is of particular interest for the utilised \ac{bdt} algorithm described in Section~\ref{sec:ml:bdts}.

\section{Neural Networks}\label{sec:ml:nns}
The concept of \aclp{nn} was already introduced in the 1940s~\cite{McCulloch1943} but only became feasible for wide applications with the easy access to large computing power, where especially GPUs were a big step forward.
Neural networks are composed of artificial neurons connected via weights to each other, forming a network. The most basic network is a so-called \textit{feed-forward network} as illustrated in Figure~\ref{fig:ml:nnex}. This simplified example has one input layer, one hidden layer and one output node.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{methodology/machine-learning/NN_example}
    \caption{Neural network with two input nodes in red, one hidden layer with two nodes in green and one output node in blue. Inspired from Ref.~\cite{fermilab:keras:workshop}. The sketch is inverted for illustration purposes to better match the form of Equation~\eqref{eq:ml:nn}.}
    \label{fig:ml:nnex}
\end{figure}
Mathematically this network can be described as a matrix function 
\begin{equation}
    \color{blue}P_\text{model}^{NN}=f_2(\color{violet}b_2+W_2\color{green}f_1(\color{orange}b_1+W_1\color{red}\Vec{x}\color{green})\color{blue})\color{black}\label{eq:ml:nn},
\end{equation}
with weight matrices $W_i$, bias terms $b_i$  and activation functions $f_i$. Expressing $P_\text{model}^{NN}$ in full matrix notation yields
\begin{equation}
P_\text{model}^{NN} = f_2 \left( \begin{bmatrix} b^2_{1,1} \end{bmatrix}  + 
    \begin{bmatrix} W^2_{1,1} & W^2_{1,2} \end{bmatrix} f_1 \left(
    \begin{bmatrix} b^1_{1,1}\\ b^1_{2,1} \end{bmatrix} +
    \begin{bmatrix} W^1_{1,1} & W^1_{1,2} \\ W^1_{2,1} & W^1_{2,2} \end{bmatrix} 
    \begin{bmatrix} x^1_{1,1}\\ x^1_{2,1} \end{bmatrix}
    \right)
    \right).    
\end{equation}
In this notation, it is easier to see that each layer is a linear system in the form $b+W\cdot x$, where the bias is a constant offset. Only the choice of the activation function (more details below) introduces a non-linearity. Consequently, a \ac{nn} can approximate any arbitrary function  by giving the network enough freedom (amount of hidden layers and nodes per hidden layer). The free parameters $\theta$ which can be optimised are the weights and the bias values. This simple example already has nine free optimisable parameters, more complex networks easily reach several ten-thousands of free parameters.\\
In this thesis mainly deep feed-forward networks are utilised but there are a vast number of different neural network architectures available for every kind of application (see a comprehensive overview of modern machine learning in high energy physics in Ref.~\cite{hep-ml}).\\
The terminology of deep learning~\cite{Goodfellow-et-al-2016} is used for \acp{nn} with multiple hidden layers.
Driven by the fast-evolving field and the large industry interest in deep learning, several sophisticated and user-friendly software packages are available, which make \acp{nn} accessible to a wide audience. In particular, Tensorflow~\cite{tensorflow2015-whitepaper}, \textsc{Keras}~\cite{chollet2015keras} and pytorch~\cite{NEURIPS2019_9015} are the most used and advanced software packages. However, there are many more different frameworks available. In order to easier exchange models between different tools, an open-source format ONNX~\cite{bai2019} is used  which also gives the future possibility to easier integrate \ac{ml} models into ATLAS software infrastructure. For the time being, the \textsc{json}-based software package lwtnn~\cite{daniel_hay_guest_2019_3249317} is used to deploy \textsc{Keras} models in ATLAS software.\\


The training of \acp{nn} is performed in batches i.e. the training data is divided into equally sized segments. The weights of the \ac{nn} are updated after every batch. It is important that every batch is an adequate representation of the full dataset, typically realised by shuffling the full dataset before slicing the sample. A full iteration over the entire dataset is called \textit{epoch}.\\
In general, \acp{nn} have so-called \textit{hyperparameters} which are all the non-trainable parameters fixing the architecture and training process of a \ac{nn}.  Besides the number of hidden layers and nodes per hidden layers, the most important hyperparameters are discussed in the following. Also, the batch-size is a hyperparameter which needs to be optimised.

\subsection*{Optimiser}
The weights $\theta$ of \acp{nn}, and therefore the model itself, are optimised using gradient descent. This requires a loss function which has to be sub-differentiable with respect to the inputs $\Vec{x_i}$ and parameters $\theta$. After a first, usually random, initialisation of the weights $\theta_0$, they are iteratively updated following the simplified formula
\begin{equation}
    \theta_{i+1}=\theta_i-\lambda_i\nabla_\theta\loss(\theta;\Vec{X}),
\end{equation}
where $\lambda_i$ represents the learning rate which is tuneable and therefore a hyperparameter. Choosing the learning rate too large can prevent convergence because the optimisation is jumping over the minimum. On the contrary, a too low learning rate slows down the optimisation and the optimiser might get stuck in a local minimum. A learning-rate scheduler can prevent both extremes. In this thesis, the Adam optimiser~\cite{kingma2019method} is used which uses estimates of the first and second moment of the gradient.

\subsection*{Backpropagation}
For the gradient-descend method, it is required to be able to calculate the gradient of the loss function with respect to all trainable parameters. However, for \acp{nn} it is not feasible to calculate it analytically. % due to their complexity and the lack of continuity in parts of the computational graph.
The computational graph, which is the mathematical representation of the \ac{nn}, is very complex and even shows certain discontinuities in some parts. Consequently, lots of nested gradients need to be calculated and the chain rule\footnote{The chain rule states how to compute the derivative of composite functions which is summarised in this formula $\frac{dy}{dx}=\frac{dy}{dz}\frac{dz}{dx}$.} needs to be employed to fully determine the gradient.
To efficiently calculate the gradient, backpropagation~\cite{Rumelhart:1986we} is used. 
So basically, it computes these nested gradients applying the chain rule gradually to the full computational graph. The gradient computation is optimised by reusing sub-expressions and keeping track of parameter dependencies.


\subsection*{Activation Functions}
As previously mentioned the activation functions $f(z)$ are essential to allow the \acp{nn} to learn non-linear patterns. In the beginnings of \acp{nn}, simple step functions were used but then were gradually replaced by monotonically increasing functions such as $\tanh$ or logistic functions also denoted as \textit{sigmoid}. These activation functions, however, suffer from vanishing gradient issues significantly slowing down the training. Nowadays, the \ac{relu}~\cite{10.5555/3104322.3104425} activation function is widely used, defined as 
\begin{equation}
    f_\textsc{ReLU}(z) = \left \{ \begin{array}{rcl}
0 & \mbox{for} & z < 0\\ z & \mbox{for} & z \ge 0\end{array} \right., \qquad f'_\textsc{ReLU}(z) = \left \{ \begin{array}{rcl} 0 & \mbox{for} & z < 0\\ 1 & \mbox{for} & z \ge 0\end{array} \right. ,
\end{equation}
with the simple derivative $f'(z)$ speeding up computations and not being affected by rapidly vanishing gradients. Besides \ac{relu} there are also other improved activation functions such as Leaky \ac{relu}~\cite{Maas2013RectifierNI} or \textit{Softplus}~\cite{10.5555/3008751.3008817}.\\
% Moreover, output nodes are differently treated 
In general, the output nodes are treated differently  and a different activation function is applied compared to the hidden layers. For regression tasks, the linear activation function is used, whereas for classification problems the output should mostly be interpretable as probabilities.  For the binary classification, this can be achieved using the \textit{sigmoid} function
\begin{equation}
    f_\text{sigmoid}(z)=\frac{1}{1+e^{-z}}\;,
\end{equation}
returning values between 0 and 1. A generalisation to multi-class classification with $C>2$ classes $c$ is the \textit{softmax} activation function
\begin{equation}
    f_\text{softmax}(z_c)=\frac{e^{z_i}}{\sum_{j=1}^Ce^{z_c}} \quad \text{fulfilling} \quad \sum_{c=1}^C f_\text{softmax}(z_c)=1,
\end{equation}
where the output for each class $c$ can be interpreted as the probability of the data point being compatible with class $c$.
% softmax: sum up to 1 -> interpretation as a probability




\subsection*{Regularisation}
Besides the training performance, an important feature of a \ac{ml} model is the ability to be generalisable and to not depend on fluctuations in the training data, i.e. avoid overfitting. In order to realise this, the capacity of the model needs to be sometimes limited producing a simpler and more robust model. In practice, mostly stochastic regularisation is used, such as Dropout~\cite{JMLR:v15:srivastava14a}, batch normalisation~\cite{DBLP:journals/corr/IoffeS15} or early stopping~\cite{Caruana00overfittingin}. The Dropout method randomly drops a certain percentage of node connections to neighbouring layers avoiding complex neuron co-adaptions. The batch normalisation re-normalises and re-scales the values of a batch and the early stopping terminates the training process after certain criteria for instance that the loss is not decreasing over a certain amount of epochs.






\section{Boosted Decision Trees}\label{sec:ml:bdts}
\acp{bdt} were one of the most commonly used multivariate technique in the last years in high energy physics, before \acp{nn} became more and more popular, and still have their raison d'être. The availability of \acp{bdt}, inside the widely used statistics and data handling package \textsc{ROOT}~\cite{Brun:1997}, via the \textsc{TMVA}~\cite{TMVA} package made it easily integratable in the analysis software workflows. Therefore, \acp{bdt} were deployed for various problem sets, such as in object identification~\cite{ATL-PHYS-PUB-2016-012} or discriminating signal and background processes in physics analyses.\\
A decision tree is structured as sketched in Figure~\ref{fig:ml:bdts} (left). It has, as the name suggests, a tree-like structure with branches connected via nodes. At each node, a cut decision based on a specific attribute is made. This is repeated until a stop criterion is met such as the maximal tree depth or the minimum events in a leaf node. A decision tree on its own is a weak learner and very sensitive to small changes in the training data.\\
\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{methodology/machine-learning/BDTs}
    \caption{Schematic view of a single decision tree on the left and an illustration of the boosting of trees on the right~\cite{bdtsource}.}
    % https://medium.com/analytics-vidhya/ensemble-models-bagging-boosting-c33706db0b0b
    \label{fig:ml:bdts}
\end{figure}

% \subsection*{Boosting}
\textit{Boosting} allows to create from an ensemble of weak learners, single decision trees, a powerful and robust model illustrated in Figure~\ref{fig:ml:bdts} (right). In the scope of this thesis, the \ac{adaboost}~\cite{Freund:1997xna} from the \textsc{TMVA}~\cite{TMVA} package is used.  A boosting algorithm iteratively combines $T$ single decision trees $P_\text{model}^{(t)}$ into a single discriminant
\begin{equation}
    P_\text{model}^\text{BDT}(\Vec{x}_i)=\sum_{t=1}^{T}\alpha_t P_\text{model}^{(t)}(\Vec{x}_i),
\end{equation}
with $\alpha_t$ the weights associated to each decision tree. The value of the weight $\alpha_t$ is chosen for \ac{adaboost} such that it minimises the loss function
\begin{equation}
    \loss_t=\sum_{i=1}^{N} \loss_\text{exponential} (P_\text{model}^{\text{BDT},(t-1)}(\Vec{x}_i)+\alpha_t P_\text{model}^{(t)}(\Vec{x}_i)),
\end{equation}
where the loss function $\loss_\text{exponential}$ is the exponential loss from equation \ref{eq:ml:exploss}. In addition, from one iteration to another, wrongly classified training events get larger weights associated to be more sensitive to those in the following tree.\\
Nowadays more advanced \ac{bdt} libraries are available with different boosting algorithms giving a better out-of-the-box performance. The most popular and successful libraries are \textsc{XGBoost}~\cite{Chen:2016:XST:2939672.2939785}, \textsc{LightGBM}~\cite{NIPS2017_6449f44a} and \textsc{CatBoost}~\cite{NEURIPS2018_14491b75}.

% The most popular boosting method, AdaBoost, is based on exponential loss, L(F,y) = e−F(x)y, which leads to the well known reweighting algorithm described in Sec. 7.1. Exponential loss has
% the shortcoming that it lacks robustness in presence of outliers or mislabelled data points. The performance of AdaBoost therefore is expected to degrade in noisy settings.
