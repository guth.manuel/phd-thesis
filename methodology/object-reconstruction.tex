\pdfbookmark[1]{Object Reconstruction and Particle Identification in ATLAS}{object-reconstruction}
\chapter{Object Reconstruction and Particle Identification in ATLAS}\label{chap:objectreco}

Physics processes produce electrons, muons, taus, quarks, gluons, photons and neutrinos in their final state. Since only position and energy information can be extracted from the detector, these physics objects need to be reconstructed. While quarks are forming jets, neutrinos cannot be directly seen; therefore, it is necessary to define, what the objects measured in the detector are.\\
% Physics analyses require physics objects like electrons or muons. However, the ATLAS detector, as described in Chapter~\ref{chap:atlas}, provides only hit and energy information of the different detector parts. \\
Figure~\ref{fig:particle-shower} illustrates the interaction of different particles with the ATLAS detector: charged particles leave a track in the \acs{id}, electrons and photons shower in the \acs{em} calorimeter while hadrons shower in the hadronic calorimeter.\\
In particular, % the flavour tagging studies in Part~\ref{part:ftag} and 
the \tthbb analysis in Part~\ref{part:analysis} makes use of electrons, muons, taus, jets and missing transverse momentum. The reconstruction of these objects is described in the following chapter. In addition, heavy-flavour identification is also an important tool for the \tthbb analysis. This topic is covered in the dedicated Chapter~\ref{chap:ftag_intro}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{methodology/object_reconstruction/particle-shower.pdf}
	\caption{Sketch of a section of the ATLAS detector in the transverse plane showing the interaction of particles with the detector material. Adapted from~\cite{atlas-sketch-particles}. \label{fig:particle-shower}}
\end{figure}

\section{Reconstruction from Detector Hits}
All physics objects reconstructed with the ATLAS detector are composed of tracks, vertices or calorimeter energy clusters (topo clusters). They are the fundamental building blocks used in all reconstruction algorithms and are introduced in the following section.


\subsection{Tracking} \label{sec:objects:tracks}
Charged particles which are passing through ATLAS leave tracks in the \ac{id}. They are reconstructed from energy deposits or hits in the \ac{id} within the tracking acceptance of $|\eta|<2.5$. A detailed description of the tracking in RUN II can be found in Ref.~\cite{Aaboud:2017all}.\\

As a first step, hits are assembled by grouping pixels and strips into clusters that reach an energy deposit above a given threshold.
In the next step, three-dimensional space points are defined where charged particles traverse the active detector material of the \ac{id}.\\% while in the pixel area one hit is used, in the \acs{sct} the info of both sides of the strip layer is utilised. On truth level (\acs{mc} level) one can define single-particle clusters and merged clusters defining if the charge deposits are arising from one particle or multiple particles, respectively. In addition, there are shared clusters which are not compatible enough with a merged cluster but they are used in multiple reconstructed tracks.\\
Next, a combinatorial track finding procedure is applied, starting by forming track seeds from a set of three space points. %They are considered in a purity order, first the \acs{sct}-only, then pixel-only and then mixed detector seeds.
The combination is done following the preliminary track trajectory adding space points iteratively. % from the remaining layers of the pixel detector and the \acs{sct} using a Kalman filter which updates the best track candidates. 
Afterwards, a score is associated to each track indicating if the track correctly represents the trajectory of a charged primary particle. According to the track score, the ambiguity solver evaluates the tracks in decreasing order to limit shared clusters which typically indicate a wrong assignment. At this stage, quality criteria are applied where the tracks have to have a minimum transverse momentum $\pt>$\unit[500]{MeV} and $|\eta|<2.5$. Moreover a minimum of seven pixel and \acs{sct} clusters (twelve are expected), a maximum of either one shared pixel cluster or two shared \acs{sct} clusters on the same layer, not more than 2 holes\footnote{A hole is a missing intersection with a sensitive detector element expected from the track trajectory estimation.} in the combined pixel and \acs{sct} detectors and no more than one hole in the pixel detector are required together with impact parameter requirements $|d_0^\text{BL}|<$\unit[2]{mm} and $|z_0^\text{BL}\sin\theta|<$\unit[3]{mm}. Here $d_0^\text{BL}$ is the transverse \ac{ip} calculated w.r.t the measured beamline position and $z_0^\text{BL}$ is the longitudinal difference along the beamline between the \ac{pv} and the point where $d_0^\text{BL}$ is measured. Finally, the reconstructed tracks are extrapolated into the \acs{trt} volume, also adding the \acs{trt} hits to the tracks.


\subsection{Vertexing} \label{sec:objects:vertices}
A vertex is the origin of tracks and therefore the point of particle interactions or particle decay. The \acf{pv} is of particular interest denoting the hard interaction of the partons in the colliding protons. Besides the \ac{pv}, secondary and tertiary vertices are also important, especially in heavy-flavour tagging covered in Chapter~\ref{chap:ftag_intro}.\\
The primary vertices within an event are iteratively reconstructed with an algorithm~\cite{Aaboud:2016rmg} which is briefly described in the following. Firstly, a set of tracks is defined satisfying certain selection criteria (similar the track requirements in sec.~\ref{sec:objects:tracks}) and a seed position of the first vertex is selected. Secondly, the tracks and the seed are utilised to estimate the best vertex position with an iterative fit. In each step, the vertex position is recomputed after down-weighting less compatible tracks. After the determination of the vertex position, all incompatible tracks are discarded to be used in another vertex. This procedure is then repeated with the remaining tracks in the event. The vertex with the largest quadratic \pt sum is defined as the \ac{pv}. For the \tthbb analysis in this thesis, only events with at least one \acs{pv} are used, to which two or more tracks are associated with $\pt>\unit[500]{MeV}$.

\subsection{Topo Clusters} \label{sec:objects:eclusters}
Vertices and tracks are reconstructed from the \acs{id} information, whereas the topological cell clusters, also called topo clusters, are iteratively reconstructed from calorimeter information~\cite{PERF-2014-07}.\\
Topologically connected cell signals are reconstructed to form 3-D 'energy blobs' from particle showers in the active calorimeter volume by extracting the significant signals over electronic noise and other fluctuations such as pile-up. This clustering is particularly effective in highly granular calorimeter systems used in ATLAS. The topo clusters are a full or fractional response to a single particle, merged response to several particles or a combination of the two.

\section{Physics Objects}

The physics objects used in this thesis are jets, electrons, muons, taus and missing transverse momentum. The definitions of these objects, their reconstruction algorithms and their performances are described in the following.

\subsection{Jets}\label{sec:objects:jets}
Due to the colour charge carried by quarks and gluons, they cannot be observed as free particles and form colourless hadrons. Jets are collimated showers formed by these hadrons. In fact, jets do not have a unique object definition. They rather depend on the chosen clustering algorithm which depend as little as possible on \acs{qcd} effects~\cite{Salam:2009jx}. Namely, the jet algorithm has to be \textit{collinear safe}, meaning that the jet configuration does not change by substituting one particle with two collinear particles, and it has to be \textit{infrared safe} for which the configuration should not change by adding soft particles.\\
In a jet, different detector objects are clustered together such as charged and neutral hadrons, photons (mostly from $\pi^0$ decays) as well as electrons and muons can be included.
In the detector, the charged particles in a jet first leave tracks in the \ac{id} and then deposit energy in the electromagnetic and hadronic calorimeters. Also neutral hadrons and photons deposit energy in the calorimeters. At the \acs{lhc}, the jet reconstruction 
% from topo clusters 
is typically performed using the \antikt algorithm~\cite{Cacciari_2008}. It is a clustering algorithm combining four-vector objects %(stable particles, charged-particle tracks, calorimeter energy deposits) 
into a cone-like object, a jet. The distance parameter $d_{ij}$ between object $i$ and $j$ defined as:
\begin{equation}
    d_{ij}=\min\left(p_{\text{T},i}^{2n},p_{\text{T},j}^{2n}\right)\frac{\Delta R^2_{ij}}{R^2},\label{eq:objects:antikt}
\end{equation}
with their transverse momentum $p_{\text{T},i/j}$ and the $\Delta R_{ij}$ between the two four-vector objects $i$ and $j$, allows a recursive recombination together with the distance of object $i$ to the beam axis
\begin{equation}
    d_{i,B}=p_{\text{T},i}^{2n}R^2,\label{eq:objects:beamdistance}
\end{equation}
with $R$ being the radius parameter and the exponent $n$ set to $-1$. The advantage of this choice ($n=-1$) is that the clustering prefers high momenta (hard) particles instead of soft ones which leads to an almost circular shape around the hardest particle as shown in Figure~\ref{fig:anti-kt}.\\
Based on the \antikt algorithm various jet collections are deployed in ATLAS, three of them will be used in this thesis: \emtopo, \textit{Particle Flow} jets and \textit{Variable Radius Track} jets.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.55\textwidth]{methodology/object_reconstruction/anti-kt-sketch.pdf}
	\caption{Illustration of the anti-$k_t$ clustering algorithm showing a circular (cone-like) structure around the track with the highest momentum~\cite{Cacciari_2008}. \label{fig:anti-kt}}
\end{figure}


\subsubsection{EMTopo Jets}
The so-called \emtopo are calorimeter jets reconstructed at the \ac{em} energy scale only using topo clusters~\cite{PERF-2016-04} with the \antikt algorithm implemented in the software package \textsc{FastJet}~\cite{Fastjet}. For the scope of this thesis, the radius parameter $R=0.4$ is used (for boosted topologies also jets with $R=1.0$ are used). Additionally, the jets have to satisfy $\pt > \unit[25]{GeV}$ and $|\eta|<2.5$. Until recently, the \emtopo were the primary jet collection, used in physics analyses in ATLAS, showing robust energy characteristics.\\
%They show robust energy scale and resolution characteristics across a wide kinematic range.\\
The calibration of \emtopo is performed in several steps illustrated in Figure~\ref{fig:jet-calib} correcting the four-momentum of the jet~\cite{JETM-2018-05}.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.65\textwidth]{methodology/object_reconstruction/jet-calibration}
	\caption{Different steps of the jet four momentum calibration~\cite{JETM-2018-05}.\label{fig:jet-calib}}
\end{figure}
After the jet reconstruction, the jet direction is modified at the topo cluster level, such that the jet originates from the primary vertex.  Then, \pt-density based pile-up corrections are applied including jet area information as well as a \acs{mc}-driven residual correction. The absolute jet energy calibration corrects the jets to agree in energy and direction with dijet \acs{mc} events. Then a global sequential calibration is set to improve the \pt resolution and the associated uncertainties. 
The final step is the \emph{in situ} calibration which is only applied to data. If there are still remaining differences between data and \acs{mc}, they are corrected at this step.


% Jets are reconstructed from noise-suppressed topological clusters of
% calorimeter energy depositions~\cite{PERF-2014-07} calibrated at the
% electromagnetic scale~\cite{JETM-2018-05}, using the \antikt\
% algorithm with a radius parameter of
% 0.4. These are referred to as small-$R$ jets. The average energy contribution from pile-up is subtracted
% according to the jet area and jets are calibrated as described in
% Ref.~\cite{JETM-2018-05} with a series of simulation-based corrections and \emph{in situ} techniques. Jets are required to satisfy $\pt > 25$~\GeV\
% and $|\eta|<2.5$.
% %Quality criteria are imposed to identify jets arising from
% %non-collision sources or detector noise, and any event containing such
% %a jet is removed~\cite{ATLAS-CONF-2015-029}.
% The effect of pile-up is reduced by an algorithm requiring that the
% calorimeter-based jets are validated as originating from the primary
% vertex using tracking information~\cite{PERF-2014-03}.

\subsubsection{Particle Flow Jets}
During RUN II, ATLAS introduced \textit{Particle Flow jets}, a new jet collection also denoted as \pflow. They combine tracking and calorimeter information in the jet reconstruction~\cite{Aaboud:2017aca} also using the  \antikt clustering algorithm with a radius parameter of $R=0.4$. \\
% They are designed to improve hadronic final state measurements by directly combining measurements from the tracking and calorimeter system to form the input for the jet reconstruction \\
The first step is to match the tracks from charged particles in the \ac{id} to the topo clusters from the calorimeter. In case of a successful match, the energy deposit of the topo cluster is replaced by the corresponding track momentum. The \antikt algorithm then takes as input the topo clusters that remain after substitution as well as tracks that match the hard-scattering \ac{pv}. The calibration follows closely the \textit{EMTopo} scheme performed in the range \mbox{$\unit[20]{GeV}<\pt<\unit[1500]{GeV}$}~\cite{JETM-2018-05}.\\
The advantage of \pflow is their improved energy and angular resolution compared to \emtopo. Also not negligible is the enhanced reconstruction efficiency and pile-up stability.


\subsubsection{Variable Radius Track Jets}
In boosted topologies, especially for the boosted \hbb decays, the two \bjets are very collimated and the hadronisation products of the two \quarks start to overlap at a certain \pt value. In order to avoid these overlaps and to improve \btag performance, a jet algorithm with a \ac{vr} parameter is employed~\cite{Krohn:2009zg} based on track jets formed from charged-particle tracks with $\pt > 0.5$~\GeV\ and $|\eta| < 2.5$. They will be further called \vrtrack. The radius parameter from the conventional \antikt algorithm from equations \eqref{eq:objects:antikt} and \eqref{eq:objects:beamdistance} is now \pt dependent:
\begin{equation}
    R\rightarrow R_\text{eff}(\pt)=\frac{\rho}{\pt},
\end{equation}
where $\rho$ is a parameter that controls how fast the effective jet size decreases with \pt.
In addition, a cut-off parameter is introduced to prevent too large jet radii $R_\textrm{max}$ as well as another cut-off to avoid the jet to shrink below the detector resolution $R_\textrm{min}$. These parameters are optimised for \hbb events~\cite{ATL-PHYS-PUB-2017-010,Aad:2019uoz} resulting in the following parameters: $\rho=\unit[30]{GeV}$, $R_\textrm{min}=0.02$ and $R_\textrm{max}=0.4$. Furthermore, only jets with at least two constituents, $\pt>\unit[10]{GeV}$ and $|\eta| < 2.5$ are considered~\cite{PERF-2016-05}. The track jets are not separately energy-calibrated, the energy is calculated via the sum of the track momenta of the tracks associated to the jet.

% Track-jets formed from charged-particle tracks 
%   Track-jets are built with the \antikt\
% algorithm with a variable radius (VR) \pt-dependent parameter, from
% tracks reconstructed in the inner detector with $\pt > 0.5$~\GeV\ and
% $|\eta| < 2.5$~\cite{Krohn:2009zg, ATL-PHYS-PUB-2017-010,
% Aad:2019uoz}. VR track-jets have an effective jet radius
% $R_\textrm{eff}$ proportional to the inverse of the jet \pt\ in the
% jet finding procedure: $R_\textrm{eff}(\pt) = \rho/\pt$, where the
% $\rho$-parameter is set to 30~\GeV. There are two additional
% parameters, $R_\textrm{min}$ and $R_\textrm{max}$, used to set the
% minimum and maximum cut-offs on the jet radius, and these are set to
% 0.02 and 0.4, respectively. Only VR track-jets with $\pt>10$~\GeV,
% $|\eta| < 2.5$ and with at least two constituents are considered~\cite{PERF-2016-05}.







\subsection{Electrons}\label{sec:objects:electrons}

Electrons are reconstructed using the information of the \acs{id} and the calorimeter system. The typical signature of an electron is that they leave a track in the \acs{id} and are then absorbed in the electromagnetic calorimeter where they leave an electromagnetic shower. The \tthbb analysis and the flavour tagging studies in this thesis use the algorithms described in detail in Ref.~\cite{ATL-PHYS-PUB-2017-022,EGAM-2018-01}.

\subsubsection{Reconstruction}
The electron object is constructed using a dynamic clustering algorithm with variable-size clusters, so-called superclusters. The reconstruction is performed in the region $|\eta_\text{cluster}|<2.47$ excluding the transition region of the barrel and end-cap ($1.37 <|\eta_\text{cluster}| < 1.52$).\\
% The reconstruction of electrons is based on a dynamic clustering algorithm with variable-size clusters, the super. % and can be divided into three main steps: firstly the preparation of the tracks and clusters, secondly the building of superclusters and thirdly the building of analysis objects.\\
At first, topo clusters (described in sec.~\ref{sec:objects:eclusters}) are selected and loosely matched to \acs{id} tracks.
Simultaneously, the conversion vertices matched to the topo clusters are built.
Next, the superclusters are built from matched clusters and a first position correction and energy calibration is applied. Tracks are then matched to the electron superclusters. % and at last, the energies are calibrated.\\
The energy scale and resolution of electrons are calibrated using $Z\rightarrow ee$ decays and validated in $Z\rightarrow \ell\ell\gamma$ decays~\cite{EGAM-2018-01}. In addition, the energy resolution of the electron is optimised using a multivariate regression algorithm based on the properties of shower developments in the electromagnetic calorimeter.

\subsubsection{Identification}
Further quality criteria are required for an electron object, passing several identification selections to improve the purity of the selected objects. The prompt electrons are identified using a likelihood discriminant which uses quantities measured in the \ac{id} and the electromagnetic calorimeter. These quantities are chosen such that they  discriminate well prompt isolated electrons from other energy deposits like jets, converted photons or genuine electrons stemming from heavy-flavoured hadron decays. Important observables for the likelihood calculation are based on the track quality in the \acs{id}, the lateral and longitudinal development of the electromagnetic shower described by shower shape variables as well as the particle identification in the \acs{trt}. The algorithm uses probability density functions as input which are derived for the signal from $Z\rightarrow ee$ ($E_T>\unit[15]{GeV}$) and $J/\psi\rightarrow ee$ ($E_T<\unit[15]{GeV}$) events.\\
The efficiency of the electron identification is provided in three operating points: \textit{Loose}, \textit{Medium} and \textit{Tight}, yielding different purities. Figure~\ref{fig:electron-calib} shows the data efficiency as a function of $E_T$ and as a function of the average number of bunch crossings for all three operating points. They are all optimised in 9 $|\eta|$ and 12 $E_T$ bins. For this thesis the \textit{Medium} and  \textit{Tight} operation points are used with an average target efficiency of $88\%$ and $80\%$ associated with an increased background process rejection of $\sim2.0$ and $\sim3.5$ with respect to the inclusive target efficiency, respectively (for $\unit[20]{GeV}<E_T<\unit[50]{GeV}$).
\begin{figure}[h]
    \centering
    \subfloat[]{\includegraphics[width=.5\textwidth]{methodology/object_reconstruction/fig_17a}}
    \subfloat[]{\includegraphics[width=.5\textwidth]{methodology/object_reconstruction/fig_16}}
    \caption{Measured electron identification efficiency in $Z\rightarrow ee$ data events as a function of $E_T$ (a) and as a function of the average number of interactions per bunch crossing $\langle\mu \rangle$ (b), for the \textit{Loose}, \textit{Medium} and \textit{Tight} operating point. The right plot also shows in grey the shape of the $\langle\mu \rangle$ distribution. The lower pad shows the Data/MC comparison~\cite{EGAM-2018-01}.\label{fig:electron-calib}}
\end{figure}


\subsubsection{Isolation Criteria}
Electrons are typically required to be spatially separated from other particles. There are two kinds of isolation variables: calorimeter-based and track-based.\\
The calorimeter-based isolation is calculated via the sum of the transverse energy of positive-energy topo clusters with a barycentre falling in a $\Delta R=0.2$ of the electron barycentre  (other than the electron clusters themselves). % other than the cluster part of the electron. 
In addition, %the core energy of the core of the electromagnetic particle cluster is removed and 
leakage and pile-up corrections are applied.\\
For the track-based isolation the sum of the transverse momentum of tracks within a cone centred around the electron track are considered, where the cone radius decreases with \pt. % ($\Delta R=\text{min}(\frac{10}{\pt\text{[GeV]}},\Delta R_\text{max})$
Moreover, only tracks are taken into account which have $\pt>\unit[1]{GeV}$ and $|\eta|<2.5$ as well as satisfy certain track quality criteria and have a loose vertex association\footnote{Vertices defined as loose association vertex are either used in the \ac{pv} fit or satisfy specific \acs{ip} criteria ($|\Delta z_0|\sin\theta<\unit[3]{mm}$).}. In this thesis the \textit{Gradient} isolation \ac{wp} is chosen which gives an efficiency of 90\% at $\pt=\unit[25]{GeV}$ and 99\% at $\pt=\unit[60]{GeV}$ uniform in $\eta$.


% from the conf note
% Electrons are reconstructed from tracks in the ID associated with
% topological clusters of energy depositions in the
% calorimeter~\cite{ATL-PHYS-PUB-2017-022} and are required to have
% $\pt>10$~\GeV\ and $|\eta|<2.47$.  Candidates in the calorimeter
% barrel--endcap transition region ($1.37 <|\eta| < 1.52$) are
% excluded. Electrons must satisfy the \textit{Medium} likelihood
% identification criterion~\cite{EGAM-2018-01}. 





\subsection{Muons}\label{sec:objects:muons}
Muons leave a track in the detector and traverse the calorimeter system typically without significant energy loss. Therefore, the muon is mainly reconstructed in the \acs{id} and the \acs{ms} sub-detector systems. The RUN II muon reconstruction and performance is described in detail in~\cite{PERF-2015-10}.


% Muon candidates are
% identified by matching ID tracks to full tracks or track segments
% reconstructed in the muon spectrometer, using the \textit{Loose}
% identification criterion~\cite{PERF-2015-10}. Muons are required to
% have $\pt>10$~\GeV\ and $|\eta|<2.5$. Lepton tracks must match the
% primary vertex of the event, i.e. they have to satisfy $|z_0\sin(\theta)|<0.5$~mm and
% $|d_0/\sigma(d_0)| < 5\,(3)$ for electrons (muons), where % $d_0$ and
% % $z_0$ are the longitudinal and transverse impact parameters defined
% % relative to the primary vertex position and $\sigma(d_0)$ is the $d_0$
% % uncertainty.
% $z_0$ is the longitudinal impact parameter relative to the primary vertex
% and $d_0$ (with uncertainty $\sigma(d_0)$) is the transverse impact parameter
% relative to the beam line.

\subsubsection{Reconstruction}

The muon reconstruction has two stages: first the independent reconstruction in the \acs{id} and \acs{ms} and secondly the combination of the two to form the muon tracks. The reconstruction in the \acs{id} is performed as for any other charged particle.\\
In the \acs{ms} at first a search for hit patterns is performed in each muon chamber to form segments. In the \acs{mdt} and nearby trigger chambers, hits are aligned on the trajectory in the bending plane of the detector using a Hough transformation and the segments are reconstructed with a straight line fit to hits found in each layer. The hits of the \acs{rpc} and \acs{tgc} provide measurements for the  plane orthogonal to the bending plane and in the \acs{csc} a combinatorial search in the $\eta$ and $\phi$ plane is utilised to build the segments. Given this information, muon track candidates are constructed by fitting segments from different layers using a global $\chi^2$ fit.\\
The combined reconstruction is based on various algorithms defining four different types of muons. The combined (CB) muons are first independently reconstructed in the \acs{id} and \acs{ms} and then their information is combined with an outside-in approach, extrapolating reconstructed tracks from the \acs{ms} to the \acs{id} (complementary an inside-out approach is also used). The segment-tagged (ST) muons are mainly reconstructed from tracks in the \acs{id} extrapolated to typically one track segment in the \acs{mdt} and \acs{csc}. The third type are the calorimeter-tagged (CT) muons where an \acs{id} track is matched to an energy deposit in the calorimeter compatible with a minimal ionising particle. This muon type has the lowest purity and is optimised for the region $|\eta|<0.1$ with $\unit[15]{GeV}<\pt<\unit[100]{GeV}$. There are also extrapolated (ME) muons that are only reconstructed in the \acs{ms} extending the acceptance to $2.5<|\eta|<2.7$ but they are not used in this thesis.
% The extrapolated (ME) muons are only built from \acs{ms} tracks with a loose requirement that they are compatible to originate from the \acs{id} and are mainly used to extend the acceptance to $2.5<|\eta|<2.7$ which is not covered by the \acs{id}. In this case, the muon is required to traverse at least two layers of the \acs{ms} chambers (three in the forward region). An overlap removal between the different muon types avoids a double-counting before a collection of muons for the physics analyses is created.



% \subsubsection{Calibration}

% Reconstruction efficiency for the Loose muon selection as a function of the pT of the muon, for muons with 0.1<|η|< 2.5 as obtained with Z→μμ and J/ψ→μμ events. The error bars on the efficiencies indicate the statistical uncertainty. The panel at the bottom shows the ratio of the measured to predicted efficiencies, with statistical and systematic uncertainties.

\subsubsection{Identification}

Similarly to the electron identification, the muon identification is performed applying quality criteria to suppress background processes.% (mainly from pion and kaon decays). 
The goal is to identify prompt muons with high efficiency and a good momentum resolution which requires a certain amount of hits in the \acs{id} and the \acs{ms}. To cover different needs of physics analyses, four different muon \acp{wp} are available: \textit{Loose}, \textit{Medium}, \textit{Tight} and \textit{high \pt}. For the scope of this thesis, the \textit{Medium} and \textit{Loose} \acp{wp} are used.\\
For the \textit{Medium} \ac{wp} only combined muons are taken into account. The combined muons are required to have three or more hits in at least two \acsp{mdt} layers except for the $|\eta|<0.1$ region where only one \acsp{mdt} layer is sufficient combined with no more than one hole layer due to a gap in the \acs{ms}. This \ac{wp} tries to minimise systematic reconstruction and calibration systematic uncertainties associated with the muon.
The \textit{Medium} \acs{wp} reconstruction efficiency with $\pt>\unit[20]{GeV}$ is 96.1\%.\\
The \textit{Loose} \ac{wp}  maximises the reconstruction efficiency with good-quality muon tracks. In this case, all muon types are utilised. In fact, the combined muons are used as they are from the \textit{Medium} selection. Additionally, the calorimeter-tagged and segment-tagged muons are taken into account for $|\eta|<0.1$. The reconstruction efficiency for muons with $\pt>\unit[20]{GeV}$ is 98.1\%.\\
Figure~\ref{fig:muon-eff} shows the reconstruction efficiency measured in data for the two described \acsp{wp}  obtained from $Z\rightarrow\mu\mu$ and $J\psi\rightarrow\mu\mu$ events.


\begin{figure}[h]
    \centering
    \subfloat[]{\includegraphics[width=.5\textwidth]{methodology/object_reconstruction/figaux_07}}
    \subfloat[]{\includegraphics[width=.5\textwidth]{methodology/object_reconstruction/fig_06}}
    \caption{The reconstruction efficiency as a function of the muon \pt for the \textit{Loose} (a) and the \textit{Medium} (b) muon selection obtained from $Z\rightarrow\mu\mu$ and $J\psi\rightarrow\mu\mu$ events with $0.1<|\eta|<2.5$. The Data/MC ratio in the lower pad includes systematic and statistical uncertainties while the efficiencies only show statistical uncertainties ~\cite{PERF-2015-10}.\label{fig:muon-eff}}
\end{figure}

\subsubsection{Isolation}

Analogously to the electron isolation strategy, the muon isolation is assessed via track- and calorimeter-based variables with very similar definitions. 
The track-based variable $\pt^\text{varcone30}$ is the scalar \pt sum of all tracks, excluding the muon track, with $\pt>\unit[1]{GeV}$ in a radius of $\Delta R=\min(\unit[10]{GeV}/p_T^\mu,0.3)$ around the muon transverse momentum $\pt^\mu$. The calorimeter isolation variable is constructed from the sum of transverse energies around the muon track, as described for electrons. However, for the scope of this thesis the isolation \ac{wp} \textit{FixedCutTightTrackOnly} is utilised which is only using the track-based isolation satisfying $\pt^\text{varcone30}/\pt^\mu<0.06$. The isolation efficiencies are measured using $Z\rightarrow\mu\mu$ events.


\subsection{Taus}\label{sec:objects:taus}
$\tau$-leptons can decay either leptonically (into electrons or muons) or hadronically. The leptonic decays are similarly reconstructed as electrons or muons. The $\tau$ decays with a hadronic final state are seeded by jets which are required to have $\pt>\unit[10]{GeV}$ and $|\eta|<2.5$ excluding the barrel-end-cap transition region~\cite{ATLAS-CONF-2017-029}. Tau leptons are calibrated to correct their energy deposit in the detector to the average value at generator level. The $\tau$ identification is based on \acp{bdt} discriminating $\tau$-jets from the quark- and gluon-initiated background jets. Three different efficiency \acp{wp} are defined: \textit{Loose}, \textit{Medium} and \textit{Tight}. In this thesis the \textit{Medium} $\tau$-\ac{wp} and the requirement $\pt>\unit[25]{GeV}$ is used as well as an isolation criterion of $\Delta R_y<0.2$ between a $\tau_\text{had}$ candidate and any selected electron or muon.





% from conf note
% Hadronically decaying $\tau$-leptons (\tauhad) are distinguished from
% jets using their track multiplicity and a multivariate discriminant
% based on calorimetric shower shapes and tracking
% information~\cite{ATLAS-CONF-2017-029}.  They are required to have
% $\pt>25$~\GeV, $|\eta| < 2.5$ and pass the \textit{Medium}
% $\tau$-identification working point.

% \newpage 
\subsection{Missing Transverse Momentum}\label{sec:objects:met}

The missing transverse momentum, also denoted as \met is the negative vector sum of fully calibrated electrons, muons, photons, hadronically decaying $\tau$-leptons and jets denoted as the hard term as well as soft objects coming from additional tracks associated to the \ac{pv}~\cite{PERF-2016-07}. The partons inside the proton are following a momentum distribution (see sec.~\ref{sec:phys-hadcol:pdfs}) and the \com system of the hard scattering is not at rest w.r.t. the lab system. 
Therefore, the known quantity in an ideal detector is the transverse momentum \pt which is 0 at the time of the interaction. 
However, not all objects are always detected, e.g. neutrinos leave the detector unseen. So \met is a measure of the neutrinos that escape detection. The vector of the missing transverse momentum can be split into a scalar part \met and an azimuthal angle $\phi^\text{miss}$ which are defined as
\begin{align}
    \met&=\sqrt{(E_x^\text{miss})^2+(E_y^\text{miss})^2},\\
    \phi^\text{miss}&=\tan^{-1}(E_y^\text{miss}/E_x^\text{miss}),
\end{align}
with $E_{x,y}$ the $x$ and $y$ components of the missing transverse momentum
\begin{equation}
  E^\text{miss}_{x,y}=-\sum_{i\in\text{hard objects}}  p_{x,y}^i-\sum_{j\in\text{soft signals}}  p_{x,y}^j.
\end{equation}
In general, overlaps of jets with electrons, muons or photons are taken into account and are corrected.  The \met is of interest in this thesis because the leptonic final states in the \tthbb analysis contain neutrinos which are not detected and only appear as missing transverse momentum.
% The calibration of \met is performed according to the object-specific schemes.


% \begin{align}
%     \met &= \sum E_T^{\text{miss},e} + \sum E_T^{\text{miss},\gamma}+\sum E_T^{\text{miss},\tau_\text{had}}+\sum E_T^{\text{miss},\mu}+\sum E_T^{\text{miss},jet} + \sum E_T^{\text{miss},\text{soft}}
% \end{align}



% The missing transverse momentum (with magnitude \met) is reconstructed
% as the negative vector sum of the \pt of all the selected electrons,
% muons, \tauhad\ and jets described above, with an extra `soft term'
% built from additional tracks associated to the primary vertex, to make
% it resilient to pileup contamination~\cite{PERF-2016-07}. The missing
% transverse momentum is not used for event selection but is included in
% the inputs to the multivariate discriminants that are built in the
% most sensitive analysis categories (see Section~\ref{sec:Regions}).