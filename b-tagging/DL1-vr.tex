\chapter{Variable Radius Track Jets Training}\label{chap:ftag:dl1:vr}
Track jets are of special interest for boosted topologies and have a variable radius depending on their jet \pt, described in more detail in Section~\ref{sec:objects:jets}. The first dedicated training for these jets is presented in the following. The basic training principle with the preprocessing chain and the architecture is taken from the \pflow training described in chapters \ref{chap:ftag_intro} and \ref{chap:ftag:dl1:pflow}. The adaptations for the application to \vrtrack and their performance are described in this chapter.




\section{Training Sample Creation}
The input features for the different tagger versions (see Fig.~\ref{fig:ftag:dl1structure}) are the same as for \pflow. The \pt distribution of the \vrtrack, as shown in Figure~\ref{fig:ftag:vr:ptall}, is quite different compared to the \pflow. The \pt spectrum of the \textit{extended} $Z'$ sample is not as flat as for \pflow for high transverse momenta and consequently, the statistics is not as high in the high \pt regime. Additionally, the \textit{standard} $Z'$ is also shown which has a steeper decrease in \pt than the extended version.\\
\begin{figure}[h]
    \centering
    \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pt_btagJes-full_spectrum-bjets.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pt_btagJes-full_spectrum-ujets.pdf}}\\
    \caption{\pt distributions of the \ttbar sample , $Z'$ sample and \textit{extended} $Z'$ for (a) \bjet{}s and (b) light-flavour jets.}
    \label{fig:ftag:vr:ptall}
\end{figure}


The hybrid sample definition for the \vrtrack turned out to not be as straight forward as for \pflow. Already in the retraining of RNNIP for \vrtrack, it was found that the \textit{extended} $Z'$ sample hurt the training and the training on the hybrid sample could not reproduce the training on the dedicated samples (\ttbar and \textit{extended} $Z'$ separately). The solution in this case was to use only the leading and subleading jets from the \textit{standard} $Z'$ sample  as well as lowering the sample transition cut to $\pt=\unit[125]{GeV}$.\\
For the \acs{dl1} training, these studies were repeated and also here it  was observed that the hybrid definition from \pflow applied to \vrtrack degrades the performance (background rejection). The \pt distributions for $b$- and light-flavour jets in Figure~\ref{fig:ftag:vr:pt-ranks} reveal several differences among them. The \pt distributions are shown for all jets in an event (inclusive), for the four leading jets (the four jets with the highest \pt in an event) and the two leading jets. The number of light-flavour jets at low \pt is reduced when considering the four leading jets and even more by only considering the first two leading jets. This is expected since soft initial and final state radiation is dominated by light constituents. Since the \ttbar sample is used to populate the lower \pt range, the four leading jets are chosen to be taken into account for the training.\\
The \textit{standard} $Z'$ sample retains more statistics compared to the \textit{extended} $Z'$ by requiring only the first two or four leading jets. Both scenarios: using either the \textit{standard} $Z'$ or the \textit{extended} $Z'$ in the training were investigated. Different training studies were performed to optimise the hybrid composition. The \textit{extended hybrid} sample showed strong overfitting and the training only converged by regulating it with \textit{Dropout}. However, \textit{Dropout} introduced the uneven distributions of the output discriminants as already described for \pflow in Section~\ref{sec:ftag:pflow:finaldiscriminant}. In addition, the training with the  \textit{standard} $Z'$ sample showed adequate results when applied to the \textit{extended} $Z'$ sample which was not the case the other way around. The additional advantage is that the \textit{standard} $Z'$ sample is available for all three data taking periods and thus more statistics can be used in the training. Since $Z'$ is used for the higher \pt regime, only the two leading jets are utilised. To ensure a smooth transition between the two samples, the \pt cut for \ttbar is chosen to be higher (\unit[400]{GeV}) compared to the \pflow, the lower \pt cut for $Z'$ is reduced to \unit[125]{GeV} and an upper limit of \unit[3]{TeV} is applied. All hybrid selections are summarised in Table~\ref{tab:ftag:vr:hybrid-cuts}.\\
The choice of the hybrid training sample turned out to be the most critical part of the \vrtrack training. Once this was solved the further training was analogous to the \pflow procedure.
\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pt_btagJes-pt-comp-ttbar-bjets}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pt_btagJes-pt-comp-ttbar-light}}\\
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pt_btagJes-pt-comp-zprime-bjets}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pt_btagJes-pt-comp-zprime-light}}\\
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pt_btagJes-pt-comp-ext-zprime-bjets}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pt_btagJes-pt-comp-ext-zprime-light}}\\
  \caption{\pt distributions for all jets in an event (inclusive), the four leading jets and the two leading jets for the \ttbar sample (a) \& (b), $Z'$ (c) \& (d) and \textit{extended} $Z'$ (e) \& (f). The left column shows the \bjet \pt distributions and the right column the light-flavour jet \pt spectra. The distribution with the label \textit{inclusive} is normalised to unity and all other distributions are then normalised to the \textit{inclusive} integral.}
  \label{fig:ftag:vr:pt-ranks}
\end{figure}
\begin{table}[h]
    \centering
    \renewcommand{\arraystretch}{0.6}
    \begin{tabular}{lll}
    \toprule
         \textbf{Jet Flavour} & \ttbar &  \textit{standard} $Z'$\vspace{0.3cm}\\
         \toprule
         all flavours & 4 leading jets & 2 leading jets\vspace{0.3cm}\\
         \midrule
         \bjet{}s & $b\text{-hadron}\; \pt < \unit[400]{GeV}$ & $\unit[125]{GeV} < b\text{-hadron } \pt < \unit[3]{TeV} $\vspace{0.3cm}\\
         \midrule
         \cjet{}s & \multirow{2}{*}{$\text{jet}\; \pt < \unit[400]{GeV}$} & \multirow{2}{*}{$\unit[125]{GeV} < \text{jet}\; \pt < \unit[3]{TeV} $}\vspace{0.3cm}\\
         light-flavour jets & & \\
        \bottomrule
    \end{tabular}
    \caption{Selections used to build the hybrid training sample for \vrtrack.}
    \label{tab:ftag:vr:hybrid-cuts}
\end{table}


The resampled \pt and $|\eta|$ distributions are illustrated in Figure~\ref{fig:ftag:vr:pteta}. The 2-D binning has been optimised for the \vrtrack to cope for the different \pt range with respect to \pflow.
In total, 20M jets are left after the resampling and can be used for the training.
%  The processing of the variables by shifting, scaling and replacing the default values is done in the same way as for \pflow described in detail in Section~\ref{sec:ftag:dl1:preprocessing}. 
 The resulting variable distributions with the full preprocessing applied are shown in Figures~\ref{fig:ftag:vr:varsummary1} and \ref{fig:ftag:vr:varsummary2}.

\begin{figure}[htbp]
  \centering
  %\includegraphics[trim=0 620 0 0,clip,width=\linewidth]{inputs_IPxformed}
  \subfloat[]{\includegraphics[width=0.6\textwidth]{b-tagging/material_dl1_general/vr/pt_btagJes-downsampled.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.6\textwidth]{b-tagging/material_dl1_general/vr/eta-downsampled.pdf}}\\
  \caption{(a) the \pt and (b) the $|\eta|$ distribution of the hybrid sample resampled to match the \bjet{} spectrum.}
  \label{fig:ftag:vr:pteta}
\end{figure}


% \begin{figure}[h]
%     \centering
%     \includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/vr/vr-summary-scaled.pdf}
%     \caption{Input variables used for the \acs{dl1} tagger family training, fully preprocessed (resampled, scaled, shifted and default values replaced).}
%     \label{fig:ftag:vr:varsummary}
% \end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/vr/vr-summary-scaled-split1.pdf}
    \caption{First part (of 2) of input variables used for the \acs{dl1} tagger family training, fully preprocessed (resampled, scaled, shifted and default values replaced).}
    \label{fig:ftag:vr:varsummary1}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/vr/vr-summary-scaled-split2.pdf}
    \caption{Second part (of 2) of input variables used for the \acs{dl1} tagger family training, fully preprocessed (resampled, scaled, shifted and default values replaced).}
    \label{fig:ftag:vr:varsummary2}
\end{figure}



\FloatBarrier
\section{Tagger Training}
After the hybrid training sample was optimised ensuring a robust training, the training itself is similar to the \pflow training. Similar to the hyperparameter optimisation described in Section~\ref{sec:ftag:pflow:hpopt} a small scan is performed with again no better configuration found than the by-hand designed architecture which is summarised in Table~\ref{tab:ftag:vr:final-nn}. In fact, the architecture is the same as for \pflow (see Table~\ref{tab:ftag:pflow:final-nn}) except the increased training batch size.
\begin{table}[h]
    \centering
    \renewcommand{\arraystretch}{0.6}
    \begin{tabular}{ll}
    \toprule
         \textbf{Hyperparameter} & \textbf{Values}  \\
         \toprule
         $N_\text{hidden layers}$ &  $8$ \vspace{0.3cm} \\
          $N_\text{nodes/layer}$  & $[256, 128, 60, 48, 36, 24, 12, 6]$    \vspace{0.3cm}\\
         learning rate &  $0.01$\vspace{0.3cm} \\
         Training batch size &  $45000$ \vspace{0.3cm} \\
         Activation function &  ReLu \vspace{0.3cm} \\
         \midrule
         Free (trainable) parameters & $59,275$  \vspace{0.3cm} \\
         Fixed parameters &  $1,140$ \vspace{0.3cm} \\
         Training sample statistic &  $20$M\\
        \bottomrule
    \end{tabular}
    \caption{Final network architecture for the \dlr tagger for \vrtrack.}
    \label{tab:ftag:vr:final-nn}
\end{table}

The training is monitored to ensure that no overfitting is occurring and to see if the training is robust. Figure~\ref{fig:ftag:vr:train-monitor} shows the loss and accuracy for both the training and validation sample. Also here, the loss and accuracy converge after around 110 epochs. 
\begin{figure}[h]
  \centering
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/vr-loss-monitor.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/vr-accuracy-monitor.pdf}}
  \caption{Training and validation (a) loss and (b) accuracy as a function of the training epoch. \dlr training with 20M training jets as indicated in Table~\ref{tab:ftag:vr:final-nn}.}
  \label{fig:ftag:vr:train-monitor}
\end{figure}
After a first decrease in the training loss, it stays constant around epoch 60 until a small drop occurs around epoch 85 and again a smaller drop around epoch 95. Interestingly, after these drops, the validation loss stabilises more and more. These drops are coming from the learning rate scheduler which reduces the learning rate, when the training loss did not change after a certain amount of epochs. Often, if the learning rate is too large, the network is jumping over the minimum and by decreasing the learning rate it can help the network to find the minimum. This seems to exactly happen in this case.\\
In addition, the background rejection at the $77\%$ \acs{wp} is monitored for the validation sample illustrated in Figure~\ref{fig:ftag:vr:monitor-rej}. After fluctuations in the first epochs, similar to the loss, the background rejection also converges. The network does not show signs for overfitting, on the one hand verified by the convergence of the validation loss and on the other hand, the same performance comparison as shown for \pflow in Figure~\ref{fig:ftag:overtrain-rej} also yielded the same conclusion.\\
The $f_c$ values were chosen to be identical to the values from the \pflow which provided again a good compromise between the $c$- and light-flavour jet rejection.
\begin{figure}[htbp]
    \centering
    \includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/vr/vr-monitor.pdf}
    \caption{Light-flavour jet rejection (green) and \cjet rejection (orange) as a function of the training epoch evaluated on the validation \ttbar sample during the training. \dlr training with 20M training jets as indicated in Table~\ref{tab:ftag:vr:final-nn}.}
    \label{fig:ftag:vr:monitor-rej}
\end{figure}



\newpage
\section{Performance Overview and Tagger Variants Comparison}\label{sec:ftag:vr:perfoverview}
The architecture of the \dlr tagger defined in Table~\ref{tab:ftag:vr:final-nn} is used also for the \dlb and \dlrmu training. The resulting final \btag discriminant is shown in Figure~\ref{fig:ftag:vr:dl1-dl1rmudiscs} for all three variants. The same behaviour as for the \pflow training is also observed here, the distributions are smooth enough to allow a good \acs{wp} definition and the \bjet{}s are peaking more towards higher values the more information is used in the training.\\
\begin{figure}[h]
  \centering
    \subfloat[]{\includegraphics[width=0.45\textwidth]{b-tagging/material_dl1_general/vr/vr-dl1_disc}}\hspace{0.3cm}
       \subfloat[]{\includegraphics[width=0.45\textwidth]{b-tagging/material_dl1_general/vr/vr-dl1r_disc}}\hspace{0.3cm}
    \subfloat[]{\includegraphics[width=0.45\textwidth]{b-tagging/material_dl1_general/vr/vr-dl1rmu_disc}}
  \caption{The (a) \dlb, (b) \dlr and (c) \dlrmu  discriminants, the vertical dashed lines are indicating the \acsp{wp}.}
  \label{fig:ftag:vr:dl1-dl1rmudiscs}
\end{figure}

The \pt dependent performance comparisons are shown in Figures~\ref{fig:ftag:vr:pt-flat-comp}-\ref{fig:ftag:vr:pt-incl-comp}. A constant efficiency per \pt bin is maintained for the plots in Figure~\ref{fig:ftag:vr:pt-flat-comp} and \ref{fig:ftag:vr:pt-flat-comp-extZ}. For lower transverse momenta, the performance is evaluated on a \ttbar sample (Fig.~\ref{fig:ftag:vr:pt-flat-comp}) where the \dlr tagger outperforms the \dlb tagger and the \dlrmu tagger again the \dlr tagger in each \pt bin whereby especially for the last two bins the performance differences are well compatible between the three variants within their uncertainties. The higher transverse momenta regime is investigated on a \textit{standard} $Z'$ sample (Fig.~\ref{fig:ftag:vr:pt-flat-comp-extZ}). Also similar to the observations for \pflow, the biggest performance gain at high \pt is achieved by employing the RNNIP information.\\% while the soft-muon information is not really contributing there.\\

The performance as a function of \pt for the $77\%$ fixed cut \acs{wp} is shown in Figure~\ref{fig:ftag:vr:pt-incl-comp}. The \bjet efficiency of the \ttbar sample (Fig.~\ref{fig:ftag:vr:pt-incl-comp}~(e)) is consistent between the three tagger variants, also here it is not constant over the full \pt range but raises in the beginning and flattens out.
\begin{figure}[h]
  \centering
  \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/c-rejection_vs_pT_flatEff_DL1-22M-ttbar-WP_77.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/l-rejection_vs_pT_flatEff_DL1-22M-ttbar-WP_77.pdf}}
  \caption{\pt dependent performance for a constant \bjet efficiency of $77\%$ per bin for the (a) \cjet rejection and (b) light-flavour jet rejection for the \ttbar sample.}
  \label{fig:ftag:vr:pt-flat-comp}
\end{figure}
\begin{figure}[h]
  \centering
  \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/c-rejection_vs_pT_flatEff_vr-DL1-22M-Z-WP_77}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/l-rejection_vs_pT_flatEff_vr-DL1-22M-Z-WP_77}}
  \caption{\pt dependent performance for a constant \bjet efficiency of $77\%$ per bin for the (a) \cjet rejection and (b) light-flavour jet rejection for the \textit{extended} $Z'$ sample.}
  \label{fig:ftag:vr:pt-flat-comp-extZ}
\end{figure}
\begin{figure}[h]
  \centering
  \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/c-rejection_vs_pT_DL1-22M-ttbar-WP_77.pdf}}\hspace{0.3cm}
   \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/c-rejection_vs_pT_vr-DL1-22M-Z-WP_77.pdf}}\hspace{0.3cm}\\
   
   \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/l-rejection_vs_pT_DL1-22M-ttbar-WP_77.pdf}}
   \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/l-rejection_vs_pT_vr-DL1-22M-Z-WP_77.pdf}}\\

 
   \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/b-efficiency_vs_pT_DL1-22M-ttbar-WP_77.pdf}}
  \subfloat[]{\includegraphics[height=0.36\textwidth]{b-tagging/material_dl1_general/vr/pts/b-efficiency_vs_pT_vr-DL1-22M-Z-WP_77.pdf}}\\
  \caption{\pt dependent performance for an inclusive \bjet efficiency of $77\%$ defined on an inclusive \ttbar sample (official ATLAS cut value) for the (a) \& (b) \cjet rejection, (c) \& (d) light-flavour jet rejection and (e) \& (f) the $b$-jet efficiency. The evaluation is done on a \ttbar sample (left column) and on a \textit{standard} $Z'$ sample (right column).}
  \label{fig:ftag:vr:pt-incl-comp}
\end{figure}
This is again in contrast to the behaviour at high transverse momenta demonstrated on the \textit{standard} $Z'$ sample (Fig.~\ref{fig:ftag:vr:pt-incl-comp}~(f)). In this case, the \bjet efficiency decreases with \pt, however, not as drastically as for \pflow. Similarly, the \dlb \bjet efficiency falls faster than for \dlr and \dlrmu. While the performance order is retained on the \ttbar sample for the \bjet rejection (Fig.~\ref{fig:ftag:vr:pt-incl-comp}~(a)) and light-flavour-jet rejection (Fig.~\ref{fig:ftag:vr:pt-incl-comp}~(c)), i.e. $\dlb<\dlr<\dlrmu$, this order is inverted for the \cjet rejection at high \pt on the \textit{standard} $Z'$ sample (Fig.~\ref{fig:ftag:vr:pt-incl-comp}~(b)) due to the lower \bjet efficiency. However, the light-flavour jet rejection (Fig.~\ref{fig:ftag:vr:pt-incl-comp}~(d)) does not suffer so much from this effect. Still, it is also visible at $\pt>\unit[2]{TeV}$ but in this regime, the statistical uncertainties are so large that they cover this effect.




% \begin{figure}[h]
%   \centering
%   \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pts/c-rejection_vs_pT_vr-DL1-22M-Z-WP_77.pdf}}\hspace{0.3cm}
%   \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pts/l-rejection_vs_pT_vr-DL1-22M-Z-WP_77.pdf}}\\
%   \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/vr/pts/b-efficiency_vs_pT_vr-DL1-22M-Z-WP_77.pdf}}
%   \caption{\pt dependent performance for an inclusive \bjet efficiency of $77\%$ defined on an inclusive \ttbar sample (official ATLAS cut value) for the (a) \cjet rejection, (b) light-flavour jet rejection and (c) the $b$-jet efficiency per as function of \pt for the \textit{standard} $Z'$ sample.}
%   \label{fig:ftag:vr:pt-incl-comp-zprime}
% \end{figure}
\FloatBarrier
The inclusive \pt performance as a function of the \bjet efficiency is shown in Figure~\ref{fig:ftag:vr:tagger-comp} comparing the three tagger variants. A similar picture emerges compared to \pflow. The additional RNNIP information improves the \cjet and the light-flavour jet rejection by about $10\%$ (with some fluctuations) to a \bjet efficiency of $80\%$. At this point, the light-flavour jet rejection increases while the \cjet rejection decreases. The general trend when including the additional soft muon information follows the one of \dlr with an increased performance of about $10\%$ in the light-flavour jet rejection (increases slightly more at a \bjet efficiency of $80\%$). Additionally, an improvement of around $25\%$ in terms of the \cjet rejection is achieved up to a \bjet efficiency of $75\%$ where it starts decreasing again. The larger improvement in the \cjet rejection compared to the light-flavour rejection is due to the $f_c$ value of $0.03$. It was decided to keep this consistent with the \pflow results since in general the \dlrmu tagger cannot be calibrated. % and the training is only performed for R\&D purposes.\\


Also, for \vrtrack the tagger \dlr will become the new recommendation for physics analyses in ATLAS. Thus the comparison to the previous recommendation in ATLAS (MV2c10 and \dlb optimised on \emtopo) is important to quantify the improvements achieved with the newly optimised \dlr tagger. 
Figure~\ref{fig:ftag:vr:tagger-comp-recomm} shows this comparison as a function of the \bjet efficiency with large improvements in both $c$- and light-flavour jet rejection of up to $60\%$ and $120\%$ at $60\%$ \acs{wp}, respectively. 
The light-flavour jet rejection performance gain is more constant over the full \bjet efficiency spectrum while for the \cjet rejection there is almost no gain for \bjet efficiencies larger than $80\%$ and the rejection is only slightly better for \bjet efficiencies around $75\%$.


\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=0.5\textwidth]{b-tagging/material_dl1_general/vr/vrtrack_c-rej_beff-tagger-comp}}
  \subfloat[]{\includegraphics[width=0.5\textwidth]{b-tagging/material_dl1_general/vr/vrtrack_u-rej_beff-tagger-comp}}
  \caption{Performance comparison of the three different \acs{dl1} tagger versions: \dlb (blue), \dlr (red) and \dlrmu (orange) for the (a) \cjet rejection and (b) light-flavour jet rejection as a function of the \bjet efficiency.}
  \label{fig:ftag:vr:tagger-comp}
\end{figure}
\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=.5\textwidth]{b-tagging/material_dl1_general/vr/vr_c-rej_beff-tagger-ttbar-recom}}
  \subfloat[]{\includegraphics[width=.5\textwidth]{b-tagging/material_dl1_general/vr/vr_u-rej_beff-tagger-ttbar-recom}}
  \caption{Performance comparison of the previous recommendations (MV2 and DL1 (2018)) and the newly optimised \dlr tagger for the (a) \cjet rejection and (b) light-flavour jet rejection as a function of the \bjet efficiency.}
  \label{fig:ftag:vr:tagger-comp-recomm}
\end{figure}

% \begin{figure}[htbp]
%   \centering
%   \subfloat[]{\includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/vr/vrtrack_c-rej_beff-tagger-comp}}\\
%   \subfloat[]{\includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/vr/vrtrack_u-rej_beff-tagger-comp}}
%   \caption{Performance comparison of the three different \acs{dl1} tagger versions: \dlb (blue), \dlr (red) and \dlrmu (orange) for the (a) \cjet rejection and (b) light-flavour jet rejection as a function of the \bjet efficiency.}
%   \label{fig:ftag:vr:tagger-comp}
% \end{figure}


% \begin{figure}[htbp]
%   \centering
%   \subfloat[]{\includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/vr/vr_c-rej_beff-tagger-ttbar-recom}}\\
%   \subfloat[]{\includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/vr/vr_u-rej_beff-tagger-ttbar-recom}}
%   \caption{Performance comparison of the previous recommendations (MV2 and DL1 (2018)) and the newly optimised \dlr tagger for the (a) \cjet rejection and (b) light-flavour jet rejection as a function of the \bjet efficiency.}
%   \label{fig:ftag:vr:tagger-comp-recomm}
% \end{figure}