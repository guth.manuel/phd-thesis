\chapter{Ongoing Developments in Flavour Tagging}\label{chap:ftag:outlook}
The retraining campaign described in the two previous chapters delivered a large performance gain for \btag in ATLAS. The \acs{nn}-based tagger architectures provide a flexible basis for further developments in this domain. 
% Especially at large transverse momenta, further improvements are necessary to achieve a better performance. In that regime hit-based methods are studied within ATLAS using the hit information from the \acl{id}. This is an ideal use case for so-called \textit{Graph Neural Networks}~\cite{zhou2018graph}. Not only in the tagger algorithm development itself, but also in the calibration, especially in the light-flavour jet calibration, \acs{nn} techniques would bring a gain. It would be for instance possible to employ \textit{Invertible Neural Networks}~\cite{ardizzone2019analyzing} to allow a better calibration with the flipped taggers.\\
The next logical step is to perform a joint training of the \acs{dl1} tagger and the successor of RNNIP, described in more detail below.



% \section{Calibration}\label{sec:ftag:outlook:calibration}




\section{Umami}\label{sec:ftag:outlook:umami}
Inspired from the Japanese word \umami for the fifth basic taste, or the fifth flavour, a new end-to-end tagger is being developed. It joins the developments of the high-level tagger optimised in this thesis and the recent improvements in the track based tagger \ac{dips}~\cite{ATL-PHYS-PUB-2020-014}, the successor of RNNIP. The advantage of the Deep Sets architecture~\cite{zaheer2018deep} of the \ac{dips} tagger compared to RNNIP is its sum pooling layer. 
A pooling operation is a lossy summary of a set of features. 
Mostly an average, maximum or sum pooling is used which takes the average of several layers, the maximum value or their sum. For RNNIP the order in which the tracks are passed to the network matters due to its underlying Recurrent Neural Network structure. The tracks are ordered by the signed impact parameter $s_{d_0}$. However, the tracks originating from $b$-hadron decays do not have a physically motivated ordering per se.  With the new architecture, the ordering is obsolete since the sum pooling layer is permutation invariant. As demonstrated in Ref.~\cite{ATL-PHYS-PUB-2020-014}, and shown in Figure~\ref{fig:ftag:dips:performance:note}, the \ac{dips} architecture outperforms RNNIP.
% and even outperforms it by loosening the track selection. 
Another important advantage is its parallelisability which reduces the training time by more than a factor three~\cite{ATL-PHYS-PUB-2020-014} as well as its evaluation time. 
% This allows an easier development of the integration into the \acs{dl1} architecture. 

\begin{figure}[h]
  \centering
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_ftag_outlook/fig_04b.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_ftag_outlook/fig_04a.pdf}}\\
  \caption{Performance comparison of the RNNIP (green) and the \acs{dips} (purple) algorithms in terms of the (a) \cjet rejection and (b) the light-flavour jet rejection as a function of the \bjet efficiency.
  The uncertainty bands originate from the standard deviation of five trainings~\cite{ATL-PHYS-PUB-2020-014}.}
  \label{fig:ftag:dips:performance:note}
\end{figure}

\newpage
\subsection{DIPS Network}
The \ac{dips} architecture is shown in Figure~\ref{fig:ftag:umamiNN} as a part of the \umami architecture. The sub-networks $\phi$ and $F$ represent the \ac{dips} part. Each track is characterised by its track variables listed in Table~\ref{table:dips:inputs}. The corresponding variable distributions are shown in Figure~\ref{fig:ftag:umami:track-vars} for the hybrid sample composed of \ttbar and \textit{extended} $Z'$ for \pflow as it was used for the \acs{dl1} \pflow training (see Section~\ref{sec:ftag:dl1:preprocessing} for more details). The scaling and shifting of the variables is described in Ref.~\cite{ATL-PHYS-PUB-2020-014}. Due to the long tails in the $\pt^{frac}$ and the $\Delta R$ distributions, their logarithms are used. The shifting and scaling is only applied to the following variables: $\log(\pt^{frac})$ (Fig.~\ref{fig:ftag:umami:track-vars}~(c)), $\log(\Delta R)$ (Fig.~\ref{fig:ftag:umami:track-vars}~(d)), nPixHits (Fig.~\ref{fig:ftag:umami:track-vars}~(i)) and nSCTHits (Fig.~\ref{fig:ftag:umami:track-vars}~(l)). All other variables have already a mean close to zero and are therefore not shifted and the differences in their values do not necessitate an additional scaling.\\
Each track in a jet is first processed through a network $\phi$ as indicated in Figure~\ref{fig:ftag:umamiNN}.
\begin{figure}[h]
    \centering
    \includegraphics[width=1.1\textwidth]{b-tagging/material_ftag_outlook/umami-structure.pdf}
    \caption{Network structure of the combined DL1 and \ac{dips} tagger \umami.}
    \label{fig:ftag:umamiNN}
\end{figure}
In the next step, all $n$ track networks, corresponding to the number of tracks in a jet, are summed up and further processed via the network $F$. This can be summarised into
\begin{equation}
    \Vec{p}_i=F\left(\sum_{i=1}^n\phi(\Vec{x}_i^t)\right),
\end{equation}
where $\Vec{x}_i^t$ are the track input features and $\Vec{p}_i$ is the vector of the $b$-, $c$- and light-flavour jet class probabilities corresponding to the \ac{dips} output nodes.
\begin{table}[h!]
  \begin{center}
    \label{tab:table1}
    \begin{tabular}{l | p{0.64\textwidth}c} % <-- Alignments: 1st column left, 2nd middle and 3rd right, with vertical lines in between
      \textbf{Input} & \textbf{Description}  & \textbf{Preprocessed} \\
    %   \hline
    %   \hline
    \toprule
%   $d_0$ & Transverse IP  & $\checkmark$ \\
% 	$z_0$ & Longitudinal IP & $\checkmark$ \\
  $s_{d_0}$ & $d_0 / \sigma_{d0}$: Transverse IP significance &  \\
	$s_{z0}$ & $z_0 \sin \theta / \sigma_{z0 \sin \theta}$: Longitudinal IP significance &  \\
	$\log \pt^{frac}$ & $\log \pt^{track} / \pt^{jet}$: Logarithm of fraction of the jet $\pt$ carried by the track & $\checkmark$ \\
	$\log \Delta R$ & Logarithm of opening angle between the track and the jet axis & $\checkmark$ \\
	IBL hits & Number of hits in the IBL: could be 0, 1, or 2 &  \\
	PIX1 hits & Number of hits in the next-to-innermost pixel layer: could be \mbox{0, 1, or 2} &  \\
	shared IBL hits & Number of shared hits in the IBL &  \\
	split IBL hits & Number of split hits in the IBL &  \\
	nPixHits & Combined number of  hits in the pixel layers & $\checkmark$ \\
	shared pixel hits & Number of shared hits in the pixel layers &  \\
	split pixel hits &  Number of split hits in the pixel layers &  \\
	nSCTHits  & Combined number of hits in the SCT layers & $\checkmark$ \\
	shared SCT hits & Number of shared hits in the SCT layers &  \\
    \end{tabular}
    \caption{Input features for the \ac{dips} algorithm~\cite{ATL-PHYS-PUB-2020-014}. The right column \textit{Preprocessed} indicates if the variables are shifted to mean zero and scaled to significance of one.}
    \label{table:dips:inputs}
  \end{center}
\end{table}



\begin{figure}[h]
  \centering
  \subfloat[]{\includegraphics[width=0.4\textwidth]{b-tagging/material_ftag_outlook/track_vars/IP3D_signed_d0_significance}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.4\textwidth]{b-tagging/material_ftag_outlook/track_vars/IP3D_signed_z0_significance}}\\
  \subfloat[]{\includegraphics[width=0.4\textwidth]{b-tagging/material_ftag_outlook/track_vars/ptfrac}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.4\textwidth]{b-tagging/material_ftag_outlook/track_vars/dr}}\\
  \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_ftag_outlook/track_vars/numberOfInnermostPixelLayerHits}}\hspace{0.2cm}
  \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_ftag_outlook/track_vars/numberOfInnermostPixelLayerSplitHits}}\hspace{0.2cm}
  \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_ftag_outlook/track_vars/numberOfInnermostPixelLayerSharedHits}}\\
  \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_ftag_outlook/track_vars/numberOfNextToInnermostPixelLayerHits}}\hspace{0.2cm}
  \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_ftag_outlook/track_vars/numberOfPixelHits}}\hspace{0.2cm}
  \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_ftag_outlook/track_vars/numberOfPixelSharedHits}}\\
  \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_ftag_outlook/track_vars/numberOfPixelSplitHits}}\hspace{0.2cm}
  \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_ftag_outlook/track_vars/numberOfSCTHits}}\hspace{0.2cm}
  \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_ftag_outlook/track_vars/numberOfSCTSharedHits}}\\
  \caption{Track variables used for the \ac{dips} training extracted from the downsampled hybrid sample: (a)\&(b) the impact parameters, (c) the $\log \pt^{frac}$, (d) 	$\log \Delta R$ and (e)-(m) the number of hits in the different \acs{id} layers.}
  \label{fig:ftag:umami:track-vars}
\end{figure}


% \begin{table}[h]
%     \centering
%     \renewcommand{\arraystretch}{0.6}
%     \begin{tabular}{ll}
%     \toprule
%          \textbf{Hyperparameter} & \textbf{Values}  \\
%          \toprule
%          $N_\text{hidden layers}$ &  $8$ \vspace{0.3cm} \\
%           $N_\text{nodes/layer}$  & $[256, 128, 60, 48, 36, 24, 12, 6]$    \vspace{0.3cm}\\
%          learning rate &  $0.01$\vspace{0.3cm} \\
%          Training batch size &  $45000$ \vspace{0.3cm} \\
%          Activation function &  ReLu \vspace{0.3cm} \\
%          \midrule
%          Free (trainable) parameters & $59,275$  \vspace{0.3cm} \\
%          Fixed parameters &  $1,140$ \vspace{0.3cm} \\
%          Training sample statistic &  $20$M\\
%         \bottomrule
%     \end{tabular}
%     \caption{\acs{nn} architectureof the \umami tagger.}
%     \label{tab:ftag:umami:architecture}
% \end{table}

\FloatBarrier
\subsection{Joint Architecture}
The \dlr tagger already used the information of the RNNIP in the training. 
However, the used RNNIP information was limited to the outputs of the standalone RNNIP algorithm which are the three flavour class probabilities.
% it was used only as input feature corresponding to the output nodes of the network, the three flavour class probabilities. 
A joint architecture allows to pass more information to the \textit{jet-network} which is the $\mathcal{U}$ \acs{nn} in the sketch~\ref{fig:ftag:umamiNN}. In this case, a layer with 30 nodes (units) is concatenated with the other jet features which are processed through one \acs{nn} layer with 72 nodes. Apart from this, a full back-propagation up to all the track \acsp{nn} $\phi$ is done in the training allowing a joint optimisation. The \acs{dips} part has also an intermediate loss which maintains the possibility to evaluate the \acs{dips} performance separately and also is an implicit help for the network to optimise this network branch. After the combination of the \acs{dips} part and the jet features a final feed-forward network $\mathcal{U}$ is employed inspired by the \acs{dl1} architecture with also three output nodes corresponding to the flavour probabilities.\\
Both networks, $F$ and $\mathcal{U}$ have their dedicated losses. While the loss of the $\mathcal{U}$ network $\loss(\mathcal{U})$ is sensitive to the track and the jet features, the loss of the $F$ network $\loss(F)$ is only sensitive to the tracks. The overall optimisation is performed on the combined loss
\begin{equation}
    \loss(\text{comb.}) = \loss(\mathcal{U}) + \lambda \cdot\loss(F) = \loss(\umami) + \lambda \cdot \loss(\text{DIPS}), \label{eq:ftag:outlook:loss}
\end{equation}
where the parameter $\lambda$ defines the importance of the two losses.\\


\subsection{\umami Training}

The training of the \umami tagger is only in a preliminary phase and no dedicated optimisation studies were performed so far. Due to technical reasons, only the standard version of \ac{dips} from~\cite{ATL-PHYS-PUB-2020-014} is studied and not the optimised version with a looser track selection.  The $\lambda$ parameter from Equation~\eqref{eq:ftag:outlook:loss} is set to $1$ and a training statistic of 6M jets is used. The corresponding loss and accuracy curves for the training and validation set are shown in Figure~\ref{fig:ftag:umami:monitor}. As expected, the loss $\loss(\mathcal{U})$ is better (lower) than the \ac{dips} loss $\loss(F)$ since $\loss(\mathcal{U})$ encodes the track and jet information. The training losses for \ac{dips} and \umami show a constant downwards trend also still after 160 epochs. In general, the \ac{dips} validation loss and accuracy are fluctuating more than for \umami. Around epoch 100 it seems that the \ac{dips} part is starting to overfit, the loss is increasing. Figure~\ref{fig:ftag:umami:monitorrej} shows the background rejection as a function of the training epochs for both sets of the network outputs (\ac{dips} and \umami). Here, the same effect is visible, while the \cjet rejection stays almost constant after epoch 100 (Fig.~\ref{fig:ftag:umami:monitorrej}~(a)), the light-flavour jet rejection decreases. This behaviour also propagates to the \umami performance where the light-flavour jet rejection starts decreasing as well around epoch 100 but seems then to stabilise again, correcting in some sense for this behaviour of the \ac{dips} network. A similar effect is already visible around epoch 45 for the light-flavour jet rejection but recovering again slightly around epoch 80.\\
\begin{figure}[t]
  \centering
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_ftag_outlook/loss-plot.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_ftag_outlook/accuracy-plot.pdf}}\\
  \caption{Epoch-dependent training and validation loss (a) and accuracy (b) for the \ac{dips} and \umami outputs.}
  \label{fig:ftag:umami:monitor}
\end{figure}
\begin{figure}[t]
  \centering
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_ftag_outlook/rej-plot_val_dips.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_ftag_outlook/rej-plot_val_umami.pdf}}\\
  \caption{Light-flavour jet rejection (red) and \cjet rejection (blue) as a function of the training epochs for the (a) \ac{dips} and (b) \umami output nodes of the tagger. The vertical dashed lines indicate the performance of the current recommendations RNNIP and \dlr.}
  \label{fig:ftag:umami:monitorrej}
\end{figure}
The \umami background rejection reveals a slightly different picture. The light-flavour jet rejection reaches a maximum around epoch 20 and then continuously decreases until it stabilises more around epoch 90 with the small decrease and recovery mentioned above. The \cjet rejection follows an opposite trend, after some larger fluctuations in the beginning, it starts to increase and reaches after certain fluctuations a still pretty noisy plateau after epoch 100.\\


To avoid the regions where overfitting occurs, an epoch around the maximum for the light-flavour jet rejection in \umami is used for further comparisons, corresponding to epoch 17. The background rejection versus \bjet efficiency plots are shown in Figure~\ref{fig:ftag:umami:dips} for the intermediate \ac{dips} output and for \umami in Figure~\ref{fig:ftag:umami:rocs}. \ac{dips} outperforms RNNIP in terms of the light-flavour jet rejection and is slightly worse for the \cjet rejection. This effect can be compensated by adapting the $f_c$ parameter. Thus the intermediate \ac{dips} output of the joint \umami training provides already promising results.\\
The final \umami performance (Fig.~\ref{fig:ftag:umami:rocs}) improves compared to \dlr in the light-flavour jet rejection over the full \bjet efficiency range and degrades for the \cjet rejection. Again, these differences can be compensated with the $f_c$ parameter.

\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_ftag_outlook/PFlow_c-rej_beff-dips.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_ftag_outlook/PFlow_u-rej_beff-dips.pdf}}\\
  \caption{\ac{dips} (orange) performance compared to RNNIP (blue) in terms of  (a) the \cjet rejection and (b) the light-flavour jet rejection as function of the \bjet efficiency.}
  \label{fig:ftag:umami:dips}
\end{figure}



\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=.48\textwidth]{b-tagging/material_ftag_outlook/PFlow_c-rej_beff-umami.pdf}}
  \subfloat[]{\includegraphics[width=.48\textwidth]{b-tagging/material_ftag_outlook/PFlow_u-rej_beff-umami.pdf}}
  \caption{\umami (orange) performance compared to \dlr (blue) in terms of  (a) the \cjet rejection and (b) the light-flavour jet rejection as function of the \bjet efficiency.}
  \label{fig:ftag:umami:rocs}
\end{figure}



\subsection{Possible Improvements and Outlook}
This first study creating a combined architecture of \ac{dips} and \acs{dl1} shows already promising results. Nevertheless, there are lots of possibilities to improve and understand the tagger better avoiding overfitting.\\
The next steps would be to make use of the optimisations described in Ref.~\cite{ATL-PHYS-PUB-2020-014} by loosening the track selection and including the impact parameters $d_0$ and $z_0\sin\theta$ in the training. Furthermore, extensive optimisation studies need to be done on the network itself: the loss function from Equation~\eqref{eq:ftag:outlook:loss} needs to be optimised, in particular the $\lambda$ parameter. The number of hidden layers and their number of nodes requires some tuning. In addition, attention techniques are worth to be tested. They allow to adapt the importance of the different tracks in the jet by assigning them a weight depending on a certain feature e.g. the impact parameter or \pt. Another possibility is to test different pooling operations besides the sum operation, e.g. the average or the maximum.\\

In conclusion, this new architecture is most probably the next step in flavour tagging in ATLAS and requires still lots of R\&D work which go beyond the scope of this thesis.\\


Especially at large transverse momenta, further improvements are necessary to achieve a better performance. In that regime methods based on the hit information in the \acl{id} are studied within ATLAS. A charged particle can be identified as hits in the \acs{id} and in case a $b$-hadron decays between two \acs{id} layers, an increase in hits is observed.
This is an ideal use case for so-called \textit{Graph Neural Networks}~\cite{zhou2018graph}. Not only in the tagger algorithm development itself, but also in the calibration, especially in the light-flavour jet calibration, \acs{nn} techniques would bring a gain. It would be for instance possible to employ \textit{Invertible Neural Networks}~\cite{ardizzone2019analyzing} to allow a better calibration with the flipped taggers.\\