\chapter{Deep Learning Based Heavy-Flavour Tagger} \label{chap:ftag:dl1}

The \acf{dl1} was introduced during RUN II as a second high-level heavy-flavour tagger, besides MV2. The tagger is designed to combine the information of the baseline taggers (introduced in Chapter~\ref{chap:ftag_intro}) into a final discriminant. Studies to include more basic detector-level variables rather than by human brain designed observables are also ongoing, shown in the outlook in Chapter~\ref{chap:ftag:outlook}, including track and hit based information directly in the \ac{dl1} training.\\
The performance of the first version of \ac{dl1}~\cite{ATLAS-CONF-2017-013} optimised for \emtopo is shown in Figure~\ref{fig:ftag:mv2}. The overall performance of \ac{dl1} is slightly better than MV2. The main advantage of \ac{dl1} is its multi-class output which means that the network predicts for every jet the probabilities for being compatible with the three main flavour classes: \bjet{}s, \cjet{}s and light-flavour jets. In general, this can be also realised with a \acs{bdt}, however, \acp{nn} are more flexible and have more possibilities to customise their structure.\\

In this thesis, the \ac{dl1} algorithm is being re-optimised and adapted for \pflow and \vrtrack introducing a new machine-learning workflow for the flavour tagging group in ATLAS. The general design will be introduced in Section~\ref{sec:ftag:dl1:design} followed by the description of the preprocessing in Section~\ref{sec:ftag:dl1:preprocessing}. The dedicated optimisation for \pflow is shown in Chapter~\ref{chap:ftag:dl1:pflow} and for \vrtrack in Chapter~\ref{chap:ftag:dl1:vr}.


\section{General \ac{dl1} Design}\label{sec:ftag:dl1:design}
The underlying \ac{nn} structure of the \ac{dl1} tagger is a deep feed-forward neural network with three output nodes corresponding to the $b$-, $c$- and light-flavour jet probabilities illustrated in Figure~\ref{fig:DL1-struct}. The \acs{relu} activation function is used for each hidden layer and the last (output) layer makes use of the softmax activation function, such that the resulting network scores can be interpreted as probabilities.
\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{material/NN_structure_DL1.pdf}
    \caption{Neural Network structure of the \ac{dl1} tagger.}
    \label{fig:DL1-struct}
\end{figure}
The final output score is calculated from the multi-class output as described in Equation~\eqref{eq:performance-multiclass} and results for the \btag discriminant into the log-likelihood
\begin{equation}
\mathcal{D}_\text{b}(f_c) = \log \left(  \frac{p_b}{f_c\cdot p_c+(1-f_c)\cdot p_\text{l}} \right),
\label{eq:dl1_b}
\end{equation}
with $p_b$, $p_c$ and $p_l$ being the probabilities for the jet to be a \bjet{}, \cjet{} or light-flavour jet, respectively.
 The \cjet{} fraction \(f_c\) allows to tune how much emphasis is given to the $c$-jet or to the light-flavour performance (rejection). While the \cjet rejection increases as a function of $f_c$, the light-flavour jet rejection decreases. This parameter has to be tuned separately for each tagger and depends on the needs of the physics analyses. The advantage compared to MV2 is that this tuning is possible after the training and the $c$-jet fraction in the training sample does not have to be adapted. Another advantage of the multi-class output is that one can by changing the log-likelihood to 
 \begin{equation}
\mathcal{D}_\text{c}(f_b) = \log \left(  \frac{p_c}{f_b\cdot p_b+(1-f_b)\cdot p_l} \right),
\label{eq:dl1_c}
\end{equation}
perform $c$-tagging without the need of retraining the tagger. Here $f_b$ is now the \bjet fraction. These possibilities have far-reaching positive effects on the workflow within ATLAS. First of all, less person power is necessary since only one tagger has to be trained and maintained. Also, fewer variables have to be calculated and stored in the files used for the physics analyses saving computing and storage resources.\\
\ac{dl1} is a family of three different taggers illustrated in Figure~\ref{fig:ftag:dl1structure}: \dlb, \dlr and \dlrmu. They differ in their input variables used for the \ac{nn} training. The \dlb uses the same variables as MV2 with the additional  \acl{jf} variables optimised for \cjet identification. All the variables are summarised in Table~\ref{tab:ftag:outputs:ipxd} for IPxD, in tables \ref{tab:ftag:outputs:JF} and \ref{tab:ftag:outputs:JFc} for the two sets of \acl{jf} variables and the \acs{sv1} variables are listed in Table~\ref{tab:ftag:outputs:sv1}. In addition to the baseline tagger information, also the kinematic variables \pt and $|\eta|$ are passed to the training to explore correlations between the kinematics and the baseline tagger variables. The kinematics are, however, treated differently since it is not intended to classify jets based on differences in the kinematic distributions between the flavours (more details in sec \ref{sec:ftag:dl1:preprocessing}).
The \dlr configuration includes in addition the flavour probabilities provided by the RNNIP algorithm as outlined in Table~\ref{tab:ftag:outputs:rnnip}. The last tagger version \dlrmu exploits also the soft-muon information from Table~\ref{tab:ftag:outputs:smtnn} besides all variables also used in \dlr.\\
The umbrella term \textit{\ac{dl1}} is used for the tagger family but typically also the baseline version is called \textit{DL1}. To avoid ambiguities in the text, the baseline version will be denoted as \textit{baseline DL1} while in plots and schematics the expression \textit{DL1} will be kept to be consistent with the official ATLAS naming.\\ 
Adam, which is a gradient descent optimiser (see sec.~\ref{sec:ml:nns}), is utilised as optimiser together with a learning rate scheduler which reduces the learning rate after a certain amount of epochs if the loss did not change.
All other hyperparameters differ slightly between the various trainings and are explained in the dedicated sections.
\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/DL1-tagger-structure.pdf}
    \caption{Structure of the different types of DL1 taggers, depending on their variables used in the training.}
    \label{fig:ftag:dl1structure}
\end{figure}


\subsection{Software Chain}
The software chains for training \& development and for the application in the ATLAS software \textsc{Athena}~\cite{atlas_collaboration_2019_2641997} are completely disentangled from each other. The training software is based on industry-standard open-source software using python3.6~\cite{10.5555/1593511}. For data handling the numpy~\cite{harris2020array} and pandas~\cite{reback2020pandas} packages are used together with the file format 
hdf5~\cite{hdf5}. In general, a heavy use of human readable file formats as \textsc{json}~\cite{pezoa2016foundations} and yaml is made to make the code structure well configurable for users. For the training itself TensorFlow~\cite{tensorflow2015-whitepaper} is employed with the keras2~\cite{chollet2015keras} frontend.
The visualisation package matplotlib~\cite{Hunter:2007} as well as the tools from scikitlearn~\cite{scikit-learn} are included in the training process.
% uproot \cite{jim_pivarski_2019_3504190}
The full workflow is based on Docker~\cite{merkel2014docker} images which allow to run the software on any computing resource without worrying about the package installation and their versions.\\
The \texttt{C++} based LightWeight Tagger Neural Network (lwtnn) package~\cite{daniel_hay_guest_2019_3249317} integrates the \ac{nn} based taggers in in the ATLAS software \textsc{Athena}.
% ressources: institute GPUs, GRID GPUs




\section{Preprocessing and Input Variable Treatment}\label{sec:ftag:dl1:preprocessing}
To guarantee a robust training, it is necessary to perform several preprocessing steps. The first important step is the choice and preparation of the training sample. For the training of the \ac{dl1} taggers a mixture of two samples is taken, as described in Section~\ref{sec:ftag:modelling}, denoted as \textit{hybrid sample}~\cite{ATLAS-CONF-2017-013}. Figure~\ref{fig:ftag:preprocess:ptall} shows their jet \pt distributions for each jet flavour.
The \ttbar sample (solid lines) has a rapid fall in \pt compared to the $Z'$ sample (dashed lines) which has a flat \pt spectrum up to roughly \unit[4.5]{TeV} and a total range up to \unit[6]{TeV}\footnote{This $Z'$ sample is also called \textit{extended} $Z'$, there is also another sample version which only has a \pt range up to roughly \unit[3-4]{TeV} (which will be important for the \vrtrack training), also denoted as \textit{standard} $Z'$.}. The use of the $Z'$ sample allows a more robust training at high \pt.
\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/preprocessing/pt_btagJes-full_spectrum.pdf}
    \caption{\pt distribution of the \ttbar sample (solid lines) and $Z'$ sample (dashed lines). The \ttbar \bjet distribution is normalised to unity and all other distributions are normalised to the \ttbar \bjet distribution.}
    \label{fig:ftag:preprocess:ptall}
\end{figure}
From these two samples a so-called \textit{hybrid sample} is created. In the following only \pflow are considered, the differences for \vrtrack are discussed in the dedicated Chapter~\ref{chap:ftag:dl1:vr}.  Since the \textit{extended} $Z'$ sample is only available with the pile-up profile of the 2017 data taking period, only this \acs{mc} production is used for both \ttbar and $Z'$. The 2017 pile-up profile is a good representation of the overall RUN~II pile-up profile as shown in Figure~\ref{fig:mu-lumi}. The jets from the \ttbar sample are chosen to populate the lower \pt-range up to a specific value $\xi$ ensuring sufficient statistics and the extended $Z'$ sample is populating only the higher \pt-regions larger than $\xi$.
% The \pt splitting is chosen such that the \ttbar \pt spectrum covers the lower \pt-range with sufficient statistics and the extended $Z'$ the higher \pt-regions. 
This results in the following selection:

\begin{equation*}
t\bar{t} \text{ selection}:
  \begin{cases}
    b\text{-jets} & b\text{-hadron } \pt < \unit[250]{GeV}\\
    c\text{-jets} & \text{jet } \pt < \unit[250]{GeV}\\
    \text{light-flavour jets} & \text{jet } \pt < \unit[250]{GeV},
  \end{cases}\label{eq:ftag:cuts}
\end{equation*}
and the $Z'$ selections are chosen orthogonal. As indicated, the \bjet{} selection is using the $b$-hadron \pt. Together with the choice of the fraction of \ttbar to be $70\%$ in the hybrid sample, a smooth transition between the two samples is realised. The \pt distributions including the hybrid selections are shown in Figure~\ref{fig:ftag:pt:1stpreprocess}~(a) separately for \ttbar and $Z'$. In Figure~\ref{fig:ftag:pt:1stpreprocess}~(b), the \pt distributions from both samples are merged. As intended, the \bjet{} distribution shows a smooth transition while the \cjet{} distribution has a kink and the light-flavour distribution a spike.
\begin{figure}[htbp]
  \centering
  %\includegraphics[trim=0 620 0 0,clip,width=\linewidth]{inputs_IPxformed}
  \subfloat[]{\includegraphics[width=0.45\textwidth]{b-tagging/material_dl1_general/preprocessing/pt_btagJes-cut_spectrum}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.45\textwidth]{b-tagging/material_dl1_general/preprocessing/pt_btagJes-cut_spectrum_merged}}\\
  \caption{\pt distribution of the \ttbar and $Z'$ sample with the hybrid selection applied with a \ttbar fraction of $70\%$. In plot (a) the samples separated normalised to the  \ttbar \bjet distribution and (b) the samples merged into one distribution per flavour normalised to the \bjet distribution.}
  \label{fig:ftag:pt:1stpreprocess}
\end{figure}
In the previous iteration of the tagger, the \pt and $|\eta|$ distributions\footnote{Due to the symmetric ATLAS detector and the symmetry of physics processes in $\pm\eta$, the absolute value is used.} were weighted to match the \bjet{} \pt distribution. The weighting solves three issues: mitigate the discontinuity between samples, solving the problem of having imbalanced classes and reducing the influence to classify w.r.t the jet kinematics. Even though the correlations of the kinematics with the baseline tagger information is important, the goal is to avoid to train on differences in the kinematic distributions to distinguish between the flavours since the tagging is targeting an independent jet-by-jet classification. The classes are imbalanced, i.e. different fractions of $b$-, $c$- and light-flavour jets are present in the sample. Figure~\ref{fig:ftag:preprocess:piechart} shows the flavour fractions of the hybrid sample. The light-flavour jets are dominating, followed by \bjet{}s and the \cjet{}s constitute the smallest fraction.
\begin{figure}[htbp]
  \centering
%   \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_intro/preprocessing/tt_composition-piechart}}\hspace{0.3cm}
%   \subfloat[]{\includegraphics[width=0.3\textwidth]{b-tagging/material_intro/preprocessing/Z_composition-piechart}}\hspace{0.3cm}
%   \subfloat[]{\includegraphics[width=0.6\textwidth]{b-tagging/material_intro/preprocessing/comb_composition-piechart}}\\
  \includegraphics[width=.5\textwidth]{b-tagging/material_dl1_general/preprocessing/comb_composition-piechart}
  \caption{Flavour composition of the hybrid sample.}
  \label{fig:ftag:preprocess:piechart}
\end{figure}
% To balance the flavour-categories and ensuring the training to be independent of $p_\text{T}$ and $|\eta|$, the jets are re-sampled as a function of $p_\text{T}$ and $|\eta|$
By reweighting in \pt and $|\eta|$, the weighted sum is $\nicefrac{1}{3}$ for each flavour across the full \pt and $|\eta|$ spectra. However, it turned out that the weighting approach introduced some instabilities in the learning process of the network. Therefore, the resampling method is employed, as illustrated in Figure~\ref{fig:ftag:resampling}.
\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/resampling}
    \caption{Illustration of the resampling method to cope for imbalanced classes. The undersampling approach is shown on the left and the oversampling approach is shown on the right~\cite{undersamp}.}
    \label{fig:ftag:resampling}
\end{figure}
Instead of weighting the distributions, single jets are either removed from the majority classes (\textit{undersampling}) or jets from the minority classes are duplicated (\textit{oversampling}) to match a given distribution. This method is also an indirect weighting but to the \acs{nn} jets with event weight equals one are passed. Luckily, sufficient \acs{mc} statistics is available, so it is possible to only make use of the \textit{undersampling} method by removing jets from the majority classes. The \bjet{} \pt and $|\eta|$ distributions are taken as reference and the $c$- and light flavour jets are undersampled. From the full sample statistic, a subset from each flavour class is extracted such that in each $\pt$--$|\eta|$ bin the same amount of jets per flavour is available. The binning used for the undersampling procedure is optimised to be more granular in the lower \pt region, especially in the sample transition region, and to be wider for higher \pt. In total 434 \pt bins and 10 $|\eta|$ bins are utilised for the resampling.  The resulting \pt spectrum of the training hybrid sample is depicted in Figure~\ref{fig:ftag:preprocess:ptdownsamp}.
\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/preprocessing/pt_btagJes-downsampled}
    \caption{\pt distribution of the undersampled hybrid sample.}
    \label{fig:ftag:preprocess:ptdownsamp}
\end{figure}
The small differences originate from the usage of an equidistant binning in the plot. The hybrid $|\eta|$ distribution is shown before resampling  in Figure~\ref{fig:ftag:preprocess:eta}~(a) and after resampling in Figure~\ref{fig:ftag:preprocess:eta}~(b).
\begin{figure}[htbp]
  \centering
  %\includegraphics[trim=0 620 0 0,clip,width=\linewidth]{inputs_IPxformed}
  \subfloat[]{\includegraphics[width=0.45\textwidth]{b-tagging/material_dl1_general/preprocessing/eta-merged}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.45\textwidth]{b-tagging/material_dl1_general/preprocessing/eta-downsampled}}\\
  \caption{The $|\eta|$ distributions of the hybrid sample (a) \ttbar and $Z'$ merged without any further processing and (b) resampled to match the \bjet{} spectrum. The \bjet distributions are normalised to unity and the \cjet and light-flavour jet distributions to the normalised \bjet distribution.}
  \label{fig:ftag:preprocess:eta}
\end{figure}

The baseline taggers are not always succeeding, e.g. if there is no \acl{sv} in the jet, the \acs{sv1} or \acl{jf} algorithm cannot extract any information and a default value is returned. The fraction of default values for the IPxD, \acs{sv1} and \acl{jf} are illustrated for the hybrid sample in Figure~\ref{fig:ftag:preprocess:defaults}. While the IPxD algorithms have almost no default values, the \acs{sv1} and \acl{jf} have significantly more cases where they cannot reconstruct a displaced vertex. Due to the design of \acl{jf} also single track vertices can be reconstructed and it therefore finds more often displaced vertices than \acs{sv1}. 
\begin{figure}[h]
    \centering
    \includegraphics[width=.6\textwidth]{b-tagging/material_dl1_general/preprocessing/default_chart}
    \caption{Bar chart indicating the amount of default values per baseline tagger and flavour.}
    \label{fig:ftag:preprocess:defaults}
\end{figure}
It is also nicely visible that in both cases the \bjet{}s have the smallest fraction of default values since a secondary vertex is expected to be present in the jet. The next category is \cjet{}s, which usually also have a secondary vertex although it is more difficult to reconstruct them. Light-flavour jets have the most default values since displaced vertices rarely appear.\\
\acsp{bdt} can easily deal with default values which are set to a value way off the spectrum which is normally done within ATLAS. However, the learning process of \aclp{nn} suffers from this kind of default value treatment. Therefore, the default values are replaced by the mean of the inclusive distribution. It is also possible to choose a different approach but the mean value is the simplest solution. In some cases, the default values are also set to the edge value of the distribution for physics reasons. The energy fractions ($f_\text{E}^\text{SV1}$, $f_\text{E}^{\text{JF}}$, $f_\text{E}^{\text{JF}_c}$) are set to zero if it is not possible to reconstruct a displaced vertex since in this case no energy is carried by a \acs{sv}. 
In addition, the default values of the variables $S_\text{xyz}^\text{JF}$, $N_\text{vertices}^\text{JF}$, $N^\text{JF}_\text{1-trk vertices}$, $N^{\text{JF}}_\text{trks}$, $N^\text{JF}_{\geq2\text{-trk vertices}}$, $N^{\text{JF$_c$}}_\text{trks}$, $N_\text{2TrkVtx}^\text{SV1}$ and $N_\text{TrkAtVtx}^\text{SV1}$ are set to the lower edge of the distributions. Two example distributions are shown with the default value set to the mean for $m^{\text{JF}}_\text{inv}$ ($\langle m^{\text{JF}}_\text{inv}\rangle=\unit[2.6]{GeV}$) in Figure~\ref{fig:ftag:preprocess:defaultreplaced}~(a) and to the lower edge value for $f_\text{E}^{\text{JF}}$ ($f_\text{E}^{\text{JF}}=0$) in Figure~\ref{fig:ftag:preprocess:defaultreplaced}~(b).
\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/preprocessing/downsampled_JetFitter_mass}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/preprocessing/downsampled_SV1_efracsvx}}\\
  \caption{The variable distribution of (a) the \acl{jf} mass $m^{\text{JF}}_\text{inv}$ with its default value set to its mean $\langle m^{\text{JF}}_\text{inv}\rangle=\unit[2.6]{GeV}$ and (b) the \acs{sv1} energy fraction $f_\text{E}^{\text{JF}}$ with its default value 0. All distributions normalised to unity.}
  \label{fig:ftag:preprocess:defaultreplaced}
\end{figure}
The default values are combined with a binary check variable for each baseline algorithm indicating if the algorithm returned default values. This allows the network to distinguish between the cases where the default value is a true default value or is actually coming from the underlying physics process. The binary indicator variables are listed in Table~\ref{tab:ftag:outputs:check}.\\
% \improvement{ add explanation jf ipxd double added}\\
\begin{table}[h!]
    \centering
    \renewcommand{\arraystretch}{0.6}
    \begin{tabular}{lp{0.83\textwidth}l}
    \toprule
         \textbf{Variable} & \textbf{Description}  \\
         \toprule
         &  Binary check variables for the baseline tagger:  \vspace{0.1cm} \\
         \acs{sv1}$_\text{isDefault}$ & \acs{sv1}  \vspace{0.1cm} \\
         IP2D$_\text{isDefault}$ & IP2D  \vspace{0.1cm} \\
         IP3D$_\text{isDefault}$ & IP3D  \vspace{0.1cm} \\
         \acs{jf}$_\text{isDefault}$ & \acl{jf}  \vspace{0.1cm} \\
         \acs{jf}$^c_\text{isDefault}$ & $c$-identification optimised \acl{jf}  \vspace{0.1cm} \\
         SMT$_\text{isDefault}$ & \acl{smt}\\
        \bottomrule
    \end{tabular}
    \caption{Binary check variables indicating if a baseline algorithm returned default values.}
    \label{tab:ftag:outputs:check}
\end{table}
The last preprocessing step is to balance the ranges of the input variables such that they are all in the same order of magnitude to improve the learning process of the \acs{nn}. Therefore, all variables are shifted to a mean of zero and a standard deviation of one, with the exception of the binary check variables. The two kinematic variables, the \acl{jf} mass $m^{\text{JF}}_\text{inv}$ and the \acs{sv1} energy fraction $f_\text{E}^{\text{JF}}$ are shown in Figure~\ref{fig:ftag:preprocess:scaled} with the full preprocessing applied.
\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/preprocessing/scaled_pt_btagJes.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/preprocessing/scaled_absEta_btagJes.pdf}}\\
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/preprocessing/scaled_JetFitter_mass.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.48\textwidth]{b-tagging/material_dl1_general/preprocessing/scaled_SV1_efracsvx.pdf}}\\
  \caption{Fully preprocessed distributions (resampled, scaled and shifted) of (a) the jet \pt (b) $|\eta|$ (c) $m^{\text{JF}}_\text{inv}$ and (d) $f_\text{E}^{\text{JF}}$. All distributions normalised to unity.}
  \label{fig:ftag:preprocess:scaled}
\end{figure}
All remaining variables which are summarised in Figure~\ref{fig:ftag:preprocess:summary} are also fully preprocessed. 
\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{b-tagging/material_dl1_general/preprocessing/summary-scaled}
    \caption{All remaining variables used for the \acs{dl1} tagger family after fully preprocessing (resampled, scaled and shifted). All distributions normalised to unity.}
    \label{fig:ftag:preprocess:summary}
\end{figure}
Furthermore, outliers coming from jets in extreme phase spaces are removed from certain distributions, which are far off the spectrum and would only disturb the training process.
% JetFitter_energyFraction: 0
%   JetFitterSecondaryVertex_energyFraction: 0
%   SV1_efracsvx: 0

%   JetFitter_significance3d: 0 
%   JetFitter_nVTX: -1  
%   JetFitter_nSingleTracks: -1  
%   JetFitter_nTracksAtVtx: -1 
%   JetFitter_N2Tpair: -1 
%   SV1_N2Tpair: -1
%   SV1_NGTinSvx: -1

%   JetFitterSecondaryVertex_nTracks: 0 

% \FloatBarrier

% \section{Performance Evaluation and Training Monitoring}\label{sec:ftag:dl1:performance-dl1}

% typically: loss, accuracy, ROC
% -> interested in performance of specific \acp{wp} 
% rejection better measure
% loss used to monitor training








% Znunu/Zmumu -> auch other backgrounds in ttH
% The production of $V+$jets is simulated with the
% \sherpa~v2.2.1~\cite{Bothmann:2019yzt}
% generator using next-to-leading order (NLO) matrix elements (ME) for up to two partons, and leading order (LO) matrix elements
% for up to four partons calculated with the Comix~\cite{Gleisberg:2008fv}
% and \openloops~\cite{Buccioni:2019sur,Cascioli:2011va,Denner:2016kdg} libraries. They
% are matched with the \sherpa parton shower~\cite{Schumann:2007mg} using the MEPS@NLO
% prescription~\cite{Hoeche:2011fd,Hoeche:2012yf,Catani:2001cc,Hoeche:2009rj}
% using the set of tuned parameters developed by the \sherpa authors.
% The \nnpdfnnlo set of PDFs~\cite{Ball:2014uwa} is used and the samples
% are normalised to a next-to-next-to-leading order (NNLO)
% prediction~\cite{Anastasiou:2003ds}.