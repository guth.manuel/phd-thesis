\pdfbookmark[1]{The ATLAS Experiment at the Large Hadron Collider}{atlas-experiment}
\chapter{The ATLAS Experiment at the Large Hadron Collider}\label{chap:atlas}

% The smaller the examined physics scale, the larger experiments are required. 
Particle physics explores scales in the order of \unit[$<10^{-15}$]{m} which requires large and complex machines to explore physics at the TeV scale.  Such a machine is the \ac{lhc}~\cite{Evans_2008,BRUNING2012705,Bruening:782076}. Experiments in these dimensions are only possible within international collaborations. Thus, the \ac{cern} is an optimal environment to host such experiments.\\
The work in this thesis is based on the $\sqrt{s}=\unit[13]{TeV}$ proton-proton collision data collected with the ATLAS experiment at the \acs{lhc}.\\
This chapter gives a short overview of the \acs{lhc} and describes the different sub-detector systems of the ATLAS detector including its magnetic and trigger system.
% precision measurements and searches for new physics in the field of high energy physics need large experiments.

\section{The Large Hadron Collider}
The largest and most powerful hadron accelerator ever built, the \ac{lhc}, is situated near Geneva, Switzerland, with a circumference of \unit[27]{km}. It consists of 1232 super-conducting dipole magnets designed to reach a \com energy of \cme[14] and currently, during RUN II, operating at a \com energy of \cme (\cme[7/8] in RUN I).\\
Apart from protons, the \ac{lhc} can also operate with heavy ions. This dissertation is, however, entirely based on proton-proton collisions.\\
\begin{figure}[h]
\centering
\includegraphics[width = 0.65\textwidth]{background/material_ATLAS/Accel_CERN}
\caption{Current CERN accelerator complex with Large Hadron Collider (LHC), Super Proton Synchrotron (SPS), Proton Synchrotron (PS), Booster, Antiproton Decelerator (AD), Low Energy Ion Ring (LEIR), Linear Accelerators (LINAC 3 \& 4), CLIC Test Facility (CTF3), CERN to Gran Sasso (CNGS), Isotopes Separation on Line (ISOLDE), neutrons Time of Flight (n-ToF) and High-Radiation to Materials High-Radiation to Materials (HiRadMat) Facility (HiRadMat)~\cite{PhysRevSTAB.16.054801}. \label{fig:accel}}
\end{figure}

As shown in Figure~\ref{fig:accel}, the \ac{lhc} is part of a large accelerator complex being the final element in this accelerator chain. The pre-acceleration of protons is performed in several steps beginning with a linear accelerator (LINAC 2 in RUN II and from RUN III on LINAC 4) where the protons are injected as hydrogen gas and are accelerated to \unit[50]{MeV} followed by the Booster where they reach \unit[1.4]{GeV}. The next step is the Proton Synchrotron (PS) which accelerates the protons to \unit[25]{GeV} and the final pre-acceleration is done in the Super Proton Synchrotron (SPS), which injects the protons into the \ac{lhc} with an energy of \unit[450]{GeV}.\\
The \ac{lhc} is using eight radiofrequency cavities per beam operating at \unit[400]{MHz} to accelerate the protons which are brought to collision at four different points, each hosting an experiment. Two of them are the multi-purpose experiments ATLAS~\cite{Collaboration_2008_ATLAS} and CMS~\cite{Collaboration_2008_CMS} pursuing a wide range of physics, comprising \ac{sm} precision measurements as well as searches for beyond the \ac{sm} phenomena such as Supersymmetry, Exotic particles or Dark Matter searches. These two collaborations are the largest ones at CERN comprising around 3000 scientist each~\cite{cms-members,atlas-members}. The LHCb experiment~\cite{Collaboration_2008_LHCb} is specialised in exploring hadrons containing $b$- or $c$-quarks especially investigating CP-violating processes. The ALICE experiment~\cite{Collaboration_2008_ALICE} is the only experiment fully focusing on heavy-ion collisions\footnote{ALICE uses proton-proton collisions only for calibration purposes.} and therefore particularly specialised on \acs{qcd} physics.\\



% \subsection*{Luminosity and Pile-up}
Apart from the \come, the instantaneous luminosity is a main characteristic of a particle collider. For a circular collider with a Gaussian-shaped effective beam area $A=4\pi\sigma_x\sigma_y$, where $\sigma_{x,y}$ are the Gaussian beamwidths in the $x$- and $y$-direction\footnote{The coordinate system is defined in Section~\ref{sec:atlas}.}, the instantaneous luminosity can be written as
\begin{equation}
     \mathcal{L} =f_\text{rev} \cdot \frac{N_1 \cdot N_2}{4\pi\sigma_x\sigma_y} F(\theta_c),
 \end{equation}
 with $f_\text{rev}$ the revolution frequency, $N_{1,2}$ the total number of protons in each beam and $F(\theta_c)$ scoping for geometric effects caused by the crossing angle $\theta_c$ of the two beams since the beams of the \ac{lhc} are not colliding exactly head-on. The revolution frequency of the \ac{lhc} is $f_\text{rev}=\nicefrac{c}{\unit[27]{km}}=\unit[11]{kHz}$ with nominally 2808 proton bunches. %a nominal spacing between proton bunches of \unit[25]{ns}.
 The protons are organised in bunches which can contain up to $10^{11}$ protons. This leads to an instantaneous luminosity of $\mathcal{O}(\unit[10^{34}]{cm^{-2}s^{-1}})$. Consequently, the produced events by the \ac{lhc}, which are the number of collisions, can be retrieved by integrating the instantaneous luminosity $\mathcal{L}$
\begin{equation}
     N=\sigma\cdot\int\mathcal{L}~\text{d}t=\sigma\cdot \mathscr{L},
\end{equation}
where $\sigma$ is the event cross-section for a given physics process. The evolution of the integrated luminosity $\mathscr{L}$ of the \ac{lhc} \mbox{RUN II} delivered to the ATLAS experiment is shown in Figure~\ref{fig:mu-lumi}~(a) yielding in total an integrated luminosity of $\mathscr{L}=\unit[139]{fb^{-1}}$ good for physics which means that this data can be used in analyses. In fact, this data will be used in this thesis.\\
\begin{figure}[h]
    \centering
    \begin{minipage}[t]{0.49\textwidth}
    \includegraphics[width=1.\textwidth]{background/material_ATLAS/intlumivstimeRun2DQall}
    \caption*{(a)}
    \end{minipage}
    \begin{minipage}[t]{0.49\textwidth}
    \includegraphics[width=1.\textwidth]{background/material_ATLAS/mu_2015_2018.pdf}
    \caption*{(b)}
    \end{minipage}
    \caption{The development of the cumulative luminosity collected by the ATLAS experiment during RUN~II~(a) and the mean number of interactions per bunch crossing splitted into the different data taking periods~(b)~\cite{atlaslumi}. \label{fig:mu-lumi}}
\end{figure}

Due to the large number of protons within a bunch, more than one collision of interest can occur within a bunch crossing,
% as well as between the same two protons several partons can interact (multi parton interaction)
 which is called in-time pile-up. In addition, there are interactions coming from neighbouring bunch crossings which cannot be resolved fast enough by the detector. These are called out-of-time pile-up. The mean interactions per crossing is a measure to quantify the pile-up. Clearly, the suppression of pile-up effects is quite a challenge for physics analyses.
The distribution of the mean interactions per crossing is shown in Figure~\ref{fig:mu-lumi}~(b). The pile-up profiles differ for each data taking period since the instantaneous luminosity (corresponding to the slope in Fig.~\ref{fig:mu-lumi}~(a)) constantly increased, reaching a plateau in 2017/2018, and therefore more interactions per bunch crossing occur indicated in the plot legend as the average number of interactions per crossing $\langle\mu \rangle$.


\section{The ATLAS Detector}\label{sec:atlas}
The ATLAS (\textbf{A} \textbf{T}oroidal \textbf{L}HC \textbf{A}pparatu\textbf{S}) detector~\cite{Collaboration_2008_ATLAS} is a multi-purpose particle detector, used to study a wide range of physics topics. It is situated $\unit[100]{m}$ below ground at Point-1 of the \ac{lhc}. With its large dimensions of $\unit[25]{m}$ in diameter, a length of $\unit[44]{m}$ and a weight of $\unit[7000]{t}$, it is the largest detector located at a collider. The detector has a cylindrical structure composed of several detector layers with an almost full solid angle coverage of $4\pi$ schematically illustrated in Figure~\ref{fig:ATdet}. 

All detector systems are designed such that they provide optimal performance for the different physics analyses. Hence it is important that the detector satisfies the following criteria: fast electronics for the readout, high granularity, good object reconstruction efficiency and resolution.\\
% The innermost layer enclosing the beam pipe is the \ac{id} recording tracks of charged particles with a high spacial granularity.


\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{background/material_ATLAS/Atlas-det}
	\caption{Schematic overview of the ATLAS detector~\cite{Pequenao:1095924}. \label{fig:ATdet}}
\end{figure}


\subsection*{Coordinate System}

\begin{figure}[b]
	\centering
	\includegraphics[width=0.7\textwidth]{background/material_ATLAS/coordinate-system_ATLAS}
	\caption{Coordinate system of the ATLAS detector. \label{fig:coord-atlas}}
\end{figure}

In order to describe the particles recorded with the ATLAS detector, a right-handed coordinate system is used as illustrated in Figure~\ref{fig:coord-atlas} with its origin at the centre of the detector which is also the nominal interaction point. The $z$-axis is defined along the beamline while the $y$-axis points towards the surface and the $x$-axis in the direction of the centre of the \acs{lhc}. 
To describe the physics objects within the detector, spherical coordinates are the best choice where the polar angle $\theta$ is the angle between the $z$-axis and the direction considered while the azimuthal angle $\phi$ is measured in the $x$-$y$ plane with respect to the $x$-axis. In fact, the polar angle is usually stated as the pseudorapidity $\eta$ which is a high-energy approximation of the rapidity $y$:
\begin{equation}
    y= \frac{1}{2}\ln\left( \frac{E+p_z}{E-p_z} \right) \qquad \xrightarrow{m\ll E} \qquad -\ln\left(\tan\frac{\theta}{2}\right)=\eta, \label{eq:theory:rapidity}
\end{equation}
with $E$ the energy and $p_z$ the $z$ component of the momentum vector. The advantage of the pseudorapidity is that the difference $\Delta\eta$ is invariant under Lorentz transformation. Thus the distances of two objects are calculated via
\begin{equation}
\Delta R=\sqrt{(\Delta\phi)^2+(\Delta\eta)^2}.
\end{equation}
In some cases, the distance is also calculated using the rapidity instead of the pseudorapidity which will be denoted as $\Delta R_y$.
Furthermore, the $x$-$y$-plane defines the transverse plane where the transverse momentum is an important quantity denoted as
\begin{equation}
    \vec{p}_\text{T}=\left(\begin{array}{c} p_x \\ p_y \end{array}\right) \qquad \pt=\sqrt{(p_x)^2+(p_y)^2}.
\end{equation}
Since protons are composite particles and the \com system moves with respect to the lab system, only the transverse momentum component of the initial partons is known to be zero in the lab system at the time of the collision.

\subsection{Inner Detector}\label{sec:ATLAS-ID}
The innermost detector system is the \acf{id}~\cite{CERN-LHCC-97-016,Haywood:331064} enclosing the beam pipe shown in Figure~\ref{fig:innerdet}. This detector system provides precise tracking information of charged particles.
% with a combined track \pt resolution of around $\nicefrac{\sigma_{\pt}}{\pt}=0.05\%\cdot\pt\oplus0.5\%$ \improvement{to be validated!}. 
It is structured into three sub-detectors: the pixel detector, the \ac{sct} and the \ac{trt}. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{background/material_ATLAS/inner_tracker}
	\caption{Overview of the inner detector of the ATLAS experiment divided into three sub-detectors (Pixels, \ac{sct} and \ac{trt})~\cite{Potamianos:2016ptf}. \label{fig:innerdet}}
\end{figure}

\subsubsection*{Pixel Detector}
The first part of the \ac{id} is the silicon pixel detector comprising 4 cylindrical layers and 2 end-caps with 3 disc layers each. The layers are located between \unit[33.25]{mm} to \unit[122.5]{mm} around the beam pipe with a coverage of $|\eta|<2.5$. The pixel detector is especially important for the track reconstruction, the primary vertex reconstruction as well as for secondary vertex finding.\\
The \ac{ibl}~\cite{Capeans:1291633} is the innermost layer, installed in-between RUN I and RUN II, having the highest granularity with a pixel size of \unit[50]{$\mu$m} in R-$\phi$-direction and \unit[250]{$\mu$m} in $z$-direction.
% IBL has |eta|<3.0 with no vertex spread
In particular, the \ac{ibl}  plays a crucial role for \btag. Figure~\ref{fig:perf-blayer} compares the resolution of the transverse and longitudinal \aclp{ip},  which are important variables for \btag, with and without the \ac{ibl} installed in ATLAS.
All flavour-tagging studies in Part~\ref{part:ftag} of this thesis are performed for RUN II and therefore include the \ac{ibl}.\\
% Figure~\ref{fig:perf-blayer} compares the \btag performance with and without the \ac{ibl} installed in ATLAS. Clearly, the light-flavour rejection improves by a factor 3-4 and the \jet[c] rejection up to 80 \%. All flavour-tagging studies in Part~\ref{part:ftag} of this thesis are performed for RUN II and therefore include the \ac{ibl}.\\
Furthermore, the three remaining layers have a pixel size of \unit[50]{$\mu$m} in the R-$\phi$-direction and \unit[400]{$\mu$m} in the $z$-direction. This gives an expected hit resolution of \unit[8]{$\mu$m} \& \unit[10]{$\mu$m} in the direction of R-$\phi$ and \unit[40]{$\mu$m} \& \unit[115]{$\mu$m} in the $z$-direction for the \ac{ibl} and the three remaining layers, respectively.\\
In total, the pixel detector contains 86 M pixels providing a good spatial resolution which make up around $50 \%$ of all ATLAS readout channels.


\begin{figure}[h]
    \centering
    \subfloat[]{\includegraphics[width=.49\textwidth]{background/material_ATLAS/fig_01a.pdf}}
    \subfloat[]{\includegraphics[width=.49\textwidth]{background/material_ATLAS/fig_03a.pdf}}
    \caption{Unfolded transverse impact parameter resolution~(a) and longitudinal impact parameter resolution~(b) measured from data in 2015 (red) with \cme with the Inner Detector including the \ac{ibl}, as a function of pT, compared to that measured from data in 2012, \cme[8] (without the \ac{ibl})~\cite{ipresolution}.
    % Performance comparison of the \btag algorithm MV2c20 applied to jets from top pair events expressed in terms of the light-jet rejection (left) and \jet[c] rejection (right) as a function of the \jet efficiency. The red line shows the performance during RUN I without the \ac{ibl} and the blue dotted curve the performance in RUN II including the information from the \ac{ibl} (the algorithm is updated to the detector geometry in both cases)~\cite{insert-blayer}. 
    \label{fig:perf-blayer}}
\end{figure}

\subsubsection*{Semiconductor Tracker}
The \acf{sct} is a silicon strip detector comprising 4 double layers in the barrel region and nine planar end-cap discs on each side. The strips have a size of \unit[80]{$\mu$m}$\times \unit[12]{cm}$ and cover a region up to $|\eta|<2.5$. The two layers within one layer-module are rotated by a stereo angle of \unit[40]{mrad}. In general, the semiconductor-based detectors in ATLAS operate at a temperature between \unit[-10]{$^\circ$C} and \unit[-5]{$^\circ$C} to suppress different types of electronic noise.\\
Overall the \ac{sct} has a resolution of \unit[17]{$\mu$m} in the R-$\phi$ direction and \unit[580]{$\mu$m} in the $z$-direction with a total of 6.3 M readout channels.

\subsubsection*{Transition Radiation Tracker}


The outermost part of the \ac{id} is the \acf{trt}. In contrast to the other \ac{id} detectors, the \ac{trt} is not based on silicon but is a gaseous detector system. It consists of around $300$k straw tubes with a diameter of \unit[4]{mm} filled with a gas mixtures\footnote{In RUN~II, a second gas mixture of Ar (70 \%), CO$_2$ (27 \%) and \mbox{O$_2$ (3 \%)} was used for straw tubes belonging to modules with large gas leaks~\cite{IDET-2015-01}.} of Xe (70 \%), CO$_2$ (27 \%) and \mbox{O$_2$ (3 \%)} and a gold-plated tungsten wire in the tube centre with a potential different to the tube surface of \unit[1.5]{kV}. The straws have a length of \unit[144]{cm} in the barrel region and \unit[37]{cm} in the end cap. The single hit resolution is \unit[120]{$\mu$m} in the barrel and \unit[130]{$\mu$m} in the end-cap. In fact, the \ac{trt} provides besides the tracking information also a particle ID. This is achieved with emitted transition radiation at the material boundaries since the straws are interleaved with polypropylene. Especially electrons can be distinguished from charged pions due to their larger transition radiation.\\
However, the \ac{trt} will be replaced for the \ac{hllhc} by a new, fully silicon-based \ac{itk}~\cite{CERN-LHCC-2017-021} which will in fact replace the full \ac{id}.

\subsection{Calorimeter System}\label{sec:ATLAS-calo}

The calorimeter system is responsible for the precise measurement of the particle energies by absorbing them as well as measuring the shower properties to allow for particle identification. Showers are cascades of secondary particles which are formed when a highly energetic particle interacts with dense material.  ATLAS uses sampling calorimeters which consist of alternating layers of active material (liquid argon \& plastic scintillators) and passive detector material (copper, iron, tungsten and lead). While the active material measures the energy deposit of the particles, the passive material induces the shower creation. The calorimeter system is composed of two main sub-systems, the electromagnetic~\cite{CERN-LHCC-96-041,CERN-LHCC-2017-018} and the hadronic calorimeter~\cite{CERN-LHCC-96-042,Artamonov_2008} as shown in Figure~\ref{fig:calo}. The calorimeter covers an $\eta$ range up to a far forward region of $|\eta|<4.9$.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\textwidth]{background/material_ATLAS/Calorimeters_labels}
	\caption{Cut-away view of the calorimeter system of the ATLAS experiment~\cite{Collaboration_2008_ATLAS} (and adpated by~\cite{ACal}). \label{fig:calo}}
\end{figure}

% forward calo~\cite{Artamonov_2008}
\subsubsection*{Electromagnetic Calorimeter}
The \ac{em} calorimeter encloses the \ac{id} and is a high granularity sampling calorimeter based on \ac{lar} technology with absorber plates made out of lead. To provide full coverage in $\phi$, the \ac{em} calorimeter has an accordion-shaped structure where the active material is placed in the gaps between the lead absorber plates and the Kapton electrodes. The detector operates at \unit[-183]{$^\circ$C} with a total of 170k readout channels.
The barrel region of the \ac{em} calorimeter, consisting of two parts with a \unit[4]{mm} gap between them and a length of \unit[3.2]{m} each, covers $|\eta|<1.475$ with its granularity of $\Delta\eta\times\Delta\phi = 0.025\times0.025$ in the second layer (middle-layer) and the two end caps cover $|\eta|<3.2$ with a slightly coarser granularity.\\
% In general, the \ac{em} showers develop in the passive material and are measured in the active material. Especially the energy measurement of the electron and photon energy is crucial
In general, the absorption power at high energies of a calorimeter can be quantified in a material-independent way by using the radiation length $X_0$ of its medium. It is defined as the distance over which the particle energy is reduced via radiation losses by a factor $\nicefrac{1}{e}$. The thickness of the barrel region, given in terms of the radiation length, is $22\;X_0$ and $24\;X_0$ for the end-caps.
Moreover, the intrinsic energy resolution of the \ac{em} calorimeter is $\nicefrac{\sigma_E}{E}=10\%/\sqrt{E}\oplus0.7\%$~\cite[p. 9]{CERN-LHCC-2017-018}, where the first term is the stochastic part and the second one the constant part.


\subsubsection*{Hadronic Calorimeter}
The second calorimeter system is the hadronic calorimeter located around the \ac{em} calorimeter consisting of three components with two different detector technologies providing roughly 19,000 readout channels.\\
Firstly, the tile calorimeter is made out of alternating layers of steel as absorber material and scintillator plastic tiles as active material being read out via photomultiplier tubes. Out of its three layers, the first two have the highest granularity with  $\Delta\eta\times\Delta\phi = 0.1\times0.1$. The barrel part of the tile calorimeter covers a region with $|\eta|<1.0$ and the two extended barrels a range of $0.8<|\eta|<1.7$. The resolution of the tile calorimeter is $\nicefrac{\sigma_E}{E}=50\%/\sqrt{E}\oplus3\%$~\cite[p. 3]{Aaboud:2018scw}.\\
Secondly, the end-cap calorimeters, which are directly outside the \ac{em} calorimeter, and the forward calorimeter are based on the \ac{lar} technology. The end-caps use copper as passive material and cover a region of $1.5<|\eta|<3.2$ with their highest granularity of $0.1\times0.1\;(\Delta\eta\times\Delta\phi)$ within $|\eta|<2.5$. Also, the first layer of the forward calorimeter uses copper as absorber scoping for \ac{em} activities. The other two layers make use of tungsten as absorber which is better suitable for hadronic measurements. In total the forward calorimeter covers a region of $3.2<|\eta|<4.9$. The overall resolution of the \ac{lar} based hadronic calorimeters is $\nicefrac{\sigma_E}{E}=100\%/\sqrt{E}\oplus10\%$~\cite[p. 2]{Ilic_2014}.


\subsection{Muon Spectrometer}\label{sec:ATLAS-muon}
Muons mostly traverse the detector without losing energy. Hence they are identified in the \ac{ms}~\cite{CERN-LHCC-97-022} which is the outermost detector system of ATLAS with a distance to the beam of \unit[5-10]{m} (see Figure~\ref{fig:muonspec}). It consists of four detector systems grouped into trigger and precision muon tracking chambers. In total the \ac{ms} has more than one million readout channels and is embedded in three superconducting toroidal magnets (one in the barrel and one at each end-cap), providing a magnetic field in $\phi$-direction (typically perpendicular to the muon trajectory). The muon system is not entirely symmetric in $\phi$ due to some gaps for detector services and support structure (detector feet). The momentum resolution of the \ac{ms} is around $10\%$ for \unit[1]{TeV} muons and around $3\%$ for \unit[10-200]{GeV} muons.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\textwidth]{background/material_ATLAS/MuonSystem_d3}
	\caption{Layout of the ATLAS muon spectrometer~\cite{MuonSpec}. \label{fig:muonspec}}
\end{figure}
\subsubsection*{Muon Trigger Chambers}
The muon trigger chambers are designed for a fast readout to provide trigger information. In the barrel region with $|\eta|<1.05$, three layers of \ac{rpc} are used. The \ac{rpc} are made out of parallel plates with a high resistivity and a potential difference between them where the gap is filled with a gas mixture (94.7\% C$_2$H$_2$F$_4$, 5\% Iso-C$_4$H$_{10}$, 0.3\% SF$_6$). Besides the trigger information the \ac{rpc} also provide an $\eta-\phi$ measurement with a spatial resolution of \unit[10]{mm}.\\
In the end-caps ($1.05<|\eta|<2.4$) multi-wire chambers filled with a gas mixture of 55\% CO$_2$ and \mbox{45\% n-C$_5$H$_{12}$} are used. These are called \ac{tgc}. They use graphite-coated cathodes and the wires are separated by \unit[1.8]{mm}. Apart from the trigger information, the \ac{tgc} provide $\phi$ information with a resolution of \unit[5]{mm}.

\subsubsection*{Precision Muon Tracking Chambers}
While the muon trigger chambers are always read out, the precision muon tracking chambers are only read out when a trigger decision was made since the detector technology is slower but provides a high resolution and precision tracking information.\\
The \ac{mdt} are installed in the barrel and end-cap region covering \mbox{$|\eta|<2.7$}. \ac{mdt} are aluminium drift tubes with a diameter of \unit[3]{cm} filled with an Ar/CO$_2$ (93/7\%) gas mixture and a wire in their centres. Typically, each chamber contains 3-8 layers of drift tubes resulting in a spatial resolution of \unit[35]{$\mu$m}~\cite[p. 165]{Collaboration_2008_ATLAS}.\\
\acp{csc} are installed in the forward region ($2.0<|\eta|<2.7$) and are proportional multi-wire chambers (as the \ac{tgc}), providing a radial resolution of \unit[40]{$\mu$m} and a resolution in $\phi$ of \unit[5]{mm}~\cite[p. 165]{Collaboration_2008_ATLAS}.

\subsection{Magnet System}\label{sec:ATLAS-magnet}

Besides the detector systems, the magnet system is of major importance to allow momenta and charge measurements. It bends the trajectory of charged particles via the Lorentz force depending on their momentum and charge and consists of two sub-systems.\\
Firstly, the central solenoid magnet located between the \ac{id} and the calorimetry generates a constant magnetic field of \unit[2]{T}. The superconducting magnet made out of NbTi is cooled via liquid helium to a temperature of \unit[1.8]{K}.\\
Secondly, the vast toroidal magnet system embedded in the \ac{ms} comprising one barrel toroid and two end-cap toroids with eight coils each. The toroidal magnets deliver an inhomogeneous magnetic field of roughly \unit[0.5]{T} and \unit[1]{T} in the central and end-cap regions, respectively.
% field up to \unit[4.7]{T}. 


\subsection{Trigger System and Data Acquisition}\label{sec:ATLAS-daq}

In order to handle the high event rates, which are expected to be \unit[40]{MHz} for ATLAS corresponding to more than \unit[40]{TB/s} of data, a trigger system is required to reduce the amount of data to be recorded without losing important information. Since RUN II the trigger system is structured into two parts, the \mbox{\ac{l1}} hardware trigger and the software-based \ac{hlt}~\cite{Jenni:616089,Ruiz-Martinez:2133909} sketched in Figure~\ref{fig:ATLtrigger}.\\
The \ac{l1} trigger uses information from the \ac{rpc}, \ac{tgc} and the calorimeter to identify high \pt electrons, muons, photons, jets and high missing transverse momentum. It has a very fast latency of \unit[2.5]{$\mu$s} and reduces the rate to \unit[100]{kHz}. The \ac{l1} trigger identifies \acp{roi} in $\eta$ and $\phi$ and passes this information to the \ac{hlt}.\\
The \ac{hlt} is fully software based and uses the full detector information within the \acp{roi} to reduce the event rate down to approximately \unit[1]{kHz} with a latency of \unit[200]{ms}.\\
Afterwards, the data is transferred to a computing centre for further processing and storage.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.75\textwidth]{background/material_ATLAS/ATL_Trigger}
	\caption{Schematic view of the ATLAS trigger and data acquisition system in Run II~\cite{Ruiz-Martinez:2133909}. \label{fig:ATLtrigger}}
\end{figure}
