\pdfbookmark[1]{Current status of ttH measurements at the LHC}{current-status-of-tth-measurements-at-the-lhc}
\chapter{Current status of ttH measurements at the LHC}\label{chap:ttH}
The discovery of the Higgs boson in 2012 was a huge success for the \acs{lhc}. The four main Higgs production channels at the \acs{lhc} were described in Section~\ref{sec:theory:higgs}. This thesis focuses on the \ttH production channel which was observed by ATLAS and CMS~\cite{CMS-HIG-17-035,HIGG-2015-07,2018173} combining different Higgs decay channels. The \ttH production allows a direct measurement of the top-Yukawa coupling $y_t$ which is the largest Yukawa coupling within the \acs{sm}. The measurement of $y_t$ is an essential validity test of the \acs{sm} in particular for the Higgs mechanism and is important for both new physics searches and Higgs precision measurements. A direct $y_t$ measurement can be compared to indirect measurements, e.g. form the loop induced $ggF$ production or the $H\rightarrow\gamma\gamma$ decay mode, giving hints for possible effects beyond the \acs{sm}. Compared to the total Higgs production cross-section, the \ttH production only contributes around $1\%$ at the \acs{lhc}, as shown in Figure~\ref{fig:lhc-xs}, but has a recognisable detector signature with the two associated top quarks.\\
% \section{Motivation for ttH Production}
% top Yukawa coupling
% ttH observation website: http://cms-results.web.cern.ch/cms-results/public-results/publications/HIG-17-035/index.html
The  \hbb\ decay mode has the largest branching fraction of $58\%$ (see Fig.~\ref{fig:higgs-br}). It was observed by both ATLAS and CMS~\cite{CMS-HIG-18-016,HIGG-2018-04} and is sensitive to the second-largest Yukawa coupling in the \acs{sm}, the \quark Yukawa coupling $y_b$. Besides the large branching fraction, the \hbb decay mode also allows the kinematic reconstruction of the Higgs boson. Therefore, it is possible to further explore the properties of the Higgs boson in \tthbb events.\\

In this thesis, the \ttH production channel is being investigated together with the \hbb decay mode. This particular process is not yet discovered but CMS already sees an evidence~\cite{CMS-PAS-HIG-18-030}.\\
In this chapter, a short overview of the \ttH discovery and of the latest \tthbb results is given, based on the latest analysis results from ATLAS~\cite{PhysRevD.97.072016} and CMS~\cite{Sirunyan:2018mvw}, followed by the introduction to \acf{stxs} measurements.



% \todo{why tthbb not hyet found even though largest systematic -> which systematics dominate, adding ttH combination and show which channel is most sensitive}
\section{Latest Results}

\subsection{\ttH Observation}\label{sec:ttH:comb}
The observation of the \ttH production of ATLAS~\cite{2018173} and CMS~\cite{CMS-HIG-17-035} was an important achievement for the two experiments. The CMS combination considered five different Higgs decay channels as shown in Figure~\ref{fig:bkg:tth:obs-mus}~(b) using the data recorded during RUN~I and II of the \acs{lhc}, resulting in an observed (expected) significance of 5.2 (4.2) standard deviations. The ATLAS analysis used four different Higgs decay channels in the \ttH combination with RUN II data indicated in Figure~\ref{fig:bkg:tth:obs-mus}~(a) with an observed (expected) significance of 5.8 (4.9) standard deviations 
% - with RUN I 6.3 (5.1)
and a cross-section of $\sigma(\ttH)=(670\pm90(\text{stat.})^{+110}_{-100}\text{syst.})$. Table~\ref{tab:bkg:ttH:obs} lists the sensitivities of the individual channels among which the $H\rightarrow\gamma\gamma$ and multilepton decay channel of the Higgs boson have the largest significance. The $H\rightarrow\gamma\gamma$ decay channel is mainly dominated by statistical uncertainties while in the multilepton decay channel the statistical and systematic uncertainties have a similar influence. The \hbb channel has a significantly lower sensitivity and is dominated by systematic uncertainties mainly due to large uncertainties in the modelling of the \ttbb background.

\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[height=.4\textwidth]{background/material_ttH/fig_05.pdf}}
  \subfloat[]{\includegraphics[height=.4\textwidth]{background/material_ttH/CMS-ttH-obs-mus.pdf}}
  \caption{Signal strength parameter of the individual channels and the combined signal strength of \ttH shown for the \ttH observation of  ATLAS~\cite{2018173} (a) and  CMS~\cite{CMS-HIG-17-035}  (b).}
  \label{fig:bkg:tth:obs-mus}
\end{figure}


\begin{table}[]
    \centering
    \includegraphics[width=\linewidth]{background/material_ttH/tab_03.pdf}
    \caption{Overview of the results of the single channels used for the ATLAS \ttH combination indicating their respective cross-section and the observed and expected significance~\cite{2018173}.}
    \label{tab:bkg:ttH:obs}
\end{table}



\subsection{\tthbb Results}\label{sec:ttH:tthbb}
Both ATLAS and CMS have already performed searches for \tthbb with a subset of the \acs{lhc} RUN~II dataset shown in Ref.~\cite{PhysRevD.97.072016} with $\unit[36.1]{fb^{-1}}$ and~\cite{CMS-PAS-HIG-18-030} with $\unit[77.4]{fb^{-1}}$, respectively.\\
The ATLAS analysis uses events where at least one $W$-boson from one of the two top quarks decays leptonically and makes use of a complex definition of analysis regions. These analysis regions are mainly defined based on different \btag criteria to better extract the information of the signal and background processes in dedicated phase spaces. The analysis is optimised using a set of multivariate analysis techniques such as reconstruction and classification \acfp{bdt}, a  likelihood discriminant and the matrix element method~\cite{Fiedler:2010sg}.
The combined signal strength\footnote{The signal strength $\mu$ is defined as the cross-section ratio of the measured cross-section $\sigma$ over the cross-section expected in the \acs{sm} $\sigma_\text{SM}$: $\mu=\frac{\sigma}{\sigma_\text{SM}}$.} was measured with an observed (expected) significance of $1.4\;(1.6)$ standard deviations and a value of \mbox{$\mu=0.84\pm{0.29}(\text{stat.})^{+0.57}_{-0.54}(\text{syst.})$} shown in Figure~\ref{fig:bkg:tth:mus}~(a).\\
CMS combined two analyses, where one uses the 2016 dataset with $\unit[35.9]{fb^{-1}}$ and the second analysis is performed with the 2017 dataset with $\unit[41.5]{fb^{-1}}$. These two analyses cover the lepton+jets, dilepton and fully hadronic channels.
A matrix element method~\cite{Fiedler:2010sg} as well as multivariate techniques are employed. In the lepton+jets channel, a Deep Neural Network is employed to categorise the events into signal and multiple background processes via a multi-classification approach. At the same time, the output discriminants of the network are also used in the combined fit.
As shown in Figure~\ref{fig:bkg:tth:mus}, the combined signal strength was measured to $\mu=1.15\pm{0.15}(\text{stat.})^{+0.28}_{-0.25}(\text{syst.})$ with an observed (expected) significance of $3.9\;(3.5)$ standard deviations.\\
Both results are mainly dominated by systematic uncertainties and limited by the challenging modelling of the \ttbb background. The uncertainties are grouped into different sources and compared between the two analyses of ATLAS and CMS in Table~\ref{tab:tth:intro:syst}. Overall, the systematic uncertainties of the CMS analysis are considerably smaller than those from the ATLAS analysis. While the uncertainty associated to the \ttbin modelling is dominating the ATLAS analysis by far, the combined $\ttbar+\text{heavy flavour}$ modelling uncertainty is roughly in the same order as for instance the signal modelling uncertainty in the CMS analysis. ATLAS is accounting for the differences between the 4 and 5 flavour scheme in the \ttbb modelling which is fairly large whereas CMS is not taking them into account.



\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[height=.4\textwidth]{background/material_ttH/ttH-ATLAS-36-mus.pdf}}
  \subfloat[]{\includegraphics[height=.4\textwidth]{background/material_ttH/ttH-CMS-mus.pdf}}
  \caption{Signal strength of the first RUN~II \tthbb analyses for (a) the ATLAS analysis~\cite{PhysRevD.97.072016} and (b) CMS analysis~\cite{CMS-PAS-HIG-18-030} showing the signal strengths of the different channels and their combination.}
  \label{fig:bkg:tth:mus}
\end{figure}




\begin{table}[h!]
    \centering
    \renewcommand{\arraystretch}{1.}
    \begin{tabular}{lll}
    \toprule
         \multirow{2}{*}{Uncertainty source} & \multicolumn{2}{c}{$\Delta\mu$}\\
         & \multicolumn{1}{c}{ATLAS} & \multicolumn{1}{c}{CMS} \\
        % & ATLAS & CMS \\
         \toprule
            \ttbin modelling & $+0.46/-0.46$ & \\
            \ttcin modelling & $+0.09/-0.11$ & \\
            $\ttbar+\text{hf}$ modelling &  & $+0.14/-0.15$ \\
            Signal modelling & $+0.22/-0.05$  & $+0.15/-0.06$ \\
            \midrule
            Size of simulated sample & $+0.29/-0.31$ & $+0.10/-0.10$ \\
            \btag & $+0.16/-0.16$ & $+0.08/-0.07$ \\
            Jet energy scale and resolution & $+0.14/-0.14$ & $+0.05/-0.04$ \\
            \midrule
            \midrule
            Total systematic uncertainty & $+0.57/-0.54$ & $+0.28/-0.25$ \\
            Statistical & $+0.29-0.29$ & $+0.15/-0.15$\\
            \midrule
            Total & $+0.64/-0.61$ & $+0.32/-0.29$\\
        %  & 2015 & 2016--2018 & 2015 & 2016--2018 & 2015 & 2016--2018 \\
         
        %  \multirow{3}{*}{electrons} & \unit[24]{GeV} & \unit[26]{GeV} & medium & tight & -- & loose\vspace{0.1cm}\\
        %  & \unit[60]{GeV} & \unit[60]{GeV} & medium & medium & -- & --\vspace{0.1cm}\\
        %  & \unit[120]{GeV} & \unit[140]{GeV} & loose & loose & -- & --\vspace{0.1cm}\\
        %  \midrule
        %  \multirow{2}{*}{muons} & \unit[20]{GeV} & \unit[26]{GeV} &  medium & medium & loose & medium \vspace{0.1cm}\\
        %  & \unit[50]{GeV} & \unit[50]{GeV} & medium & medium & -- & --\\
        \bottomrule
    \end{tabular}
    \caption{Comparison of the breakdown of the contributions to the uncertainties in $\mu$ ($\Delta\mu$) for the ATLAS and CMS \tthbb analysis. The ATLAS analysis split the group of the \ttbar production in association with heavy flavour jets ($\ttbar+\text{hf}$) modelling uncertainties into \ttbin and \ttcin modelling while CMS provided the combined value for it~\cite{PhysRevD.97.072016,CMS-PAS-HIG-18-030}.}
    \label{tab:tth:intro:syst}
\end{table}




\section{Simplified Template Cross-Section Measurements}\label{sec:ttH-intro:stxs}

The \acf{stxs} formalism~\cite{Berger:2019wnu,deFlorian:2016spz} is a common effort of the \acs{lhc} experiments to define a consistent basis for comparable differential Higgs kinematic measurements performed in exclusive kinematic phase space regions (\ac{stxs} bins). This simplifies the combinations of different decay channels as well as between the experiments. Several theory uncertainties are directly folded into the measurements. 
Thus, the kinematic bins are optimised such that they reduce them as much as possible~\cite{deFlorian:2016spz,Badger:2016bpw}. For each \acs{stxs} bin, a separate signal template is defined which is the signal \acs{mc} prediction in the targeted kinematic region at truth level.
After the discovery of the most prominent Higgs decay channels, the statistics of RUN~II allows now to perform \ac{stxs} measurements in the \ttH channel.\\

Cross-section measurements in the \ttH production channel split into bins of the transverse momentum of the Higgs boson \ptH are sensitive to the CP structure of the Higgs boson~\cite{Boudjema:2015nda} and to the Higgs self-coupling~\cite{Maltoni:2017ims}. Figure~\ref{fig:bkg:tth:cp-stxs} shows the normalised \ptH distribution with the Higgs produced in the \ttH mode and the normalised differential cross-section as a function of \ptH for three different CP-scenarios of the Higgs coupling to the top quark: scalar (CP even) case (solid black) which corresponds to the predictions of the \acs{sm}, pseudo-scalar (CP odd) case (dashed blue) and the CP-violating case (dotted red). In the pseudo-scalar scenario, the values are shifted to higher \ptH values and the differential cross-section is suppressed with respect to the \acs{sm} case. In the presented analysis, these properties are, however, not yet investigated due to a too low sensitivity.\\
% In Figure~\ref{fig:bkg:tth:selfcoup-stxs} 

\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=.44\textwidth]{background/material_ttH/higgs-cp.pdf}}
  \subfloat[]{\includegraphics[width=.44\textwidth]{background/material_ttH/higgs-cp-differential.pdf}}
  \caption{The normalised event distribution (a) and the cross-section (b)  as a function of the Higgs transverse momentum \ptH shown for three different CP-scenarios of the Higgs boson: CP even (solid black), CP odd (dashed blue) and CP-violating (dotted red)~\cite{Boudjema:2015nda}.}
  \label{fig:bkg:tth:cp-stxs}
\end{figure}

% \begin{figure}[htbp]
%     \centering
%     \includegraphics[width=.6\textwidth]{background/material_ttH/higgs-self_couplings.pdf}
%     \caption{\cite{Maltoni:2017ims}}
%     \label{fig:bkg:tth:selfcoup-stxs}
% \end{figure}

The \hbb decay mode is well suited to probe the differential cross-sections due to their large production rate. Even the high $\pt^H$ regime can be accessed in which for instance the $\ttH(H\rightarrow \gamma\gamma)$ decay mode is lacking statistics~\cite{ATLAS-CONF-2019-004}. Another advantage is that the $b\bar{b}$ final state of the Higgs can be fully reconstructed.\\
% \ptHtruth\\
Similar to the recommendations for the \acs{stxs} bins for other Higgs production channels given in Ref.~\cite{Berger:2019wnu}, taking into account the theory considerations from above, the following \acs{stxs} bins  used for the analysis shown in this thesis are:  \mbox{$0~\GeV\leq\ptH<120~\GeV$},  \mbox{$120~\GeV\leq\ptH<200~\GeV$},\linebreak \mbox{$200~\GeV\leq\ptH<300~\GeV$}, \mbox{$300~\GeV\leq\ptH<450~\GeV$}, and \mbox{$\ptH\geq450~\GeV$}.



% \FloatBarrier
% \clearpage
% \newpage

% The simplified template cross-section (STXS) framework has been designed to maximize the sensitivity of
% the Higgs boson cross-section measurements while minimizing their
% theory dependence~\cite{Badger:2016bpw,deFlorian:2016spz}. In this
% framework cross-section measurements are performed in mutually
% exclusive phase space regions (STXS bins) specific to the different
% Higgs boson production modes, and can be easily combined across the
% decay channels. In the recently proposed STXS stage 1.1
% binning~\cite{Berger:2019wnu}, there is only one bin dedicated to the
% \ttH production mode. However, introducing an additional splitting in
% this part of the phase space would allow to constrain possible Beyond
% Standard Model effects, such as CP-odd contributions, or gaining
% sensitivity to the Higgs boson
% self-coupling. In this analysis, we aim to split this part
% of phase space into Higgs boson transverse
% momentum (\ptH) bins.

% The STXS measurement is performed at a function of the Higgs boson transverse momentum.
% In addition to the total \ttH\ cross-section, since the $t\bar{t}H(b\bar{b})$ final states allow to reconstruct the Higgs boson kinematics,
% the cross-section is also measured as a function of the true Higgs boson transverse momentum (\ptH), in the STXS formalism~\cite{deFlorian:2016spz}.
% This measurement is performed both in the dilepton and in the single-lepton resolved and boosted channels. In case of events overlapping between resolved and boosted category, boosted events are being vetoed in the resolved regions.

% The \ttH\ signal is split in five \ptH\ categories at truth level, using the bin boundaries discussed and defined within the LHC Higgs cross-section working group: 0~\GeV<\ptH<120~\GeV, 120~\GeV<\ptH<200~\GeV, 200~\GeV<\ptH<300~\GeV, 300~\GeV<\ptH<450~\GeV, and \ptH>450~\GeV. The last two bins can be merged into one if the analysis does not have the necessary sensitivity. For this reason two different scenarios have been considered and studied: one with four bins and one with five, reaching the highest  \ptH\ boundary at 450~\GeV.
% A profile likelihood fit is performed to measure the signal strengths in each of the category simultaneously. The boosted analysis category is optimised to improve the precision in the bins \ptH>300~\GeV.
% The 5-bins scenario is used as baseline for this analysis; therefore fits using this scenario are shown in the main body of this supporting note,
% while the fits performed using the 4-bins scenario are shown in Appendix~\ref{subsec:STXS4binsFits}.
% %Table~\ref{tab:discrPerRegionSTXS} summarises the discriminating variable used in the fit for the STXS measurement, for each signal region,
% %as well as the number of bins of the fitted distribution, for each of the five \ptH\ category.


% In order to perform this measurement, changes w.r.t. inclusive cross-section have been made in the event categories. While the definition of the control regions in the single-lepton resolved and dilepton channels stay unchanged, the most sensitive category \SR[]{6}{4} (\SR[]{4}{4}) in the single-lepton (dilepton) channel, defined by requiring at
% least 6~jets (4~jets) out of which at least 4 are $b$-tagged at the 70\% WP, is not further split based on the tighter $b$-tagging WP into the \SR[lo]{6}{4} and \SR[hi]{6}{4} (\SR[lo]{4}{4} and \SR[hi]{4}{4}) regions as it is the case in the inclusive cross-section measurement. Instead, this most sensitive categories in each channel is further split based on the \pt of the Higgs boson candidate
% reconstructed using the parton-to-jet assignments determined by a boosted decision tree - reconstruction BDT as descried in Section ~\ref{sec:Strategy}. The selection criteria on the \pt of the
% Higgs boson candidate correspond to the STXS bin boundaries in each binning
% scenario. In this way, the different regions defined by
% selections on the \pt of the Higgs boson candidate are dominated by
% Higgs boson signal from the corresponding STXS \ptH bin, minimising the
% correlation among signal strengths in different STXS bins. In each of the regions, the Classification BDT described in section ~\ref{sec:Strategy} is used as an input to the fit. The binning of the fitted distribution in each region is re-optimised with respect to the binning used for the measurement of the total cross-section (cf. Table~\ref{tab:discrPerRegion})
% in order to account for the increased statistical fluctuations induced by the splitting in different \ptH\ categories.
% Table~\ref{tab:discrPerRegionSTXS} summarises the discriminating variable used in the fit for the STXS measurement, for each signal region,
% as well as the number of bins of the fitted distribution, for each of the five \ptH\ category.
