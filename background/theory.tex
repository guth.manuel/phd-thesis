\pdfbookmark[1]{The Standard Model of Particle Physics}{theory-chap }
\chapter{The Standard Model of Particle Physics}\label{chap:theory}
The \ac{sm} of elementary particle physics is the theoretical framework describing the known elementary particles and their interactions comprising all fundamental forces - the electromagnetic, the strong and the weak force - except the gravitational force. This theory has been probed over the last decades with enormous precision, although there are also hints for physics beyond its scope.

The \ac{sm} is inspired by two main principles: simplicity and symmetries.
It is a non-abelian gauge theory invariant under the gauge group
\begin{equation}
        G = SU(3)_C \otimes SU(2)_L \otimes U(1)_Y,
\end{equation}
described in the framework of Lorentz invariant \ac{qft} with the Lagrangian being renormalisable and invariant under local gauge transformation.\\
This chapter gives a brief overview of the particle content of the \ac{sm} in Section~\ref{sec:sm-particles} followed by the description of \ac{qed} and \ac{qcd} in Sections~\ref{sec:theory:qed} and \ref{sec:theory:qcd}, respectively. Afterwards, the electroweak unification (sec.~\ref{sec:theory:emweak}) and the Higgs sector (sec.~\ref{sec:theory:higgs}) are introduced. The content of this chapter is mainly inspired by~\cite{Peskin:1995ev,Burgess:2007zi,Hollik:2010id}.


% Thompson~\cite{thomson_2013}    
% \begin{equation}
%       {\mathcal {L}}_{\text{SM}} = {\mathcal {L}}_{\text{EW}} + {\mathcal {L}}_{\text{QCD}}, 
% \end{equation}
\section{Particle Content of the Standard Model} \label{sec:sm-particles}
The \ac{sm} comprises all known elementary particles summarised in Figure~\ref{fig:SMparticles}. It consists of twelve fermions (half-integer spin particles), twelve vector bosons (spin-1 particles) and the Higgs boson, a scalar particle (spin 0). 


\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{background/material_theory/SMinfographic_image-2}
	\caption{Overview of the particles in the Standard Model~\cite{smpar}. Adapted the top quark mass according to Ref.~\cite{TOPQ-2017-03} and the Higgs boson mass according to Ref.~\cite{PhysRevD.98.030001}. \label{fig:SMparticles}}
% 	\caption{Overview of the particles of the SM grouped by~\cite{SMparticles}. \label{fig:SMpart}}
\end{figure}

Fermions are sorted into three generations comprising one charged lepton, one neutrino and two quarks each. The particles of a different generation have identical quantum numbers with the exception of their mass. In fact, the ordinary matter is only composed of the first generation fermions. In addition, every fermion\footnote{In fact, the $W$-boson also has its anti-particle.} has also an associated anti-particle with opposite charge.\\
Quarks carry an electric and a colour charge and are therefore interacting weakly, electromagnetically and strongly. Each generation has an up-type quark (up-, charm- \& top-quark) and a down-type quark (down-, strange \& bottom-quark) with an electric charge of $Q=\nicefrac{2}{3}$ and $Q=-\nicefrac{1}{3}$, respectively. In general, quarks can only occur in bound states due to the colour confinement~\cite{PhysRevLett.30.1346}. The colour charge was introduced to maintain the Pauli principle and explain the coexistence of quarks in hadrons in otherwise identical quantum states. These bound states are called hadrons and they can be either fermions formed out of three quarks denoted as baryons or bosons composed of a quark and an anti-quark denoted as mesons\footnote{The LHCb collaboration discovered also penta- and tetra-quark states~\cite{Aaij:2015tga}.}.\\
Leptons are the electron $e$, muon $\mu$ and tau $\tau$ and their associated neutrinos $\nu_e$, $\nu_\mu$ and $\nu_\tau$, respectively. The neutrinos are considered massless in the \ac{sm}. While the charged leptons ($e,\mu,\tau$) carry an electric charge $Q=-1$ and can interact electromagnetically, neutrinos carry neither electric nor colour charge and therefore are only interacting via the weak force.

The vector bosons are gauge bosons and act as force carriers. The massless photon $\gamma$ is the mediator of the electromagnetic force while the massive $Z$ and $W^\pm$ bosons are associated to the weak force. To be strictly accurate, they are all associated to the electroweak theory which unifies the electromagnetic and weak theory, described in Section~\ref{sec:theory:emweak}. Furthermore, there are eight types of gluons $g$ carrying the strong force.\\
The Higgs boson is the only scalar particle of the \ac{sm}. The Higgs mechanism and the concept of \ac{ewsb} is discussed in more detail in Section~\ref{sec:theory:higgs}.\\

In the following, using the terms electrons, muons and taus comprise always the particles and anti-particles if not stated differently. The same is valid for quarks and anti-quarks.
% Last, the Higgs boson being a part of the \ac{ewsb} discussed in more detail in Section~\ref{sec:theory:higgs}.\\



\section{Quantum Electrodynamics}\label{sec:theory:qed}

The framework of \ac{qft} combines quantum mechanics and special relativity and thus particles are represented as fields. \ac{qed} is the theoretical description of the electromagnetic interactions, based on the abelian $U(1)$ gauge group, being a generalisation of Maxwell's theory.\\
A freely propagating fermion field corresponding to a massive spin $\nicefrac{1}{2}$ particle is described by the Dirac Lagrangian
\begin{equation}
    {\mathcal {L}}_{\text{Dirac}} = \bar{\psi}(\iu \slashed{\partial}-m)\psi,\label{eq:dilag}
\end{equation}
where $\slashed{\partial}=\gamma^\mu\partial_\mu$ denotes the contraction with the Dirac matrices $\gamma^\mu$, the fermion mass $m$ and a free spinor field $\psi$.\\
    %  Comparing the Dirac equation with the relativistic Maxwell equations
    % \begin{align}
    %     \partial_\rho F_{\mu\nu}+\partial_\mu F_{\nu\rho}+\partial_\nu F_{\rho\mu}=0,\\
    %     \partial_\mu F^{\mu\nu} = j^\nu,\label{eq:max2}
    % \end{align}
    % with $F_{\mu\nu}=\partial_\mu A_\nu - \partial_\nu A_\mu$ the field strength tensor and $A_\mu$ a vector field, one finds that the Maxwell equations are invariant under the gauge transformation $A_\mu\xrightarrow{}A_\mu+\frac{1}{e}\partial_\mu\alpha$, but the Dirac Lagrangian is not ($e$ is the electrical charge). 
    A local $U(1)$ gauge transformation would lead to an additional term $\bar{\psi}\slashed{\partial}\alpha\psi$ in the Lagrangian, with $\alpha$ being the electromagnetic coupling constant. It is enforced that the \ac{qed} Lagrangian is invariant under this gauge transformation and thus a coupling between the Dirac fermion and the vector field $A_\mu$ (corresponding to the photon) is introduced in the form of a covariant derivative
      \begin{equation}
        D_\mu = \partial_\mu +\iu e A_\mu(x),\label{eq:covder}
    \end{equation}
    where $e=-|e|$ is the electron charge.\\
    The \ac{qed} lagrangian results in
    \begin{align}
        \mathcal{L}_\text{QED} &= \mathcal{L}_\text{Dirac} + \mathcal{L}_\text{Maxwell} + \mathcal{L}_\text{interaction}\\
        &=\bar{\psi}\left(\iu \slashed{\partial}-m\right)\psi -\frac{1}{4}F^{\mu\nu}F_{\mu\nu} - e\bar{\psi}\gamma^\mu\psi A_\mu\\
        &=\bar{\psi}\left(\iu \slashed{D}-m\right)\psi -\frac{1}{4}F^{\mu\nu}F_{\mu\nu},
    \end{align}
    with $F_{\mu\nu}=\partial_\mu A_\nu - \partial_\nu A_\mu$ the field strength tensor. Consequently, the constructed \ac{qed} lagragian is invariant under a local $U(1)$ gauge transformation
    \begin{equation}
        \psi(x) \xrightarrow{}  e^{-\iu \alpha(x)}\psi(x),\qquad  A_\mu(x) \xrightarrow{} A_\mu(x) -\frac{1}{e}\partial_\mu\alpha(x).
    \end{equation}

\section{Quantum Chromodynamics}\label{sec:theory:qcd}
The strong interactions are described by \ac{qcd} which is a non-abelian gauge theory based on the $SU(3)$ group. The Lagrangian can be retrieved in a similar manner as for \ac{qed}. In this context the quark field can be written as colour triplets $\bar{q}_k=(\bar{q}_\text{red}, \bar{q}_\text{blue}, \bar{q}_\text{green})$, which transform under a local gauge transformation as
\begin{equation}
q_k(x) \xrightarrow{} e^{\iu\alpha_a \lambda_a/2}q_k(x),\qquad \alpha\in\mathbb{R},\quad a\in\{1,\mathellipsis,8\},
\end{equation}
with $k$ the flavour index, $\alpha_a$ a local phase and $\lambda_a$ the generators of the $SU(3)$ group called \textit{Gell-Mann matrices}~\cite{PhysRev.125.1067} and $a$ the colour index. They follow the commutation rule
\begin{align}
[\lambda_a,\lambda_b]=\iu f_{ab}^c \lambda_c,
\end{align}
with $f_{ab}^c$ the completely anti-symmetric structure constant. The coupling between quarks and gluons is introduced analogous to \ac{qed} as a covariant derivative
\begin{equation}
D_\mu = \partial_\mu - \iu g_s \frac{\lambda_a}{2}G_\mu^a,
\end{equation}
where $G_\mu^a$ are the eight gluon field strength tensors and $g_s$ is the strong coupling strength which can also be expressed as the coupling constant of the strong interaction
\begin{equation}
    \alpha_s=\frac{g_s^2}{4\pi}.
\end{equation}
The final QCD Lagrangian then reads
\begin{align}
{\mathcal {L}}_{\text{QCD}}&=\sum_k{\bar {q }_k}\left(\iu\slashed{D}-m_k\right)q_k-{\frac {1}{4}}G_{\mu \nu }^{a}G^{a\mu \nu}\\
&=\sum_k{\bar {q }_k}\left(i\slashed{\partial}-m_k\right)q_k+ \frac{ g_s}{2}{\bar {q}_k}\left(i\gamma ^{\mu }G_\mu^a\lambda_a \right)q_k - {\frac {1}{4}}G_{\mu \nu }^{a}G^{a\mu \nu},
\label{eq:qcdlag}
\end{align}
with
\begin{equation}
    G^a_{\mu\nu}=\partial_\mu G_\nu^a-\partial_\nu G_\mu^a+g_s f^a_{\beta\gamma}G^\beta_\mu G_\nu^\gamma.
\end{equation}
Due to the non-abelian structure of \ac{qcd}, the term ${\frac {1}{4}}G_{\mu \nu }^{a}G^{a\mu \nu}$ is needed in order to maintain local gauge invariance of the Lagrangian and results in a self-coupling of the gluons illustrated in the two Feynman diagrams on the right in Figure~\ref{fig:gluoninteractions}.
\begin{figure}[h]
	\centering
	\includegraphics[width=1.\textwidth]{background/material_theory/gluon-interactions}
	\caption{Possible interactions of the gluon: the interaction with quarks (left), self-interaciton of three gluons (middle) and the self interaction of four gluons (right).  \label{fig:gluoninteractions}}
\end{figure}
% \begin{fmffile}{feyngraphgluons}
% \begin{figure}[h] % Do not use only [h] in real documents.
% \begin{minipage}[b]{.33\linewidth}
% \vspace{3ex}
% \centering
% \begin{fmfgraph*}(70,70)
%  \fmfstraight
%     \fmfleft{i1,i2}
%     \fmfright{h}
%     % gluons
%     \fmf{fermion}{v,i1}
%     \fmf{fermion}{i2,v}
%     % \fmfblob{20}{v}
%     % Higgs boson
%     \fmf{gluon, label=$\mathbf{g_s}$,label.dist=0.4cm}{v,h}
%     % \fmflabel{$g_s$}{h}
%     \fmflabel{$q$}{i2}
%     \fmflabel{$\bar{q}$}{i1}
%   \end{fmfgraph*}
% % \vspace{5ex}

% \vspace{5ex}
% \end{minipage}
% \begin{minipage}[b]{.33\linewidth}
% \vspace{3ex}
% \centering
% \begin{fmfgraph*}(70,70)
%     \fmfstraight
%     \fmfleft{i1,i2}
%     \fmfright{h}
%     % gluons
%     \fmf{gluon}{i1,v}
%     \fmf{gluon}{v,i2}
%     % \fmfblob{20}{v}
%     % Higgs boson
%     \fmf{gluon, label=$\mathbf{g_s}$,label.dist=0.4cm}{v,h}
%     % \fmf{gluon}{h,o1}
%     % \fmf{dashes,tension=1}{h,o2}
%   \end{fmfgraph*}
% \vspace{5ex}

% % \vspace{5ex}
% \end{minipage}
% \begin{minipage}[b]{.33\linewidth}
% \vspace{3ex}
% \centering
% % \caption*{(a) ggF}
%     \begin{fmfgraph*}(70,70)
%     \fmfstraight
%     \fmfleft{i1,i2}
%     \fmfright{o1,o2}
%     % gluons
%     \fmf{gluon}{i1,v}
%     \fmf{gluon}{v,i2}
%     % \fmfblob{20}{v}
%     % Higgs boson
%     \fmf{gluon, label=$\mathbf{g_s^2}$,label.dist=0.3cm}{v,o1}
%     \fmf{gluon}{v,o2}
%   \end{fmfgraph*}
% \vspace{5ex}

% % \vspace{5ex}
% \end{minipage}
% \caption{Possible interactions of the gluon, the interaction with quarks (right), self-interaciton of three gluons (middle) and the self interaction of four gluons (right).  \label{fig:gluoninteractions}}
% \end{figure}
% \end{fmffile}   
The self-coupling induces a different energy scaling behaviour compared to \ac{qed}. The coupling constant $\alpha_s$ depends on the energy scale (renormalisation scale) $\mu_R^2$~\cite{Salam:2010zt},
\begin{equation}
    \alpha_s(\mu_R^2)=\frac{12\pi}{(33-2n_f)\ln(\nicefrac{\mu_R^2}{\Lambda^2_\text{QCD}})},\label{eq:theory:alphas}
\end{equation}
with $n_f$ the number of 'light' quark flavours (those whose mass is lower than $\mu$) and $\Lambda_\text{QCD}$ a non-perturbative constant indicating the scale at which the coupling diverges. 
The application of perturbation theory in order to calculate scattering amplitudes is only feasible for scales  $\mu_R\gg\Lambda_\text{QCD}$, where $\alpha_s(\mu_R^2)\ll1$.
% The perturbation theory is only applicable to the scales $\mu_R\gg\Lambda_\text{QCD}$ where $\alpha_s(\mu_R^2)\ll1$.
At low energy scales (larger distances) the effective coupling between two coloured particles increases and thus coloured objects cannot exist isolated and always form colourless bound states (hadrons), this effect is called colour confinement. At high energy scales (short distances) the coupling strength is decreasing, denoted as asymptotic freedom~\cite{PhysRevD.8.3633,PhysRevLett.30.1346}.


\section{Electroweak Unification}\label{sec:theory:emweak}
The electroweak unification was introduced in the 1960s by Glashow, Salam and Weinberg~\cite{Glashow,Salam,Weinberg}. It unifies electromagnetic and weak interactions within one theory based on the non-abelian gauge group $SU(2)_L \otimes U(1)_Y$. The electroweak Lagrangian is composed of several parts, the gauge, fermion, Higgs and Yukawa part
\begin{equation}
       {\mathcal {L}}_{\text{EW}} = {\mathcal {L}}_{\text{gauge}} + {\mathcal {L}}_{\text{fermion}} + {\mathcal {L}}_\text{Higgs} + {\mathcal {L}}_{\text{Yukawa}}.
\end{equation}

Fermions (leptons and quarks) are represented as left-handed doublets $\psi_L$ and right-handed singlets $\psi_R$, classified with the quantum numbers of the weak isospin $I$ ($SU(2)$ generators), $I_3$ being the third component of the isospin, and the weak hypercharge $Y$ ($U(1)$ generators) as shown in Table~\ref{tab:doublets} where the doublets have $I_3=\nicefrac{1}{2}$ and the singlets $I_3=0$. The Gell-Mann–Nishijima formula~\cite{NishiGell} relates these two quantum numbers with the electric charge $Q$
\begin{equation}
    Q = I_3+\frac{Y}{2}.
\end{equation}
% \begin{table}[h]
% \centering
% \begin{tabular}{ccc|ccc}
% 	\toprule
% 	\multicolumn{3}{c}{Generations} & \multicolumn{3}{c}{Charges} \\ 
% 	I & II & III  & $I_3$ & Y & Q\\
% 	\midrule 
% 	$	\left( 	\begin{array}{c} 	\nu_e \\ 	e	\end{array}\right) _L $ & 			$	\left( 	\begin{array}{c}	\nu_\mu \\ 		\mu		\end{array}\right) _L$ &$	\left( 	\begin{array}{c}		\nu_\tau \\ 		\tau \end{array}\right) _L$ & $\begin{array}{c} 	+\nicefrac{1}{2} \\ 	-\nicefrac{1}{2}	\end{array}$ & $\begin{array}{c} 	-1 \\ 	-1	\end{array}$ & $\begin{array}{c} 	0 \\ 	-1\end{array}$\\
% 	$e_R$ & $\mu_R$ & $\tau_R$ & 0 & $-2$ & $-1$\\ \midrule
% 		$	\left( 	\begin{array}{c} 	u \\ 	d	\end{array}\right) _L $ & 			$	\left( 	\begin{array}{c}	c \\ 		s	\end{array}\right) _L$ &$	\left( 	\begin{array}{c}		t \\ 		b\end{array}\right) _L$ & $\begin{array}{c} 	+\nicefrac{1}{2} \\ 	-\nicefrac{1}{2}	\end{array}$ & $\begin{array}{c} 	\nicefrac{1}{3} \\ 	\nicefrac{1}{3}	\end{array}$ & $\begin{array}{c} 	+\nicefrac{2}{3} \\ 	-\nicefrac{1}{3}\end{array}$\\
% 		$\begin{array}{c} 	u_R \\ 	d_R	\end{array}$ & $\begin{array}{c} 	c_R \\ 	s_R	\end{array}$ & $\begin{array}{c} 	t_R \\ 	b_R	\end{array}$ & $\begin{array}{c} 	0 \\ 0	\end{array}$ & $\begin{array}{c} 	\nicefrac{4}{3} \\ 	-\nicefrac{2}{3}	\end{array}$ & $\begin{array}{c} 	+\nicefrac{2}{3} \\ 	-\nicefrac{1}{3}\end{array}$	\\
	
	
% 	\bottomrule 	\end{tabular} 
% \caption{ The fermions are grouped in singlets according to the $U(1)$ symmetry group and in doublets corresponding to the $SU(2)$ symmetry group. The shown quantum numbers are the third component of the weak isospin ($I_3$), the weak hypercharge ($Y$) and the electric charge ($Q$). \label{tab:doublets}}
% \end{table}

\begin{table}[h]
\centering
\begin{tabular}{c|ccc|ccc}
	\toprule
	Fields & \multicolumn{3}{c}{Generations} & \multicolumn{3}{c}{Charges} \\ 
	& I & II & III  & $I_3$ & Y & Q\\
	\midrule 
	$\psi_L,\;L_L$ & $	\left( 	\begin{array}{c} 	\nu_e \\ 	e	\end{array}\right) _L $ & 			$	\left( 	\begin{array}{c}	\nu_\mu \\ 		\mu		\end{array}\right) _L$ &$	\left( 	\begin{array}{c}		\nu_\tau \\ 		\tau \end{array}\right) _L$ & $\begin{array}{c} 	+\nicefrac{1}{2} \\ 	-\nicefrac{1}{2}	\end{array}$ & $\begin{array}{c} 	-1 \\ 	-1	\end{array}$ & $\begin{array}{c} 	0 \\ 	-1\end{array}$\\
	$\psi_R,\;\ell_R$ & $e_R$ & $\mu_R$ & $\tau_R$ & 0 & $-2$ & $-1$\\ \midrule
	 	$\psi_L,\;Q_L$& 	$	\left( 	\begin{array}{c} 	u \\ 	d	\end{array}\right) _L $ & 			$	\left( 	\begin{array}{c}	c \\ 		s	\end{array}\right) _L$ &$	\left( 	\begin{array}{c}		t \\ 		b\end{array}\right) _L$ & $\begin{array}{c} 	+\nicefrac{1}{2} \\ 	-\nicefrac{1}{2}	\end{array}$ & $\begin{array}{c} 	\nicefrac{1}{3} \\ 	\nicefrac{1}{3}	\end{array}$ & $\begin{array}{c} 	+\nicefrac{2}{3} \\ 	-\nicefrac{1}{3}\end{array}$\\
	$\psi_R,\;\begin{array}{c} 	\mathit{u}_R \\ 	\mathit{d}_R	\end{array}$ & 	$\begin{array}{c} 	u_R \\ 	d_R	\end{array}$ & $\begin{array}{c} 	c_R \\ 	s_R	\end{array}$ & $\begin{array}{c} 	t_R \\ 	b_R	\end{array}$ & $\begin{array}{c} 	0 \\ 0	\end{array}$ & $\begin{array}{c} 	\nicefrac{4}{3} \\ 	-\nicefrac{2}{3}	\end{array}$ & $\begin{array}{c} 	+\nicefrac{2}{3} \\ 	-\nicefrac{1}{3}\end{array}$	\\
	
	
	\bottomrule 	\end{tabular} 
\caption{ The fermions are grouped into right-handed singlets %according to the $U(1)$ symmetry group 
and in left-handed doublets. % corresponding to the $SU(2)$ symmetry group. 
The shown quantum numbers are the third component of the weak isospin ($I_3$), the weak hypercharge ($Y$) and the electric charge ($Q$). The field column contains the definition of the different fields. \label{tab:doublets}}
\end{table}


% \begin{equation}
%     T_a=I_a (a\in \{1,2,3\}), \quad T_4=Y
% \end{equation}


% \begin{equation}
%     [I_a,I_b]=\iu\epsilon_{abc}I_c, \quad [I_a,Y]=0
% \end{equation}
\subsection{Gauge Term}

Each generalised charge is associated to a vector field, $W_\mu^{1,2,3}$ to $I_{1,2,3}$ and the singlet field $B_\mu$ to $Y$. The field strength tensors of the vector fields are given as
\begin{align}
    W_{\mu\nu}^a&=\partial_\mu W_\nu^a-\partial_\nu W_\mu^a+g_2\epsilon_{abc}W_\mu^b W_\nu^c,\\
    B_{\mu\nu}&=\partial_\mu B_\nu-\partial_\nu B_\mu,
\end{align}
with $\epsilon_{abc}$ the totally asymmetric Levi-Civita tensor and $g_2$ the gauge coupling constant for the non-abelian factor $SU(2)$. The Lagrangian for the gauge part then reads
\begin{equation}
    \mathcal {L}_\text{gauge} = -\frac{1}{4} W_{\mu\nu}^a W^{\mu\nu,a} - \frac{1}{4}B_{\mu\nu}B^{\mu\nu}.
\end{equation}
With this formulation, mass terms for the gauge bosons would violate the gauge invariance. However, it is possible to introduce these mass terms with the mechanism of spontaneous electroweak symmetry breaking described below.

\subsection{Fermion Term}
As mentioned above, the fermions have different chiralities (left- \& right-handed) on which also their representation depends (see Table~\ref{tab:doublets}).
The covariant derivatives, describing the fermion-gauge field interaction, are slightly different for the right-handed $R$ and left-handed $L$ case:
\begin{align}
    D_\mu^L&=\partial_\mu-\iu g_2 \frac{{\sigma}_a}{2}W_\mu^a+\iu g_1\frac{Y}{2}B_\mu,\\
    D_\mu^R&=\partial_\mu+\iu g_1\frac{Y}{2}B_\mu,
\end{align}
with $\Vec{\sigma}=(\sigma_1 \quad \sigma_2 \quad\sigma_3)^T$ being the vector of Pauli matrices satisfying $[\sigma_i,\sigma_j]=2\iu\epsilon_{ijk}\sigma_k$ and $g_1$ the gauge coupling constant for $U(1)_Y$ gauge group.
Then, the fermionic part of the lagrangian is denoted as
\begin{equation}
    \mathcal {L}_\text{fermion} = \sum_j\bar{\psi}^j_L\iu \gamma^\mu D_\mu^L\psi_L^j+\sum_{j,\xi}\bar{\psi}^j_{R\xi}\iu \gamma^\mu D_\mu^R\psi_{R,\xi}^j\label{eq:theory:fermionlagrangian},
\end{equation}
with the generation index $j$ running over the three lepton and quark generations and $\xi$ the index for up-type and down-type fermions. Similar to the gauge term, the fermion masses are also not described here since they are mixing left- and right-handed fields which would break the gauge symmetry. The fermion masses are introduced via the Yukawa term in Section~\ref{sec:theory:yukawa}.

\subsection{Higgs Term}\label{sec:theory:emweak:higgs}
The Higgs mechanism, introduced in the 1960s~\cite{PhysRevLett.13.321,HIGGS1964132,PhysRevLett.13.508,PhysRevLett.13.585,PhysRev.145.1156,PhysRev.155.1554}, spontaneously breaks the gauge symmetry $SU(2)_L \otimes U(1)_Y$ down to the $ U(1)_\text{EM}$ symmetry by introducing an isospin doublet of complex scalar fields
\begin{equation}
    \phi(x)=\left(\begin{array}{cc}
         \varphi^+(x)  \\
         \varphi^0(x)
    \end{array}\right) 
    % = 
    % \frac{1}{\sqrt{2}}
    %  \left(\begin{array}{cc}
    %      \varphi_1(x) +\iu \varphi_2(x) \\
    %      \varphi_3(x) +\iu \varphi_4(x)
    % \end{array}\right)
\end{equation}
with the covariant derivative
\begin{equation}
    D_\mu=\partial_\mu-\iu g_2\frac{{\sigma}_a}{2}W_\mu^a+\iu\frac{g_1}{2}B_\mu,
\end{equation}
which introduces three- and four-point interactions between the gauge bosons and the Higgs field in the lagrange density
\begin{equation}
    \mathcal{L}_\text{Higgs}= \left(D_\mu\phi\right)^\dagger D^\mu\phi-V(\phi). \label{eq:theory:higgs-lagrangian}
\end{equation}
The Higgs potential
\begin{equation}
    V(\phi) = -\mu^2\phi^\dagger\phi+\frac{\lambda}{4}\left(\phi^\dagger\phi\right)^2,\label{eq:theory:higgspot}
\end{equation}
contains in the first term the Higgs mass after the \ac{ewsb} with the constant $\mu^2$ and in the second term the Higgs field self-interaction with the constant $\lambda>0$ guaranteeing a lower bound of the potential.


\subsection{Yukawa Term}\label{sec:theory:yukawa}
The last term of the electroweak Lagrangian is the Yukawa term, introducing fermion mass terms
\begin{equation}
    \mathcal{L}_\text{Yukawa}= -G_{ij}^\ell \bar{L}_L^i\phi\ell_R^j-G^d_{ij}\bar{Q}^i_L\phi\mathit{d}_R^j - G^u_{ij}\bar{Q}^i_L\phi^c\mathit{u}_R^j+h.c.,\label{eq:theory:yukawa:first}
\end{equation}
with $\phi^c=\iu\sigma_2\phi$ the charge conjugate,  $h.c.$ the hermitian conjugated term and the Yukawa couplings $G^{\ell,d,u}_{ij}$ described as $3\times3$-matrices.


\section{The Higgs Mechanism}\label{sec:theory:higgs}
The Higgs mechanism induces the spontaneous \ac{ewsb} as described in Section~\ref{sec:theory:emweak:higgs}. This mechanism allows mass terms in the electroweak Lagrangian for gauge bosons $W^\pm$ and $Z$ as well as for fermions via the Yukawa couplings.\\
The lowest energy state of the potential in Equation~\eqref{eq:theory:higgspot} is denoted as the \ac{vev} $v$. By choosing the parameter $\mu^2<0$, the minimum is located at $\phi^\dagger\phi=0$ with all real scalar fields having zero \ac{vev} as shown in the left plot of Figure~\ref{fig:theory:higgs-v}. This configuration would preserve the $SU(2)_L \otimes U(1)_Y$ symmetry also at the minimum.
\begin{figure}[h]
    \centering
    \begin{minipage}[t]{0.3\textwidth}
    \includegraphics[width=1.\textwidth]{background/material_theory/Higgs-unbroken}
    \end{minipage}
    \begin{minipage}[t]{0.3\textwidth}
    \includegraphics[width=1.\textwidth]{background/material_theory/Higgs-potential.pdf}
    \end{minipage}
    \begin{minipage}[t]{0.3\textwidth}
    \includegraphics[width=1.\textwidth]{background/material_theory/Higgs-potential-flat.pdf}
    \end{minipage}
    \caption{Higgs potential following Equation~\eqref{eq:theory:higgspot}. On the left with the parameter choice $\mu^2<0$ with only a minimum at 0. The middle plot shows the Higgs potential with $\mu^2>0$ as well as the right plot which is a projection of the middle plot, indicating the minima at $\phi^\dagger\phi=2\mu^2/\lambda$.\label{fig:theory:higgs-v}}
\end{figure}
 However, with $\mu^2>0$ the minimum is not located at 0, instead it is at $\phi^\dagger\phi=2\mu^2/\lambda$, illustrated in the two right plots in Figure~\ref{fig:theory:higgs-v}, resulting in the \ac{vev}
 \begin{equation}
   \langle\phi \rangle=\frac{1}{\sqrt{2}}\left(\begin{array}{c}
        0  \\
        v 
   \end{array}\right)\quad \text{with}\quad v=\frac{2\mu}{\sqrt{\lambda}},\label{eq:theory:vev}
 \end{equation}
picking $\phi$ as electrically neutral without loss of generality. In fact, the vacuum configuration $\langle\phi \rangle$ violates the $SU(2)_L \otimes U(1)_Y$ symmetry and spontaneously breaks it down to the \ac{em} subgroup $U(1)_\text{EM}$. Rewriting the potential in the unitary gauge one gets
\begin{equation}
    \phi(x)=\frac{1}{\sqrt{2}}\left(\begin{array}{c}
         0  \\
         v+H(x) 
    \end{array}\right),\label{eq:theory:unitarypotential}
\end{equation}
where $H$ is a scalar field depicting the Higgs boson and the potential can be written as 
\begin{equation}
    V=\mu^2H^2+\frac{\mu^2}{v}H^3+\frac{\mu^2}{4v^2}H^4=\frac{M_H^2}{2}H^2+\frac{M_H^2}{2v}H^3+\frac{M_H^2}{8v^2}H^4,
\end{equation}
where the Higgs mass results in $M_H=\mu\sqrt{2}$. Also, the potential contains terms with triple and quartic self-interactions of the Higgs with couplings proportional to the Higgs mass $M_H$.\\
The kinematic term of Equation~\eqref{eq:theory:higgs-lagrangian} describes the coupling of the Higgs field to the gauge fields, and can be expressed via \eqref{eq:theory:vev} and \eqref{eq:theory:unitarypotential} as
\begin{equation}
\begin{split}
    \left(D_\mu\phi\right)^\dagger D^\mu\phi = & \frac{g_2v^2}{4}W^{+\mu}W_\mu^-\left(1+\frac{H}{v}\right)^2\\
    &+\frac{1}{2}\frac{\sqrt{g_1^2+g_2^2}v^2}{4}Z^\mu Z_\mu\left(1+\frac{H}{v}\right)^2+\frac{1}{2}(\partial^\mu H)(\partial_\mu H),
\end{split}
\end{equation}
containing the physical fields
\begin{equation}
    W_\mu^\pm = \frac{1}{\sqrt{2}}(W_\mu^1\mp\iu W_\mu^2),
\end{equation}
depicting the massive charged gauge bosons $W^\pm$ and the neutral gauge bosons, given as
\begin{equation}
    \left(\begin{array}{cc}
         Z_\mu  \\
         A_\mu 
    \end{array}\right)
    =
    \left(\begin{array}{cc}
         \cos\theta_W & \sin\theta_W  \\
         -\sin\theta_W & \cos\theta_W 
    \end{array}\right)
    \left(\begin{array}{cc}
         W^3_\mu  \\
         B_\mu 
    \end{array}\right),\label{eq:theory:mixingzgamma}
\end{equation}
where the $Z_\mu$ field describes the massive $Z$-boson and the $A_\mu$ field the massless photon. As a consequence the masses are defined in terms of the couplings and the \ac{vev} as
\begin{equation}
    M_W=\frac{1}{2}g_2v, \quad M_Z=\frac{1}{2}\sqrt{g_1^2+g_2^2}v.
\end{equation}
The weak mixing angle $\theta_W$ is introduced in the rotation in Equation~\eqref{eq:theory:mixingzgamma} and defined as
\begin{equation}
    \cos\theta_W=\frac{g_2}{\sqrt{g_1^2+g_2^2}}=\frac{M_W}{M_Z},
\end{equation}
which also allows to express the electric charge in terms of the gauge couplings as $e=g_2\sin\theta_W$.\\


The fermions also acquire their masses via the interaction with the Higgs field described in the Yukawa term in Equation~\eqref{eq:theory:yukawa:first} which can be expressed in the unitary gauge:
\begin{equation}
    \mathcal{L}_\text{Yukawa}= -\sum_f m_f\bar{\psi}_f\psi_f\left(1+\frac{H}{v}\right),
    % -\sum_f m_f\bar{\psi}_f\psi_f-\sum_f\frac{m_f}{v}\bar{\psi}_f\psi_f H
\end{equation}
with the mass defined as
\begin{equation}
    m_f=G_f\frac{v}{\sqrt{2}}=y_f\frac{v}{\sqrt{2}},\label{eq:theory:yukawa}
\end{equation}
and $y_f$ being the Yukawa coupling.
The lepton mass matrix $G_{ij}^\ell$ has no off-diagonal entries since the lepton number is conserved for each generation in the \ac{sm} and reduces with the assumption of massless neutrinos to 
\begin{equation}
    \left(\begin{array}{ccc}
        m_e & 0 & 0 \\
         0 & m_\mu & 0 \\
         0 & 0 & m_\tau
    \end{array}\right).
\end{equation}
% \begin{equation}
%     M_H=\mu\sqrt{2}
% \end{equation}
However, the quark mass matrix $G_{ij}^{u,d}$ has off-diagonal entries which can be diagonalised via four unitary matrices $V_{L,R}^{u,d}$ resulting in the mass eigenstates
\begin{equation}
    \Tilde{u}^i_{L,R}=(V^u_{L,R})_{ik}u^k_{L,R}, \quad \Tilde{d}^i_{L,R}=(V^d_{L,R})_{ik}u^k_{L,R}.
\end{equation}
By introducing the mass eigenstates in the Lagrange density, its structure is retained except for the flavour-changing quark interactions mediated by the charged vector bosons.
The quark mixing matrix also denoted as \ac{ckm} matrix reads then as
\begin{equation}
\left(\begin{array}{rrr} 
d' \\ 
s' \\ 
b' \\ 
\end{array}\right)
=V_{CKM}\left(\begin{array}{rrr} 
d \\ 
s \\ 
b \\ 
\end{array}\right)=
\left(\begin{array}{rrr} 
V_{ud} & V_{us} & V_{ub} \\ 
V_{cd} & V_{cs} & V_{cb} \\ 
V_{td} & V_{ts} & V_{tb} \\ 
\end{array}\right)
\left(\begin{array}{rrr} 
d \\ 
s \\ 
b \\ 
\end{array}\right),
\end{equation}
with 
\begin{equation}
    V_L^uV_L^{d\dagger}\equiv V_{CKM}.
\end{equation}
The diagonal elements of the \ac{ckm} matrix are close to one, in particular the $|V_{tb}|$ term with a value of $0.999105\pm 0.000032$~\cite{PhysRevD.98.030001} which will be of special importance in the following chapters.

\section{Higgs Boson Production and Decay Channels}\label{sec:theory:higgs}
 In 2012, the ATLAS and CMS collaborations discovered a new particle compatible with the \ac{sm} Higgs boson~\cite{higgs, higgs-cms}, representing one of the major physics goals of the \ac{lhc}. Further studies over the last years confirmed its properties being consistent with the \ac{sm} predictions. Probing the \ac{ewsb} sector is important for \ac{sm} precision measurements as well as for investigations for physics beyond the \ac{sm}. The Higgs boson is measured to have a mass of $\unit[(125.10\pm0.14)]{GeV}$~\cite{PhysRevD.98.030001}.\\

The Feynman diagrams of the four major Higgs production modes at the \ac{lhc} are shown in Figure~\ref{fig:theory:higgsproduction}.
\begin{figure}[h]
	\centering
	\includegraphics[width=1.\textwidth]{background/material_theory/Higgs-production-feyn}
	\caption{Feynman diagrams of the four major Higgs production modes at the \acs{lhc}: (a) gluon fusion (ggF), (b) vector boson fusion (VBF), (c) Higgs Strahlung (VH) and (d) associated production with a top quark pair ($t\bar{t}H$).\label{fig:theory:higgsproduction}}
\end{figure}%
The most dominant production mode is the gluon fusion (ggF) process, comprising a fermion loop dominated by the heaviest fermion, the top quark, % (the \quark contributes $\sim5\%$)
followed by the vector boson fusion (VBF) and the Higgs Strahlung (VH) which both probe the Higgs coupling to the heavy gauge bosons. The fourth most dominant Higgs production mode is the associated production with a top quark pair (\ttH) allowing a direct measurement of the top Yukawa coupling. The $tH$ production mode is sensitive to both the magnitude and the sign of the Yukawa coupling. Due to negative inference of the Feynman diagrams, its cross-section is about one order of magnitude lower than the \ttH cross-section.
% Even though the $tH$ production mode would be even sensitive to the sign of the top-Yukawa coupling it is far more difficult to access it due to its low cross-section which is around one order of magnitude smaller than for \ttH. \\
 Figure~\ref{fig:lhc-xs} illustrates the cross-sections of important processes, including Higgs productions, at hadron colliders. Compared to many other \acs{sm} processes, or even compared to the \ttbar cross-section, the Higgs production modes are orders of magnitudes lower and therefore sophisticated techniques are necessary to extract the Higgs signals in analyses. For the \ttH production mode, the main background is coming from \ttbar processes whose cross-section is more than two orders of magnitudes larger.\\
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{background/material_ttH/lhc-xsec}
	\caption{Cross-section of different processes as a function of the centre-of-mass energy in proton-proton collisions above \cme[4] and below for proton-antiproton collisions. The dashed line indicates a centre-of-mass energy of \cme~\cite{lhcxs}. \label{fig:lhc-xs}}
\end{figure}
\begin{figure}[h!]
	\centering
\begin{minipage}[t]{0.45\textwidth}
	\includegraphics[width=1.\textwidth]{background/material_ttH/higgs-piechart}
	\caption{Branching ratio of the Higgs decay with a Higgs mass of $m_H=\unit[125]{GeV}$. The numbers are taken from~\cite{PhysRevD.98.030001}. \label{fig:higgs-br}}
	\end{minipage}
	\hspace{0.2cm}
	\begin{minipage}[t]{0.45\textwidth}
\includegraphics[width=1.\textwidth]{background/material_theory/ttbar-piechart-extended}
	\caption{Pie chart showing the individual \ttbar decay modes. The label $e/\mu$ comprises the dileptonic decay modes $\mu\mu,\;\mu e,\;ee$ and $\tau+\ell$ the decay modes $\tau+\tau/\mu/e$. Also hadronically decaying $\tau$s are included in the leptonic decay modes. The numbers are taken from~\cite{PhysRevD.98.030001}.\label{fig:ttbar-br}}
\end{minipage}
\end{figure}

Figure~\ref{fig:higgs-br} shows the different decay modes for a Higgs with a mass of  $m_H=\unit[125]{GeV}$. By far the largest branching ratio is the \hbb decay mode with 58.2\%.
% \begin{figure}[h!]
% 	\centering
% 	\includegraphics[width=0.5\textwidth]{background/material_ttH/higgs-piechart}
% 	\caption{Branching ratio of the Higgs decay with a Higgs mass of $m_H=\unit[125]{GeV}$. The numbers are taken from~\cite{PhysRevD.98.030001}. \label{fig:higgs-br}}
% \end{figure}
The second most probable decay mode with 21.4\% is the decay to $WW^*$. Even though both of these decay modes have a large branching ratio, they are challenging to access due to difficulties to distinguish them from background processes. The so-called \textit{Golden Channels}, having the cleanest final state signatures, are the $\gamma\gamma$ and the $ZZ^*\rightarrow4\ell$ decay modes. Despite their very low branching ratios: 0.2\% and 2.6\%, respectively, they are easier to access in physics analyses.\\


 The \ttH production combined with the $\bbbar$ decay mode is analysed in this thesis in Part~\ref{part:analysis} and a more detailed description and motivation for this analysis can be found in Chapter~\ref{chap:ttH}.\\



\section{The Top Quark}
The top quark, with its mass of $\unit[(172.69\pm0.25(\text{stat.})\pm0.41(\text{syst.}))]{GeV}$~\cite{TOPQ-2017-03}, is the heaviest particle in the \ac{sm}. Due to its large fermion mass, also its Yukawa coupling to the Higgs is the strongest with $y_t\simeq1$ shown in Equation~\eqref{eq:theory:yukawa}. This strong coupling induces the dominance in the loop contributions in Higgs productions ($ggF$) and decays ($H\rightarrow\gamma\gamma$). Since these loop effects involve heavy virtual particles they are also sensitive to physics beyond the \ac{sm}.\\
Another implication of the high mass is that the top quark can decay into a $W$-boson and a \quark. In fact, the top quark almost exclusively decays via this mode indicated by the value of the \ac{ckm} matrix element $|V_{tb}|$ being close to 1, as already mentioned above. Its decay width of $\unit[1.42^{+0.19}_{-0.15}]{GeV}$~\cite{PhysRevD.98.030001} leads to a mean lifetime of $\sim\unit[5\cdot10^{-25}]{s}$ which is uniquely shorter than the time scale of hadronisation processes, making the top quark decay as an almost free particle. These properties create a very recognisable decay signature in the detector.\\
At hadron colliders, top quarks are predominantly produced in \ttbar pairs. The decay modes are typically classified according to the decay of the two involved $W$-bosons. The pie chart in Figure~\ref{fig:ttbar-br} shows the individual decay rates of  \ttbar. The all-hadronic final state has the largest branching ratio with 45.7\% where both $W$-bosons decay hadronically. Followed by the \ljchan (semi-leptonic) decay channel (43.8\%) in which one $W$-boson decays hadronically and one leptonically. The smallest fraction of 10.5\% is allocated to the dileptonic mode (both $W$-bosons decay leptonically).





\section{Limitations of the Standard Model}\label{sec:theory:bsm}
Figure~\ref{fig:smresultsoverview} shows different \ac{sm} cross-section measurements in comparison to their theoretical predictions. They are all in a good agreement with theoretical expectations. However, despite the success of the \ac{sm}, it cannot describe all experimental observations sufficiently. Several astrophysical observations saw that only a small fraction (around $17\%$) of the matter in the universe is made of the \ac{sm} components. Thus, another source of matter has to be present making up $25\%$ of the universe, corresponding to $83\%$ of all matter in the universe, denoted as dark matter~\cite{doi:10.1146/annurev.aa.25.090187.002233} which could consist of  weakly interacting massive particles. There are, however, no suitable candidates within the \ac{sm}. Also, astrophysical observations show a stronger expansion of the universe than predicted by cosmological theories~\cite{doi:10.1063/1.3232196}. This phenomenon appears to be caused by a non-detectable energy, called dark energy (being the last missing $70\%$). Moreover, neutrino experiments observed neutrino oscillations~\cite{PhysRevLett.81.1562,PhysRevLett.87.071301}, indicating that neutrinos do have a mass. However, they are assumed to be massless in the \ac{sm} because at the time of the formulation of the \ac{sm} their mass was not observed. If neutrinos would have a Dirac mass, it could be easily added to the \ac{sm}. However, there are no indications yet for a right-handed neutrino and it is therefore not evident that neutrinos have a Dirac mass.  The fact that the universe consists of far more matter than anti-matter reveals another unexplained physics behaviour.\\ % This matter-antimatter asymmetry is also called baryon asymmetry.\\
Furthermore, there are theory implications suggesting physics beyond the \ac{sm} e.g. the unification of the electroweak and strong force as well as the missing gravity in the theory.\\
Several of these beyond \ac{sm} physics processes predict new particles that could be found in particle collider experiments. There is, however, no evidence yet for these beyond \ac{sm} particles. The search for these new particles is also one of the main goals of the \ac{lhc} in the future.
% \begin{itemize}
%     \item Vacuum stability \& top yukawa coupling -> Top quark chapter
% \end{itemize}


\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{background/material_theory/ATLAS_b_SMSummary_FiducialXsect.pdf}
	\caption{A summary of different Standard Model production cross-section measurements (total and fiducial) performed with the ATLAS experiment. They are corrected for leptonic branching fractions and compared to the corresponding theoretical expectations~\cite{ATL-PHYS-PUB-2020-010}. \label{fig:smresultsoverview}}
% 	\caption{Overview of the particles of the SM grouped by~\cite{SMparticles}. \label{fig:SMpart}}
\end{figure}

% \section*{General Remark}
% In the following, using the terms electrons, muons and taus comprise always the particles and anti-particles if not stated differently. The same is valid for quarks and anti-quarks. In addition, the term lepton is used to refer to only the first and second generation leptons excluding the $\tau/\nu_\tau$.
% \improvement{check if this is already well placed here or should go later on}

