\pdfbookmark[1]{Physics Simulation at Hadron Colliders}{physics-simulation-hadron-colliders}
\chapter{Physics Simulation at Hadron Colliders}\label{chap:hadron-phys}

As described in Chapter~\ref{chap:theory}, most elementary particles are unstable and have short lifetimes. To investigate their properties in a controlled environment particle colliders are used. In particular, hadron colliders allow to reach higher \com energies than $e^+$-$e^-$-colliders. However, the initial state in hadron collisions is not well defined since hadrons are composite particles and carry only a fraction of the hadron momentum.\\
This chapter will give an introduction to the event simulation with  \acp{pdf} and \ac{mc} generators and a quick discourse on the detector simulation.


\section{Event Simulation}\label{sec:phys-hadcol:mc}

In order to analyse the data from collider experiments, it is important to have a reliable simulation of the underlying processes. Simulation is the basis for each physics analysis performed at collider experiments.\\
% The analysis optimisation is typically performed in a blinded way, which means that only in the last analysis step data is used.\\
In particle physics, the simulation is based on \ac{mc} generators which are %basically random number generators, 
a stochastic tool incorporating theoretical predictions, which are well-suited to describe the statistical processes.\\
The cross-section of a hard scattering-event at hadron colliders $\sigma_{A,B\rightarrow X} $ can be factorised into two components using the factorisation theorem~\cite{doi:10.1146/annurev.ns.37.120187.002123}. The \acp{pdf} $f_{a}^A$ and $f_{a}^B$ describe the colliding partons $a,b$ which are contained in the hadrons $A,B$ while the cross-section of the hard scattering itself, $\hat{\sigma}_{a,b}$, can be usually calculated with perturbation theory. The cross-section can be written as
\begin{equation}
		\sigma_{A,B\rightarrow X} = \sum_{a,b} \int_0^1 \text{d}x_1\text{d}x_2 f_a^A(x_1,\mu_F^2) f_b^B(x_2,\mu_F^2) \hat{\sigma}_{a,b\rightarrow X}(\alpha_s(\mu_R^2),\mu_R^2),
\end{equation}
where $\mu_F$ is the factorisation scale chosen such that it usually corresponds  to a characteristic momentum transfer of the selected process and $x_{1,2}$ the \textit{Bjorken} $x$ described in more detail below. 

\subsection{Parton Distribution Functions}\label{sec:phys-hadcol:pdfs}
The \acp{pdf} are crucial for the description of proton-proton collisions since protons are not point-like particles but consist of so-called \textit{partons}. The first type of partons are the valence quarks which determine the quantum numbers (charge, etc.) of the proton. In addition, gluons and virtual quark-antiquark pairs (sea-quarks) coming from vacuum fluctuations are also a part of the proton. A \ac{pdf} $f_{a}^A(x,Q^2)$ describes the probability density of a parton $a$ inside a hadron $A$ to carry a certain momentum fraction $x=p_a/p_A$ also betoken as \textit{Bjorken} $x$ evaluated at a specific momentum transfer $Q^2$. In general, \acp{pdf} cannot be directly predicted\footnote{In principle lattice \acs{qcd} could be used to calculate \acp{pdf}~\cite{Bhat:2020ktg}, however, this is very computationally intensive.} thus they are extracted from several measurements using a complex fit, performed at a specific scale. Several collaborations such as  the CTEQ, MSTW and NNPDF collaborations~\cite{PhysRevD.93.033006,Whalley:2005nh,Martin:2009iq,Ball:2014uwa} determine the \acp{pdf} and provide them for physics analyses.
 With the help of the \ac{dglap} Equations~\cite{Gribov:1972ri,ALTARELLI1977298,Dokshitzer:1977sg}, the \acp{pdf} can be extrapolated to different scales $Q^2$ and do not have to be measured at each scale individually. Figure~\ref{fig:pdf} shows the proton \acp{pdf} for two different factorisation scales.\\
\begin{figure}[h]
    \centering
    \begin{minipage}[t]{0.49\textwidth}
    \includegraphics[width=1.\textwidth]{background/material_physics_at_lhc/NNPDF-10}
    % \caption*{(a)}
    \end{minipage}
    \begin{minipage}[t]{0.49\textwidth}
    \includegraphics[width=1.\textwidth]{background/material_physics_at_lhc/NNPDF-10000}
    % \caption*{(b)}
    \end{minipage}
    \caption{The parton distribution functions $xf(x,\mu_F^2)$ are shown for two different factorisation scales $\mu_F^2$: left: $\mu_F^2=$\unit[10]{GeV$^2$} and right: $\mu_F^2=$\unit[10]{TeV$^2$}. They are obtained with the \nnpdfnnlo global analysis~\cite{Ball:2014uwa}. Plots were taken from~\cite{PhysRevD.98.030001}. \label{fig:pdf}}
\end{figure}
% from https://journals.aps.org/prd/pdf/10.1103/PhysRevD.93.033006

Processes involving \quarks can be described in \acs{qcd} in two different factorisation schemes arising from the \quark mass $\Lambda_\text{QCD}<m_b\ll v$ : the \ac{4fs} and the \ac{5fs}. The \ac{4fs} treats the \quarks massive and since $m_b>m_\text{proton}$, they do not appear in the initial state. Consequently, the \quarks do not have dedicated \acp{pdf}, so they decouple from the \acs{qcd} perturbative evolution and therefore decouple from the $\alpha_s$ running and the number of 'light' flavour quarks is set to $n_f=4$ in Equation~\eqref{eq:theory:alphas}. Considering the \quarks as massive is especially impacting calculations at lower scales, around the production threshold. On the other hand, at high scales the mass effects are negligible. This case is described by the \ac{5fs} in which the initial state \quarks are considered massless and they are treated in the same manner as the other light quarks comprising a \quark \ac{pdf} and $n_f=5$.


\subsection{Monte Carlo Generators}
Typically, the event generation is divided into two parts: first the \ac{me} generation describing the hard scattering and secondly the \ac{ps} evolution and hadronisation modelling including \ac{isr} and \ac{fsr}. While the \ac{me} and most parts of the \ac{ps} can be calculated perturbatively, the other processes are non-perturbative. A simplified illustration of this full simulation process is shown in Figure~\ref{fig:parton-shower}. For the modelling of the hadronisation, there are different models, the most widely used models are: the Lund string model~\cite{Andersson:1983ia} and the cluster model~\cite{Winter:2003tt}. In the Lund string model, the colour connection of a quark-antiquark pair is described as a string and the potential between them is assumed to be linearly increasing with their distance. The strings then split according to a fragmentation function forming  new quark-antiquark pairs which continues until only hadrons with on-shell mass remain.
The cluster model is based on \acs{qcd} pre-confinement, where neighbouring partons build colour-singlet clusters, these clusters then decay into two hadrons and they then decay further until the final state hadrons are formed.\\

% \improvement{Lund String model~\cite{Andersson:1983ia} und Cluster model~\cite{Winter:2003tt} -> als unc benutzt -> 3-4Saetze}
\begin{figure}[h]
    \centering
       \includegraphics[width=.75\textwidth]{background/material_physics_at_lhc/parton-shower.pdf}
       \caption{Illustration of a hadron-hadron collision event simulated with a \ac{mc} event generator. In the centre, the red circle represents the hard collision while the purple oval depicts the secondary hard scattering process (underlying event) with multi parton interaction. Both are surrounded by a tree-like structure describing the \acs{qcd} bremsstrahlung simulated by the \ac{ps}. The other elements in the sketch are the hadronisation (light green), hadron decays (dark green) and photon radiation (yellow)~\cite{Hoche:2014rga}. \label{fig:parton-shower}}
\end{figure}


% "The $h_\text{damp}$ factor, which is the model parameter that controls ME/Parton Shower (PS) matching in Powheg and effectively regulates the high-pT ra- diation, is set to the top quark mass"

% "This parameter is used as a resummation damping factor, which is one of the parameters controlling the ME/PS matching in Powheg and effectively regulates the high-pT radiation."


% "The hdamp parameter controls the pT of the first additional emission beyond the leading-order Feynman diagram in the PS and therefore regulates the high-pT emission against which the tt system recoils."


% 1.5~\mtop~\cite{ATL-PHYS-PUB-2016-020}


% \begin{figure}[h]
%     \centering
%       \includegraphics[width=.5\textwidth]{background/material_physics_at_lhc/pp_interaction}
%       \caption{Schematic view of a $pp$-collision~\cite{pp-coll} \label{fig:pp-schematic}}
% \end{figure}

The full process involving matrix element generation, parton shower, underlying event, hadronisation and fragmentation can be simulated by \acs{mc} generators like \pythia~\cite{Sjostrand:2014zea}, \herwigseven~\cite{Bahr:2008pv,Bellm:2015jjp} or \sherpa~\cite{Bothmann:2019yzt}. However, \pythia provides mainly leading order calculations which are often not sufficient since the \ac{nlo} corrections can be fairly large. \herwigseven provides many \acp{me} also at \ac{nlo}. Since the fraction of negative event weights can be quite large (up to $\sim 40\%$ for certain generator setups), the generator is only used as parton shower in this thesis. In fact, there are other generators like \powhegbox~\cite{Nason:2004rx,Frixione:2007vw,Alioli:2010xd,Hartanto:2015uka,Frixione:2007nw} %~\cite{Oleari:2010nx} 
or \mgamc~\cite{Alwall:2007st} providing higher-order calculations which can be interfaced with \pythia or \herwigseven for the simulation of \ac{ps} and hadronisation.\\
Furthermore, the models used to describe the non-perturbative processes have parameters that can be tuned using collision data. The most common tunes used by the ATLAS experiment are the A14 parameters~\cite{ATL-PHYS-PUB-2014-021} for \pythia or the H7UE set of tuned parameters~\cite{Bellm:2015jjp} for \herwigseven.\\

% \ac{mpi}
% Lately, new machine learning developments are dealing with the problem of likelihood-free inference to easier access the underlying theory parameters. This is e.g. realised solving the inverse problem with madminer~\cite{Brehmer:2019xox}.

\subsection{Common Generator Setup of used Samples} \label{sec:objects:mcsettings}
Throughout this thesis the physics processes for proton-proton collisions at a \come \cme are modelled using various combinations of \ac{mc} generators and settings. The specific details are stated in the dedicated chapters. Nevertheless, all \ac{mc} samples using \pythia or \herwigseven to model the \ac{mpi}, hadronisation and \ac{ps} use the same settings if not differently stated. The mass of the top quark is set to $m_t=\unit[172.5]{GeV}$, the Higgs boson mass to $m_H=\unit[125]{GeV}$ and the mass of the \quark to $m_b=\unit[4.8]{GeV}$ for \pythia, to $m_b=\unit[4.5]{GeV}$ for \herwigseven and to $m_b=\unit[4.75]{GeV}$ for \sherpa. The simulation of $b$- and $c$-hadron decays is performed via the \evtgen\ v1.6.0 program~\cite{Lange:2001uf} with the exception of \sherpa. As mentioned above the two tunes A14 combined with the \nnpdftwo \ac{pdf} set~\cite{Ball:2012cx} and H7UE together with the set of \mmhtlo \acp{pdf}~\cite{Harland-Lang:2014zoa} are used for \pythia and \herwigseven, respectively.





% \subsection{4 and 5 Flavour Scheme}

% $n_f$ from 



\section{Detector Simulation}\label{sec:hadroncoll:geant}
The last step in the simulation chain is the detector simulation. The \ac{mc} generators, as described in Section~\ref{sec:phys-hadcol:mc}, provide information about stable particles in the final state, not taking into account the detector response. The full ATLAS detector simulation~\cite{SOFT-2010-01} is performed in two steps. The first step is based on \textsc{Geant}4~\cite{Agostinelli:2002hh} incorporating the geometry of the detector and providing highly precise modelling of the particle interactions with the detector matter. However, it comes with the shortcoming of using a large fraction of the available computing power of ATLAS. As an alternative, fast calorimeter simulation algorithms~\cite{ATLAS:1300517,Dias:2016tea,Schaarschmidt:2017gpe} are developed and already used in practice. They mimic the \textsc{Geant}4 results, based on thousands of individual parametrisations of the calorimeter response, using significantly less computing resources with a trade-off in precision. A comparison of the necessary CPU time for the different detector simulations are shown in Figure~\ref{fig:geant4cputime}. In practice, the fast simulation algorithms are widely used in ATLAS and are called \textit{AtlFast-II}. In the second step, the readout electronics and digitisation is simulated which is adjusted for the different detector systems.\\ % giving adequat results for small $R$-jets.\\
Taking advantage of the latest machine learning developments in the last years, deep generative algorithms such as Generative Adversarial Networks (GANs) and Variational Auto-Encoders (VAEs) are studied to improve the fast calorimeter simulation~\cite{GANs:ATLAS} showing already promising results.


\begin{figure}[h]
    \centering
      \includegraphics[width=1.\textwidth]{background/material_physics_at_lhc/geant4-cpu}
      \caption{Comparison of the CPU time distributions for the full \textsc{Geant}4 (black), fast \textsc{Geant}4 (red) and the fast calorimeter simulation (blue) for 250 $t\bar{t}$ events. The vertical dotted lines indicate the average of the distributions~\cite{ATLAS:1300517}.\label{fig:geant4cputime}}
\end{figure}
