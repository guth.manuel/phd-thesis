#!/bin/bash
mtext=${1?missing text to show}
angle=45 # in degrees counterclockwise from horizontal
grey=0.85 # 0 is black 1 is white

ps2pdf - - <<!
%!PS
/cm { 28.4 mul } bind def
/draft-Bigfont /Helvetica-Bold findfont 14 scalefont def
/draft-copy {
        gsave initgraphics $grey setgray
        9 cm 29 cm moveto
        draft-Bigfont setfont
        ($mtext) show grestore
 } def
draft-copy showpage
!

# pdftk "$f" background watermark.pdf output "$f.pdf"