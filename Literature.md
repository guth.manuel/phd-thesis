# Literature and Infos for PhD Thesis

* BDT configurations l+jets classification training: https://gitlab.cern.ch/atlasHTop/TTHbbAnalysis/-/blob/master/TTHbbLeptonic/share/BDTweights_semilep/classification/TMVAClassification_BDT6j_signalRegions601_withReco_withBTag_15_0511_modulo_2_rest_1.weights.xml
* Explanation and graphic for resampling: https://www.kaggle.com/rafjaa/resampling-strategies-for-imbalanced-datasets
* Deep Sets Conf Note: https://cds.cern.ch/record/2718948
* Study of ttbb and ttW background modelling for ttH analyses https://cds.cern.ch/record/2697143/files/ATL-PHYS-PUB-2019-043.pdf
* ttH bb INT Note: https://cds.cern.ch/record/2699427
* ttHbb CONF Note: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2020-058/#tables



* Tracking Twiki: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TrackingCPMoriond2017
    also show all the tracking variables
* Newer Tracking Twiki: 

## Btagging 
* Secondary Vertex finding: https://cds.cern.ch/record/2270366/files/ATL-PHYS-PUB-2017-011.pdf

* gitlab repo DIPS Pub Note: https://gitlab.cern.ch/atlas-physics-office/FTAG/ANA-FTAG-2020-03/ANA-FTAG-2020-03-PUB

* Flavour labelling: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/FlavourTaggingLabeling
* Track jet calib (no calib) https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagTaggerRecommendationsRelease21
* VR overlap https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagCalibrationRecommendationsRelease21#Recommendations_for_variable_rad

* DIPS pub note git latex repo: https://gitlab.cern.ch/atlas-physics-office/FTAG/ANA-FTAG-2020-03/ANA-FTAG-2020-03-PUB




## Additional material (maybe for defence preparation)
* STXS uncertainties in ttH and others https://indico.cern.ch/event/922192/contributions/4057339/
* IML workshop with lots of ideas: https://indico.cern.ch/event/852553/timetable/?view=standard

* proton spin puzzle: https://www.gauss-centre.eu/results/elementaryparticlephysics/article/towards-solving-the-proton-spin-puzzle-by-lattice-qcd/

* High lumi perspectives: https://arxiv.org/pdf/1901.09829.pdf https://arxiv.org/pdf/1211.7242v1.pdf
* new sherpa mixing scheme: https://arxiv.org/pdf/1904.09382.pdf 



## Technical stuff

counting number of files in repository
```
find ./ -print | wc -l
 ```

excluding folders from sync with Dropbox: https://medium.com/@bozzified/dropbox-ignore-solving-ignoring-node-modules-and-other-folders-from-syncing-on-a-mac-ae5fa44543e8

### Running Latex diff
    1. Install `latexpand` https://gitlab.com/latexpand/latexpand
    2. Merge the latex files into one file `latexpand main.tex >merged.tex`
    3. Run latexdiff `latexdiff  merged-previous.tex merged.tex > diff.tex`
    4. Compile diff file: `latexmk diff.tex -pdf`



## Comments - Corrections
* Single top references: https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SingleTopRefXsec (is default reference - related to comment from Johannes)


## Abstracts for Adum (without latex characters)

### english

Since several decades, the predictions of the Standard Model (SM) of particle physics are being probed and validated. One major success of the Large Hadron Collider (LHC) at CERN was the discovery of the Higgs boson in 2012. With the increasing amount of proton-proton collisions recorded with the experiments located at the LHC, precise Higgs measurements are now possible and rare processes are accessible.
ATLAS and CMS  recently discovered the production process of a Higgs boson in association with a pair of top quarks using LHC RUN II data. The ttH(bb) process allows for a direct measurement of the Top-Yukawa coupling which is the strongest fermion-Higgs coupling in the Standard Model and plays therefore an important role in Higgs physics. The challenging final state with at least 4 b-jets requires an advanced analysis strategy as well as sophisticated b-jet identification methods. b-tagging is not only crucial in the ttH(bb) analysis, but most physics analyses within ATLAS are making use of it. The reoptimisation of the deep-learning-based heavy flavour tagger in ATLAS is shown in this thesis for two different jet collections. Various improvements were made resulting in a drastic performance increase up to a factor two in certain regions of the phase space.
The ttH(bb) analysis is performed using  139 fb-1 of RUN II ATLAS data at a centre-of-mass energy of √s=13 TeV. The signal strength, being the ratio of the measured cross-section over the predicted cross-section in the SM, was measured to be 0.43+0.20/-0.19(stat.)+0.30/-0.27(syst.) with an observed (expected) significance of 1.3 (3.0) standard deviations in the inclusive cross-section measurement. In addition, a simplified template cross-section (STXS) measurement in different Higgs pT bins is performed which is possible because of the ability to reconstruct the Higgs boson. The measurement is limited by the capability to describe the challenging irreducible ttbar+bb background and by systematic uncertainties.


### German

Seit mehreren Jahrzehnten werden die Vorhersagen des Standardmodells (SM) der Teilchenphysik erprobt und validiert. 
Mit der zunehmenden Anzahl von Proton-Proton-Kollisionen, die mit den Experimenten am LHC aufgezeichnet werden, sind nun präzise Higgs-Messungen möglich.
ATLAS und CMS haben kürzlich den ttH-Produktionsprozess mit Hilfe von LHC RUN II-Daten entdeckt. Der ttH(bb)-Prozess ermöglicht eine direkte Messung der Top-Yukawa-Kopplung, welche die stärkste Fermion-Higgs-Kopplung ist und daher eine wichtige Rolle im SM einnimmt. Der anspruchsvolle Endzustand mit mindestens 4 b-Jets erfordert eine fortschrittliche Analysestrategie sowie elaborierte b-Jet-Identifikationsmethoden. $b$-Tagging ist nicht nur in der ttH(bb)-Analyse von entscheidender Bedeutung, sondern die meisten Physik-Analysen innerhalb von ATLAS machen davon Gebrauch. Die Re-Optimierung des Deep-Learning-basierten Heavy-Flavour Taggers in ATLAS wird in dieser Arbeit für zwei verschiedene Jet-Definitionen gezeigt. Es wurden verschiedene Änderungen vorgenommen, die zu einer signifikanten Verbesserung von bis zu einem Faktor zwei in der Untergrundunterdrückung in bestimmten Phasenraumregionen führten.
Die ttH(bb)-Analyse wurde mit 139 fb-1 RUN II ATLAS-Daten bei einer Schwerpunktsenergie von √s=13 TeV durchgeführt. Die Signalstärke, d.h. das Verhältnis des gemessenen Wirkungsquerschnitts zum vorhergesagten Wirkungsquerschnitt im SM, wurde mit 0,43+0,20/-0,19(stat.)+0,30/-0,27(syst.) mit einer beobachteten (erwarteten) Signifikanz von 1,3 (3,0) Standardabweichungen für den inklusiven Wirkungsquerschnitt gemessen. Zusätzlich wurde zum ersten Mal eine vereinfachte differenzielle Wirkungsquerschnittsmessung in verschiedenen Higgs pT-Bereichen durchgeführt. Die Messung wird durch systematische Unsicherheiten begrenzt, hauptsächlich im Zusammenhang mit dem anspruchsvollen irreduziblen ttbar+bb Untergrund.




### french

ATLAS et CMS ont récemment découvert le processus de production ttH en utilisant les données prises durant le RUN II du LHC. Le processus ttH(bb) permet de mesurer directement le couplage de Yukawa du quark top, qui est le couplage fermion-Higgs le plus grand du modèle standard et joue donc un rôle important dans la physique du boson du Higgs.
L'état final de ce processus contient au moins 4 jets provenant de quarks b ce qui nécessite d'établir une stratégie d'analyse avancée ainsi que de développer des méthodes sophistiquées pour l'identification des jets provenant de quarks b. L'étiquetage des quarks b n'est pas seulement crucial pour l'analyse ttH(bb), mais aussi pour la plupart des analyses de physique au sein de l'expérience d'ATLAS. La ré-optimisation de l'étiquetage des quarks de saveurs lourdes basé sur un apprentissage profond dans ATLAS est présentée dans cette thèse pour deux collections de jets différentes. Diverses améliorations ont été apportées, entraînant une augmentation importantes des performances allant jusqu'à un facteur deux dans certaines régions de l'espace des phases.
L'analyse ttH(bb) est effectuée en utilisant 139 fb-1 de données enregistrées par ATLAS  durant le RUN II à une énergie dans le centre de masse de √s=13 TeV.L'intensité du signal, qui est le rapport entre la section efficace mesurée et la section efficace prédite par le modèle standard, a été mesurée à 0,43+0,20/-0,19(stat.)+0,30/-0,27(syst.) avec une signification observée (prévue) de 1,3 (3,0) déviations standard pour la mesure de la section efficace inclusive. En outre, une mesure simplifiée de la section efficace utilisant des gabarits Monte Carlo en fonction de l'impulsion transverse du boson de Higgs est effectuée.Cette mesure est limitée par la difficulté de simuler correctement le bruit de fond dominant ttbar+bb ainsi que par de grandes incertitudes systématiques.