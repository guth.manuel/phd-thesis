% https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/CONFNOTES/ATLAS-CONF-2020-058/#tables

\pdfbookmark[1]{Analysis-overview}{analysis-overview}
\chapter{Analysis Overview}\label{chap:ttH:overview}

Since the Yukawa coupling of a fermion is directly proportional to its mass, the top quark as heaviest elementary particle has therefore also the largest Yukawa coupling. The measurement of the \ttH process allows a direct measurement of this coupling.
% The Yukawa coupling plays an important role in the \acs{sm}. The \ttH production mode allows to directly measure the top-Yukawa coupling which is the largest within the \acs{sm}. The second-largest Yukawa coupling is the bottom-quark-Yukawa coupling accessible via the \hbb decay mode.
% A more detailed review of the \ttH production is shown in Chapter~\ref{chap:ttH}.
% While the $\ttH(\hbb)$ analysis allows to probe both of these Yukawa couplings. 
After the first RUN~II  $\ttH(\hbb)$ analysis from ATLAS~\cite{PhysRevD.97.072016} was published on a subset of the dataset of  $\unit[36]{fb^{-1}}$ (see sec.~\ref{sec:ttH:tthbb}), the analysis presented in this thesis is performed with the full RUN~II proton-proton collision dataset of $\unit[139]{fb^{-1}}$ at the \come of \cme~\cite{ttH-Conf}. 
% For the first time, a \acs{stxs} measurement of the Higgs transverse momentum for \ttHbb is shown.\\
A measurement of the \acf{stxs} as a function of the Higgs \pt is performed for the first time using \ttHbb events.\\

This chapter describes the \ttHbb analysis overview giving insights in its motivation, challenges and strategy. After a short analysis summary in Section~\ref{sec:tth:nutshell}, the event selection is explained in Section~\ref{sec:ttH:selection} followed by a description of the modelling of the signal and background processes in Section~\ref{sec:ttH:modelling}. The analysis strategy is presented in Section~\ref{sec:ttH:strategy} and the profile likelihood fit is introduced in Section~\ref{sec:ttH:profile-likelihoodfit}. In the last Section~\ref{sec:ttH:systunc} a summary of the systematic uncertainties is given.\\

% The topic then concludes in the results shown in Chapter~\ref{chap:ttH:results}. In addition, an outlook is presented in Chapter~\ref{chap:ttH:outlook} showing studies for further improvements of the analysis.\\

\section{The Analysis in a Nutshell}\label{sec:tth:nutshell}
As was shown in the pie-chart in Figure~\ref{fig:ttbar-br}, the \ttbar pair can either decay fully hadronically, semi-leptonically (lepton+jets) or dileptonically. In the scope of this thesis the lepton+jets channel\footnote{The lepton+jets channel will be also denoted as \textit{single lepton channel}.} was studied where one $W$-boson decays leptonically and the other one hadronically as shown in the Feynman diagram in Figure~\ref{fig:feym:ttHbb}. This channel offers a large statistics and has a relatively clean topology with the lepton in the final state allowing to suppress the multijet background. Since only one neutrino is present in the final state, the kinematics of the event can be fully determined taking into account the missing transverse momentum \met. 
Due to the fairly high branching ratio, also higher \pt regimes have sufficient statistics (compared to the dilepton channel) which is important for the \acs{stxs} measurements. The lepton+jets channel is further split into a resolved (lower \pt) regime and a boosted regime. 
% In fact, the dilepton channel, which only has a low branching ratio of $10.5\%$, is separately optimised by another team which also applies for the boosted lepton+jets channel. 
While this thesis focuses on the analysis of the resolved \ljchan channel, the dilepton and the boosted channel were optimised separately.
Nevertheless, all three channels are combined in a joint likelihood fit for the final results presented in this thesis. \\
% The dilepton channel has a very clean topology with its two leptons in the final state. However, the two associated neutrinos make the full event reconstruction more difficult.\\

\begin{figure}[h]
    \centering
\resizebox{0.7\textwidth}{!}{%
\begin{feynman} 
    \fermion[ label=$\bar{t}$, lineWidth=6, showArrow=true, flip=false, labelDistance=-0.6, labelLocation=0.5]{5.40, 1.80}{2.20, 3.00}
    \fermion[label=$q\quad\nu_\ell$%\quad\nu_\ell$
    , labelDistance=-0.10, labelLocation=1.1, lineWidth=6, showArrow=true, flip=false]{6.60, 5.80}{7.80, 6.60}
    \fermion[label=$\bar{\nu}_{\ell}\quad\bar{q}$%\quad\bar{\nu}_{\ell}$
    , labelDistance=0.1, labelLocation=1.2, lineWidth=6, flip=true]{6.60, 1.80}{7.80, 1.00}
    \gluon[label=$g$, lineWidth=6]{0.20, 4.60}{2.20, 4.60}
    \fermion[ lineWidth=6]{3.00, 3.80}{2.20, 4.60}
    \fermion[ lineWidth=6, label=\textcolor{red}{$\mathbf{b}$}, labelLocation=1.1, labelDistance=0.04]{5.40, 5.80}{7.80, 7.40}
    \fermion[ lineWidth=6]{2.20, 3.00}{3.00, 3.80}
    \fermion[showArrow=true, flip=true, label=\textcolor{red}{$\mathbf{\bar{b}}$}, labelDistance=0.1, labelLocation=1.1, lineWidth=6]{5.40, 1.80}{7.80, 0.20}
    \fermion[ label=\textcolor{red}{$\mathbf{b}$},color=0693e3, lineWidth=6, labelLocation=1.1, labelDistance=-0.04]{5.40, 3.80}{7.80, 4.20}
    \fermion[ label=\textcolor{red}{$\mathbf{\bar{b}}$}, color=0693e3, labelDistance=-0.04, labelLocation=-0.1, lineWidth=6, showArrow=true, flip=false]{7.80, 3.40}{5.40, 3.80}
    \electroweak[label=$W^{-}$, lineWidth=6, labelDistance=0.50, labelLocation=0.6]{6.60, 1.80}{5.40, 1.80}
    \fermion[label=$\ell^{-}\quad q'$%\quad\ell^{-}$
    , labelDistance=0.01, labelLocation=-0.2, lineWidth=6, showArrow=true, flip=true]{7.80, 2.60}{6.60, 1.80}
    \fermion[label=$\bar{q}'\quad\ell^{+}$%\quad\ell^{+}$
    , labelDistance=0.20, labelLocation=-0.1, lineWidth=6, showArrow=true, flip=false]{7.80, 5.00}{6.60, 5.80}
    \gluon[ label=$g$, lineWidth=6, labelDistance=-0.45, labelLocation=0.5]{0.20, 3.00}{2.20, 3.00}
    \electroweak[label=$W^{+}$, lineWidth=6, labelDistance=-0.40, labelLocation=0.4]{5.40, 5.80}{6.60, 5.80}
    \dashed[label=$H$, lineWidth=6]{3.00, 3.80}{5.40, 3.80}
    \fermion[ lineWidth=6, label=$t$]{2.20, 4.60}{5.40, 5.80}
\end{feynman}
}
\caption{Feynman diagram of the Higgs production with associated top quarks. The Higgs is decaying into a pair of $b$-quarks. The final state of this process contains at least four $b$-jets and exactly one lepton from the $W$ decays.}
    \label{fig:feym:ttHbb}
\end{figure}


The detector signature of the lepton+jets channel includes exactly one isolated lepton. For this analysis only decays into electrons and muons are considered, thus the term lepton $\ell$ is exclusively used for electrons and muons in the following. Nonetheless, the leptonic decay of the taus into electrons and muons is also considered. In addition, six quarks and therefore six jets are present in the final state where at least four of them are \bjet{}s. Consequently, \btag is crucial for this analysis and the analysis would heavily benefit from the improvements shown in Part~\ref{part:ftag}. However, this analysis was still performed with \emtopo and therefore does not include these improvements. Nevertheless, a short perspective will be given in Chapter~\ref{chap:ttH:outlook} with \pflow and the new \btag improvements. The complex final state topology of \ttHbb poses great challenges, especially the overwhelming \ttjets background. In particular, the main irreducible background is coming from \ttbb production for which an example Feynman diagram is shown in Figure~\ref{fig:feym:ttbb}. This process is poorly constrained by data measurements and has large theory uncertainties which limit the analysis.\\
\begin{figure}[h]
    \centering
\resizebox{0.6\textwidth}{!}{%
\begin{feynman} 
	\gluon[ lineWidth=4]{6.40, 6.80}{5.20, 6.60}
	\fermion[label=\textcolor{red}{$\mathbf{\bar{b}}$}, labelDistance=0.20, labelLocation=-0.19, color=0693e3, lineWidth=4]{7.20, 6.40}{6.40, 6.80}
	\fermion[label=$\bar{t}$, labelDistance=0.20, labelLocation=-0.10,  lineWidth=6]{11.20, 4.00}{8.80, 5.60}
	\fermion[label=$t$, labelDistance=-0.20, labelLocation=1.11,  lineWidth=6]{8.80, 5.60}{11.20, 7.20}
	\fermion[label=\textcolor{red}{$\mathbf{{b}}$}, labelDistance=-0.20, labelLocation=1.21,color=0693e3,  lineWidth=4]{6.40, 6.80}{7.20, 7.20}
	\gluon[label=$g$, labelDistance=0.30, labelLocation=-0.10,  lineWidth=6]{4.00, 4.00}{6.40, 5.60}
	\gluon[ lineWidth=6]{6.40, 5.60}{8.80, 5.60}
	\gluon[label=$g$, labelDistance=-0.20, labelLocation=-0.07,  lineWidth=6]{4.00, 7.20}{6.40, 5.60}
\end{feynman}
}
\caption{Example Feynman diagram showing the \ttbar production associated with a gluon initiated $b\bar{b}$ pair.}
    \label{fig:feym:ttbb}
\end{figure}

A schematic overview of the analysis strategy is illustrated in Figure~\ref{fig:tth:overview:ana-strategy}. The first step is the event selection (see sec.~\ref{sec:ttH:selection}), where a first phase space is chosen enhancing the \ttHbb signal contribution. In the next step, different multivariate techniques are employed which are based on \acfp{bdt} for the resolved lepton+jets channel. The reconstruction \ac{bdt} is matching the jets to the final state partons from the top-quark and Higgs decays. 
% In the next step, the analysis regions are defined which are categorised into \acp{sr} and \acp{cr}. 
% The \acp{sr} are enriched in signal and a classification \acs{bdt} is employed to separate the signal and background further. In the \acp{cr} the $\Delta R_{bb}^\text{avg}$ variable, which is the average $\Delta R$ between all possible \bjet{} pairs in an event, is used in the fit. The \acp{cr} are mainly used to constrain systematic uncertainties and normalisations coming from the background modelling.  The region definition was simplified with respect to the previous publication~\cite{PhysRevD.97.072016} and optimised for the \acs{stxs} measurement.
The preselected events are split into signal-depleted categories (control region, CR) and signal-enriched categories (signal region, SR) optimised for the \acs{stxs} measurement. In the signal regions, an additional \acs{bdt} is used to separate signal and background processes. All regions are then used in a combined profile-likelihood fit taking into account the systematic uncertainties and also including the boosted lepton+jets and dilepton channel. 

% In total, five \acp{sr}, one for each \acs{stxs} \higgspt~bin, and two \acp{cr} are defined. All regions are then used in a combined profile likelihood fit taking into account the systematic uncertainties and also including the boosted lepton+jets and dilepton channel. 


\begin{figure}[h]
    \centering
    \includegraphics[width=1.\textwidth]{ttH-Analysis/material_overview/Analysis-overview.pdf}
    \caption{Sketch of the \ttHbb analysis strategy for the resolved lepton+jets channel. The profile likelihood fit is performed in a combination with the lepton+jets boosted and the dilepton channel.}
    \label{fig:tth:overview:ana-strategy}
\end{figure}
 
%  Overall, 16 orthogonal regions are used in the final fit. The signal strength was measured to be $0.43^{+0.20}_{-0.19}(\text{stat.})^{+0.3}_{-0.27}(\text{syst.})$ with an observed (expected) significance of $1.3\;(3.0)$ standard deviations.