\pdfbookmark[1]{Analysis-outlook}{analysis-outlook}
\chapter{Outlook}\label{chap:ttH:outlook}

The \tthbb analysis presented in Chapters~\ref{chap:ttH:overview} and \ref{chap:ttH:results} is dominated by systematic uncertainties among which the modelling of the \ttbb background is by far the largest uncertainty.
In the following, ideas to further reduce the systematic uncertainties and ideas to improve the general analysis strategy will be discussed.
% For instance, the update of the conservative estimate of the \ttbin fraction uncertainty can slightly reduce the systematic uncertainties of the analysis, however, this effect should not be very large since this systematic uncertainty was not highly ranked in the fit result.
The multivariate techniques, namely the reconstruction and classification \acsp{bdt}, are well suited to be replaced by \aclp{nn} opening new possibilities in the reconstruction and classification of events.
And in particular, the improvements achieved within this thesis in the \btag algorithm performance described in detail in Part~\ref{part:ftag} are a great basis for further improvements. In the following two different optimisation possibilities are demonstrated: a new tagger incorporating the $\ttbar+1B$  category and a first look into possible improvements using the newly optimised \btag algorithm \dlr. 


\section{First Studies with the new Deep Learning based $b\bar{b}$-Tagger}

Depending on the chosen \acs{mc} generator, the fraction of $\ttbar+1B$ events contained in the \ttjets category as shown in Figure~\ref{fig:tth:strategy:ttbfraction} varies between 11\% and 13\%.
The $\ttbar+1B$ events contain two $b$-hadrons within one jet in addition to the \ttbar system as shown in the Feynman graph in Figure~\ref{fig:tth:overview:ana-strategy}. 
Due to the flexible structure of the deep-learning-based heavy flavour tagger presented in Part~\ref{part:ftag}, it is possible to introduce an additional category to the multi-classification. The extended flavour labelling as described in Section~\ref{sec:ftag:intro:flavourlabelling} can be used to identify jets containing two $b$-hadrons, further denoted as $bb$-\textit{jets}.

\subsection{Training Dataset}
The \ttbar samples described in Section~\ref{sec:ftag:modelling} are complemented by $Z(\rightarrow \mu\mu)$+\,$bb$-jets 
% and $Z(\rightarrow \nu\nu)$+\,jets 
events from which the $bb$-jets are extracted.
The \Zjets events are simulated with the \mbox{\sherpa\ v2.2.1} generator using the \acs{pdf} set \nnpdfnnlo as indicated in Table~\ref{tab:modelling:samples2}. While the light-flavour, $c$- and $b$-jets are extracted from the \ttbar sample, the $bb$-jets are taken from the \Zjets sample only.\\
The jets are clustered with the \textit{Particle Flow} algorithm.


\subsection{Training Setup}

The neural network architecture is adapted from the \acs{dl1} tagger described in Section~\ref{sec:ftag:dl1:design} adding a fourth classification category, the $bb$-jets category. The variables used for the training are the same as for the \dlrmu tagger (see. fig~\ref{fig:ftag:dl1structure}) and the \pt and $|\eta|$ distributions are reweighted to match the \bjet distributions. The same preprocessing steps are performed as for the \acs{dl1} tagger trainings.
Four example variable distributions are shown in Figure~\ref{fig:tth:outlook:bb-vars} which are the most promising input variables for the classification of $bb$-jets. 
The  charged energy fraction of the secondary vertex, calculated from the \acl{jf} algorithm optimised for \cjet identification (a), is shifted to lower values for $bb$-jets compared to \bjets. This can happen since only the \acs{sv} closest to the \acl{pv} is taken into account and hence often only one $b$-hadron is considered in the calculation which is typically the softer $b$-hadron since it is closer to the \acs{pv} and thus more likely to be less boosted than the other.
The mass of the vertex from the \acs{sv1} algorithm~(c) is flatter for $bb$-jets compared to \bjets and also reaches larger values. In the \acs{sv1} algorithm only one secondary vertex is reconstructed and hence in some cases only one $b$-hadron is part of the \acs{sv} and for the higher values both $b$-hadrons are reconstructed in one \acs{sv}.  Unfortunately, the distribution is cut at \unit[6]{GeV} since the algorithm is optimised for single \bjets and thus not optimal for $bb$-jets. 
% There is a possibility to have multi-vertex tagging with \acs{sv1}, however this information was not available.

\begin{figure}[htbp]
  \centering
  %\includegraphics[trim=0 620 0 0,clip,width=\linewidth]{inputs_IPxformed}
  \subfloat[]{\includegraphics[width=0.45\textwidth]{ttH-Analysis/material_outlook/secondaryVtx_EFrac.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.45\textwidth]{ttH-Analysis/material_outlook/IP2D_bu.pdf}}\\
  \subfloat[]{\includegraphics[width=0.45\textwidth]{ttH-Analysis/material_outlook/SV1_masssvx.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.45\textwidth]{ttH-Analysis/material_outlook/SV1_deltaR.pdf}}\\
  \caption{Example input variable distributions for the extended \dlrmu tagger training with a \mbox{$bb$-category}: (a) charged jet energy fraction of \acs{jf} w.r.t. all tracks in the jet $f_{E}^{{JF}_c}$ (b) IP2D$_{cl}$ (c)  the invariant mass of the \acs{sv} calculated from the associated tracks and (d)  $\Delta R({jet, SV})$.}
  \label{fig:tth:outlook:bb-vars}
\end{figure}





\subsection{First Training Results}
A first training was performed for 200 epochs. Figure~\ref{fig:tth:outlook:bb-scores} shows the distribution of the \bjet output node (a) and the $bb$-jet output node (b) which indicate the probability of a jet being a $b$- or $bb$-jet, respectively. Both distributions show a nice separation of the $b$- and $bb$-jets.
To further evaluate the performance, a simplified version of the log-likelihood discriminant from Equation~\eqref{eq:performance-multiclass} is used %to only take into account $b$- and $bb$-jets
$\mathcal{D}_\text{bb} = \log \left(  \frac{p_{bb}}{p_{b}}\right)$,
only taking into account the output-nodes of the $b$- ($p_b$) and $bb$-jets ($p_{bb}$). This simplification is made since the main goal is to compare the performance between these two classes. For further studies, all output-nodes should be taken into account. At a $bb$-jet efficiency of $30\%$, a \bjet rejection of 64 is obtained. Moreover, a rejection of around 5 for $bb$-jets is achieved at a \bjet efficiency of $70\%$.

% \begin{equation}
%     \mathcal{D}_\text{b}(f_c,f_\text{l},f_{bb}) = \log \left(  \frac{p_b}{f_c\cdot p_c+f_\text{l}\cdot p_\text{l}+f_{bb}\cdot p_{bb}} \right), \quad \text{with} \quad f_c+f_\text{l}+f_{bb}=1
% \end{equation}

\begin{figure}[htbp]
  \centering
  \subfloat[]{\includegraphics[width=0.45\textwidth]{ttH-Analysis/material_outlook/p-2.pdf}}\hspace{0.3cm}
  \subfloat[]{\includegraphics[width=0.45\textwidth]{ttH-Analysis/material_outlook/p-3.pdf}}\\
  \caption{Output distributions of the extended \dlr tagger with an additional $bb$-category for the (a) $b$-jet class output node and (b) the $bb$-jet output node.}
  \label{fig:tth:outlook:bb-scores}
\end{figure}


% \begin{figure}[htbp]
%   \centering
%   \subfloat[]{\includegraphics[width=0.45\textwidth]{ttH-Analysis/material_outlook/bb_rej-b.pdf}}\hspace{0.3cm}
%   \subfloat[]{\includegraphics[width=0.45\textwidth]{ttH-Analysis/material_outlook/bb_rej-bb.pdf}}\\
%   \caption{Tagger performance curves for (a) the $bb$-jet rejection as a function of the \bjet efficiency and (b) the\bjet rejection as a function of $bb$-jet efficiency.}
%   \label{fig:tth:outlook:bb-rej}
% \end{figure}

\subsection{Outlook}

This first study of an extended heavy-flavour tagger incorporating an additional $bb$-category showed already promising results. Nevertheless, there are still many improvements possible. Especially, the resampling approach presented in Section~\ref{sec:ftag:dl1:preprocessing} offers a more stable training. Moreover, the newly introduced \umami tagger (see sec.~\ref{sec:ftag:outlook:umami}) exploiting track information in an end-to-end training can extract more information to better classify $bb$-jets.\\
% Before this tagger could be used it would need to be calibrated which is fairly challenging





\section{First look at PFlow jets and the new DL1r Tagger in \tthbb}


The \tthbb analysis benefits from improvements in \btag in many ways: smaller calibration uncertainties reflect in smaller systematic uncertainties in the analysis and an improved tagging performance allows to better classify events. The improvements made in this thesis in the \btag performance are therefore crucial for further analysis improvements. In this section, a first look is taken into possible sensitivity improvements due to the new \dlr tagger using \pflow.\\

A simplified setup is used: no systematic uncertainties are taken into account, only the resolved \ljchan regions are considered and the sensitivity is only evaluated using \acs{mc} simulation.\\
The region definitions are the same as described in Section~\ref{sec:tth:regiondef} with the exception that events from the boosted \ljchan channel are not vetoed. 
For \emtopo, the same simplifications apply, but apart from that the same setup as described in Chapter~\ref{chap:ttH:overview} is used for them (i.e. the MV2c10 tagger), to have a setup for a direct comparison here.
Instead of the MV2c10 \btag algorithm, the \dlr tagger developed in Chapter~\ref{chap:ftag:dl1:pflow} is employed for \pflow. 
To incorporate the new \btag algorithm in the full analysis chain, the reconstruction and classification \acsp{bdt} are also evaluated using the information of \dlr but no retraining is performed with respect to the version for \emtopo. 
The signal ($S$) over background ($B$) ratio (black) as well as $S/\sqrt{B}$ (red) are shown in Figure~\ref{fig:tth:outlook:soverb} for both the \emtopo (dotted-dashed lines) and \pflow (solid lines). Overall, the new setup with \pflow and \dlr shows an increased $S/B$ and $S/\sqrt{B}$ ratio in each resolved \ljchan region with respect to the \emtopo.\\
\begin{figure}[h!]
    \centering
    \includegraphics[width=.9\textwidth]{ttH-Analysis/material_outlook/soverb_pflow.pdf}
    \caption{Contribution of the \ttH signal ($S$) in the different resolved \ljchan analysis regions shown for \pflow (solid lines) which use the new \dlr tagger and \emtopo (dotted-dashed lines) which still use the MV2c10 tagger. The black lines are associated to the left vertical axis showing the signal over background ($B$) ratio and the red lines show the $S/\sqrt{B}$ distribution corresponding to the right vertical axis.}
    \label{fig:tth:outlook:soverb}
\end{figure}

A statistical only Asimov fit is performed yielding a signal strength of
\begin{equation}
    \mu_\text{incl.}^{\text{PFlow}} = 1.00 \pm 0.19.
\end{equation}
 Moreover, the same fit is carried out for \emtopo resulting in
\begin{equation}
    \mu_\text{incl.}^{\text{EMTopo}} = 1.00 \pm 0.22.
\end{equation}
The new jet collection and the improvements from \dlr are already reflected in the results for this simplified setup. These results are a good basis for further improvements which are under development towards a RUN~II legacy paper.
